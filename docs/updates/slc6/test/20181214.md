## 2018-12-14

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-693.43.1.rt56.630.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-693.43.1.rt56.630.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-693.43.1.rt56.630.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-693.43.1.rt56.630.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-693.43.1.rt56.630.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-firmware-3.10.0-693.43.1.rt56.630.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-693.43.1.rt56.630.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-693.43.1.rt56.630.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-vanilla-3.10.0-693.43.1.rt56.630.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-vanilla-devel-3.10.0-693.43.1.rt56.630.el6rt | &nbsp; &nbsp; | &nbsp;
rt-setup-1.60-41.el6rt | &nbsp; &nbsp; | &nbsp;
autofs-5.0.5-140.el6_10 | &nbsp; &nbsp; | &nbsp;
ghostscript-8.70-24.el6_10.2 | &nbsp; &nbsp; | &nbsp;
ghostscript-devel-8.70-24.el6_10.2 | &nbsp; &nbsp; | &nbsp;
ghostscript-doc-8.70-24.el6_10.2 | &nbsp; &nbsp; | &nbsp;
ghostscript-gtk-8.70-24.el6_10.2 | &nbsp; &nbsp; | &nbsp;
kernel-2.6.32-754.9.1.el6 | &nbsp; &nbsp; | &nbsp;
kernel-abi-whitelists-2.6.32-754.9.1.el6 | &nbsp; &nbsp; | &nbsp;
kernel-debug-2.6.32-754.9.1.el6 | &nbsp; &nbsp; | &nbsp;
kernel-debug-devel-2.6.32-754.9.1.el6 | &nbsp; &nbsp; | &nbsp;
kernel-devel-2.6.32-754.9.1.el6 | &nbsp; &nbsp; | &nbsp;
kernel-doc-2.6.32-754.9.1.el6 | &nbsp; &nbsp; | &nbsp;
kernel-firmware-2.6.32-754.9.1.el6 | &nbsp; &nbsp; | &nbsp;
kernel-headers-2.6.32-754.9.1.el6 | &nbsp; &nbsp; | &nbsp;
oracle-instantclient-tnsnames.ora-1.4.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-754.9.1.el6 | &nbsp; &nbsp; | &nbsp;
python-perf-2.6.32-754.9.1.el6 | &nbsp; &nbsp; | &nbsp;
spice-server-0.12.4-16.el6_10.2 | &nbsp; &nbsp; | &nbsp;
spice-server-devel-0.12.4-16.el6_10.2 | &nbsp; &nbsp; | &nbsp;
thunderbird-60.3.0-1.el6 | &nbsp; &nbsp; | &nbsp;
