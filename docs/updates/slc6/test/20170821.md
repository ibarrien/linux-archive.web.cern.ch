## 2017-08-21

Package | Advisory | Notes
------- | -------- | -----
emacs-git-1.7.1-9.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2485.html" target="secadv">RHSA-2017:2485</a> | &nbsp;
emacs-git-el-1.7.1-9.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2485.html" target="secadv">RHSA-2017:2485</a> | &nbsp;
git-1.7.1-9.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2485.html" target="secadv">RHSA-2017:2485</a> | &nbsp;
git-all-1.7.1-9.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2485.html" target="secadv">RHSA-2017:2485</a> | &nbsp;
git-cvs-1.7.1-9.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2485.html" target="secadv">RHSA-2017:2485</a> | &nbsp;
git-daemon-1.7.1-9.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2485.html" target="secadv">RHSA-2017:2485</a> | &nbsp;
git-email-1.7.1-9.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2485.html" target="secadv">RHSA-2017:2485</a> | &nbsp;
git-gui-1.7.1-9.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2485.html" target="secadv">RHSA-2017:2485</a> | &nbsp;
gitk-1.7.1-9.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2485.html" target="secadv">RHSA-2017:2485</a> | &nbsp;
git-svn-1.7.1-9.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2485.html" target="secadv">RHSA-2017:2485</a> | &nbsp;
gitweb-1.7.1-9.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2485.html" target="secadv">RHSA-2017:2485</a> | &nbsp;
perl-Git-1.7.1-9.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2485.html" target="secadv">RHSA-2017:2485</a> | &nbsp;
