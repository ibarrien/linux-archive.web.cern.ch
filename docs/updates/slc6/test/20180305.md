## 2018-03-05

Package | Advisory | Notes
------- | -------- | -----
devtoolset-7-binutils-2.28-9.el6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-0001.html" target="secadv">RHBA-2018:0001</a> | &nbsp;
devtoolset-7-binutils-devel-2.28-9.el6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-0001.html" target="secadv">RHBA-2018:0001</a> | &nbsp;
