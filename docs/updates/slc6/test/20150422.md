## 2015-04-22

Package | Advisory | Notes
------- | -------- | -----
crash-6.1.0-6.el6_6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-0861.html" target="secadv">RHEA-2015:0861</a> | &nbsp;
crash-devel-6.1.0-6.el6_6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-0861.html" target="secadv">RHEA-2015:0861</a> | &nbsp;
glibc-2.12-1.149.el6_6.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0863.html" target="secadv">RHSA-2015:0863</a> | &nbsp;
glibc-common-2.12-1.149.el6_6.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0863.html" target="secadv">RHSA-2015:0863</a> | &nbsp;
glibc-devel-2.12-1.149.el6_6.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0863.html" target="secadv">RHSA-2015:0863</a> | &nbsp;
glibc-headers-2.12-1.149.el6_6.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0863.html" target="secadv">RHSA-2015:0863</a> | &nbsp;
glibc-static-2.12-1.149.el6_6.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0863.html" target="secadv">RHSA-2015:0863</a> | &nbsp;
glibc-utils-2.12-1.149.el6_6.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0863.html" target="secadv">RHSA-2015:0863</a> | &nbsp;
java-1.7.0-oracle-1.7.0.79-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-devel-1.7.0.79-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-jdbc-1.7.0.79-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-plugin-1.7.0.79-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-src-1.7.0.79-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-1.8.0.45-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.45-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.45-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.45-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.45-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.45-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
kernel-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-abi-whitelists-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-debug-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-debug-devel-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-devel-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-doc-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-firmware-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-headers-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-module-openafs-2.6.32-504.16.2.el6-1.6.6-cern3.0.slc6 | &nbsp; &nbsp; | &nbsp;
nscd-2.12-1.149.el6_6.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0863.html" target="secadv">RHSA-2015:0863</a> | &nbsp;
perf-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
python-perf-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
qemu-guest-agent-0.12.1.2-2.448.el6_6.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0867.html" target="secadv">RHSA-2015:0867</a> | &nbsp;
qemu-img-0.12.1.2-2.448.el6_6.2 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-0.12.1.2-2.448.el6_6.2 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-tools-0.12.1.2-2.448.el6_6.2 | &nbsp; &nbsp; | &nbsp;
tzdata-2015c-2.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-0855.html" target="secadv">RHEA-2015:0855</a> | &nbsp;
tzdata-java-2015c-2.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-0855.html" target="secadv">RHEA-2015:0855</a> | &nbsp;
virt-who-0.10-8.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0859.html" target="secadv">RHBA-2015:0859</a> | &nbsp;
