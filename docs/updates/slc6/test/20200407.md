## 2020-04-07


Package | Advisory | Notes
------- | -------- | -----
ipmitool-1.8.15-3.el6_10 | <div class="adv_s">[S]</div>&nbsp;<a href="http://rhn.redhat.com/errata/RHSA-2020-1331.html" target="secadv">RHSA-2020:1331</a> | &nbsp;
telnet-0.17-49.el6_10 | <div class="adv_s">[S]</div>&nbsp;<a href="http://rhn.redhat.com/errata/RHSA-2020-1335.html" target="secadv">RHSA-2020:1335</a> | &nbsp;
telnet-server-0.17-49.el6_10 | <div class="adv_s">[S]</div>&nbsp;<a href="http://rhn.redhat.com/errata/RHSA-2020-1335.html" target="secadv">RHSA-2020:1335</a> | &nbsp;

