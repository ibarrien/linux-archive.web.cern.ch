## 2020-03-11

Package | Advisory | Notes
------- | -------- | -----
insights-client-3.0.13-1.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2020-0748.html" target="secadv">RHBA-2020:0748</a> | &nbsp;
nfs-utils-1.2.3-78.el6_10.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2020-0749.html" target="secadv">RHBA-2020:0749</a> | &nbsp;
qemu-guest-agent-0.12.1.2-2.506.el6_10.6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0775.html" target="secadv">RHSA-2020:0775</a> | &nbsp;
qemu-img-0.12.1.2-2.506.el6_10.6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0775.html" target="secadv">RHSA-2020:0775</a> | &nbsp;
qemu-kvm-0.12.1.2-2.506.el6_10.6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0775.html" target="secadv">RHSA-2020:0775</a> | &nbsp;
qemu-kvm-tools-0.12.1.2-2.506.el6_10.6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0775.html" target="secadv">RHSA-2020:0775</a> | &nbsp;
