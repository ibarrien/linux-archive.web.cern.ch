## 2018-02-20

Package | Advisory | Notes
------- | -------- | -----
rhev-hypervisor7-7.3-20180206.0.el6ev | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-0329.html" target="secadv">RHBA-2018:0329</a> | &nbsp;
