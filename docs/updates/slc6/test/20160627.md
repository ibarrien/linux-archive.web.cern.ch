## 2016-06-27

Package | Advisory | Notes
------- | -------- | -----
libxml2-2.7.6-21.el6_8.1 | &nbsp; &nbsp; | &nbsp;
libxml2-devel-2.7.6-21.el6_8.1 | &nbsp; &nbsp; | &nbsp;
libxml2-python-2.7.6-21.el6_8.1 | &nbsp; &nbsp; | &nbsp;
libxml2-static-2.7.6-21.el6_8.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1292.html" target="secadv">RHSA-2016:1292</a> | &nbsp;
