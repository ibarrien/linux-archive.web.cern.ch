## 2018-08-21

Package | Advisory | Notes
------- | -------- | -----
mutt-1.5.20-9.20091214hg736b6a.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2526.html" target="secadv">RHSA-2018:2526</a> | &nbsp;
