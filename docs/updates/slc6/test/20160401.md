## 2016-04-01

Package | Advisory | Notes
------- | -------- | -----
git19-emacs-git-1.9.4-4.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0497.html" target="secadv">RHSA-2016:0497</a> | &nbsp;
git19-emacs-git-el-1.9.4-4.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0497.html" target="secadv">RHSA-2016:0497</a> | &nbsp;
git19-git-1.9.4-4.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0497.html" target="secadv">RHSA-2016:0497</a> | &nbsp;
git19-git-all-1.9.4-4.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0497.html" target="secadv">RHSA-2016:0497</a> | &nbsp;
git19-git-cvs-1.9.4-4.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0497.html" target="secadv">RHSA-2016:0497</a> | &nbsp;
git19-git-daemon-1.9.4-4.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0497.html" target="secadv">RHSA-2016:0497</a> | &nbsp;
git19-git-email-1.9.4-4.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0497.html" target="secadv">RHSA-2016:0497</a> | &nbsp;
git19-git-gui-1.9.4-4.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0497.html" target="secadv">RHSA-2016:0497</a> | &nbsp;
git19-gitk-1.9.4-4.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0497.html" target="secadv">RHSA-2016:0497</a> | &nbsp;
git19-git-svn-1.9.4-4.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0497.html" target="secadv">RHSA-2016:0497</a> | &nbsp;
git19-gitweb-1.9.4-4.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0497.html" target="secadv">RHSA-2016:0497</a> | &nbsp;
git19-perl-Git-1.9.4-4.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0497.html" target="secadv">RHSA-2016:0497</a> | &nbsp;
git19-perl-Git-SVN-1.9.4-4.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0497.html" target="secadv">RHSA-2016:0497</a> | &nbsp;
java-1.7.0-openjdk-1.7.0.99-2.6.5.0.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0511.html" target="secadv">RHSA-2016:0511</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.99-2.6.5.0.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0511.html" target="secadv">RHSA-2016:0511</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.99-2.6.5.0.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0511.html" target="secadv">RHSA-2016:0511</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.99-2.6.5.0.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0511.html" target="secadv">RHSA-2016:0511</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.99-2.6.5.0.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0511.html" target="secadv">RHSA-2016:0511</a> | &nbsp;
java-1.8.0-openjdk-1.8.0.77-0.b03.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0514.html" target="secadv">RHSA-2016:0514</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.77-0.b03.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0514.html" target="secadv">RHSA-2016:0514</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.77-0.b03.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0514.html" target="secadv">RHSA-2016:0514</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.77-0.b03.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0514.html" target="secadv">RHSA-2016:0514</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.77-0.b03.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0514.html" target="secadv">RHSA-2016:0514</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.77-0.b03.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0514.html" target="secadv">RHSA-2016:0514</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.77-0.b03.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0514.html" target="secadv">RHSA-2016:0514</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.77-0.b03.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0514.html" target="secadv">RHSA-2016:0514</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.77-0.b03.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0514.html" target="secadv">RHSA-2016:0514</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.77-0.b03.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0514.html" target="secadv">RHSA-2016:0514</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.77-0.b03.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0514.html" target="secadv">RHSA-2016:0514</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.77-0.b03.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0514.html" target="secadv">RHSA-2016:0514</a> | &nbsp;
