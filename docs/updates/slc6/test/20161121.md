## 2016-11-21

Package | Advisory | Notes
------- | -------- | -----
dracut-modules-growroot-0.23-4.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2016-2701.html" target="secadv">RHEA-2016:2701</a> | &nbsp;
lcm-profile-1-20161121.slc6 | &nbsp; &nbsp; | &nbsp;
