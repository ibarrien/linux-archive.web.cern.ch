## 2014-03-25

Package | Advisory | Notes
------- | -------- | -----
CERN-CA-certs-20140325-1.slc6 | &nbsp; &nbsp; | &nbsp;
CERN-CA-certs-20140325-2.slc6 | &nbsp; &nbsp; | &nbsp;
grep-2.6.3-4.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0318.html" target="secadv">RHBA-2014:0318</a> | &nbsp;
net-snmp-5.5-49.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0321.html" target="secadv">RHSA-2014:0321</a> | &nbsp;
net-snmp-devel-5.5-49.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0321.html" target="secadv">RHSA-2014:0321</a> | &nbsp;
net-snmp-libs-5.5-49.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0321.html" target="secadv">RHSA-2014:0321</a> | &nbsp;
net-snmp-perl-5.5-49.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0321.html" target="secadv">RHSA-2014:0321</a> | &nbsp;
net-snmp-python-5.5-49.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0321.html" target="secadv">RHSA-2014:0321</a> | &nbsp;
net-snmp-utils-5.5-49.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0321.html" target="secadv">RHSA-2014:0321</a> | &nbsp;
spice-vdagent-0.14.0-3.el6_5 | &nbsp; &nbsp; | &nbsp;
