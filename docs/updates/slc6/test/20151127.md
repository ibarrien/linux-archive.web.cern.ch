## 2015-11-27

Package | Advisory | Notes
------- | -------- | -----
thunderbird-38.4.0-1.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2519.html" target="secadv">RHSA-2015:2519</a> | &nbsp;
