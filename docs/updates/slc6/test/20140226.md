## 2014-02-26

Package | Advisory | Notes
------- | -------- | -----
postgresql-8.4.20-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0211.html" target="secadv">RHSA-2014:0211</a> | &nbsp;
postgresql-contrib-8.4.20-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0211.html" target="secadv">RHSA-2014:0211</a> | &nbsp;
postgresql-devel-8.4.20-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0211.html" target="secadv">RHSA-2014:0211</a> | &nbsp;
postgresql-docs-8.4.20-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0211.html" target="secadv">RHSA-2014:0211</a> | &nbsp;
postgresql-libs-8.4.20-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0211.html" target="secadv">RHSA-2014:0211</a> | &nbsp;
postgresql-plperl-8.4.20-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0211.html" target="secadv">RHSA-2014:0211</a> | &nbsp;
postgresql-plpython-8.4.20-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0211.html" target="secadv">RHSA-2014:0211</a> | &nbsp;
postgresql-pltcl-8.4.20-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0211.html" target="secadv">RHSA-2014:0211</a> | &nbsp;
postgresql-server-8.4.20-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0211.html" target="secadv">RHSA-2014:0211</a> | &nbsp;
postgresql-test-8.4.20-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0211.html" target="secadv">RHSA-2014:0211</a> | &nbsp;
