## 2014-07-30

Package | Advisory | Notes
------- | -------- | -----
aide-0.14-7.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0948.html" target="secadv">RHBA-2014:0948</a> | &nbsp;
kernel-2.6.32-431.23.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0981.html" target="secadv">RHSA-2014:0981</a> | &nbsp;
kernel-abi-whitelists-2.6.32-431.23.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0981.html" target="secadv">RHSA-2014:0981</a> | &nbsp;
kernel-debug-2.6.32-431.23.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0981.html" target="secadv">RHSA-2014:0981</a> | &nbsp;
kernel-debug-devel-2.6.32-431.23.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0981.html" target="secadv">RHSA-2014:0981</a> | &nbsp;
kernel-devel-2.6.32-431.23.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0981.html" target="secadv">RHSA-2014:0981</a> | &nbsp;
kernel-doc-2.6.32-431.23.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0981.html" target="secadv">RHSA-2014:0981</a> | &nbsp;
kernel-firmware-2.6.32-431.23.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0981.html" target="secadv">RHSA-2014:0981</a> | &nbsp;
kernel-headers-2.6.32-431.23.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0981.html" target="secadv">RHSA-2014:0981</a> | &nbsp;
kernel-module-openafs-2.6.32-431.20.5.el6-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
mutt-1.5.20-7.20091214hg736b6a.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0945.html" target="secadv">RHBA-2014:0945</a> | &nbsp;
perf-2.6.32-431.23.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0981.html" target="secadv">RHSA-2014:0981</a> | &nbsp;
python-perf-2.6.32-431.23.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0981.html" target="secadv">RHSA-2014:0981</a> | &nbsp;
