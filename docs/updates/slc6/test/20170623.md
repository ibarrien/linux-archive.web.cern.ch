## 2017-06-23

Package | Advisory | Notes
------- | -------- | -----
rhev-hypervisor7-7.3-20170615.0.el6ev | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1568.html" target="secadv">RHBA-2017:1568</a> | &nbsp;
cern-get-keytab-1.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
protobuf3-3.3.1-2.slc6 | &nbsp; &nbsp; | &nbsp;
protobuf3-compiler-3.3.1-2.slc6 | &nbsp; &nbsp; | &nbsp;
protobuf3-devel-3.3.1-2.slc6 | &nbsp; &nbsp; | &nbsp;
protobuf3-lite-3.3.1-2.slc6 | &nbsp; &nbsp; | &nbsp;
protobuf3-lite-devel-3.3.1-2.slc6 | &nbsp; &nbsp; | &nbsp;
protobuf3-lite-static-3.3.1-2.slc6 | &nbsp; &nbsp; | &nbsp;
protobuf3-static-3.3.1-2.slc6 | &nbsp; &nbsp; | &nbsp;
protobuf3-vim-3.3.1-2.slc6 | &nbsp; &nbsp; | &nbsp;
python-protobuf3-3.3.1-2.slc6 | &nbsp; &nbsp; | &nbsp;
sudo-1.8.6p3-29.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1574.html" target="secadv">RHSA-2017:1574</a> | &nbsp;
sudo-devel-1.8.6p3-29.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1574.html" target="secadv">RHSA-2017:1574</a> | &nbsp;
