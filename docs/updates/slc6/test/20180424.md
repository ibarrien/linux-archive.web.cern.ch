## 2018-04-24

Package | Advisory | Notes
------- | -------- | -----
cx_Oracle-5.1.3-1.slc6 | &nbsp; &nbsp; | &nbsp;
flash-plugin-29.0.0.140-1.slc6 | &nbsp; &nbsp; | &nbsp;
cern-get-certificate-0.9.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
patch-2.6-8.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1199.html" target="secadv">RHSA-2018:1199</a> | &nbsp;
