## 2017-07-12

Package | Advisory | Notes
------- | -------- | -----
cloud-init-0.7.5-8.el6_9 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1722.html" target="secadv">RHBA-2017:1722</a> | &nbsp;
createrepo-0.9.9-27.el6_9 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1729.html" target="secadv">RHBA-2017:1729</a> | &nbsp;
httpd-2.2.15-60.el6_9.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1721.html" target="secadv">RHSA-2017:1721</a> | &nbsp;
httpd-devel-2.2.15-60.el6_9.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1721.html" target="secadv">RHSA-2017:1721</a> | &nbsp;
httpd-manual-2.2.15-60.el6_9.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1721.html" target="secadv">RHSA-2017:1721</a> | &nbsp;
httpd-tools-2.2.15-60.el6_9.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1721.html" target="secadv">RHSA-2017:1721</a> | &nbsp;
iscsi-initiator-utils-6.2.0.873-27.el6_9 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1725.html" target="secadv">RHBA-2017:1725</a> | &nbsp;
iscsi-initiator-utils-devel-6.2.0.873-27.el6_9 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1725.html" target="secadv">RHBA-2017:1725</a> | &nbsp;
kernel-2.6.32-696.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1723.html" target="secadv">RHSA-2017:1723</a> | &nbsp;
kernel-abi-whitelists-2.6.32-696.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1723.html" target="secadv">RHSA-2017:1723</a> | &nbsp;
kernel-debug-2.6.32-696.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1723.html" target="secadv">RHSA-2017:1723</a> | &nbsp;
kernel-debug-devel-2.6.32-696.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1723.html" target="secadv">RHSA-2017:1723</a> | &nbsp;
kernel-devel-2.6.32-696.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1723.html" target="secadv">RHSA-2017:1723</a> | &nbsp;
kernel-doc-2.6.32-696.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1723.html" target="secadv">RHSA-2017:1723</a> | &nbsp;
kernel-firmware-2.6.32-696.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1723.html" target="secadv">RHSA-2017:1723</a> | &nbsp;
kernel-headers-2.6.32-696.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1723.html" target="secadv">RHSA-2017:1723</a> | &nbsp;
kernel-module-openafs-2.6.32-696.6.3.el6-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
ksh-20120801-35.el6_9 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1728.html" target="secadv">RHBA-2017:1728</a> | &nbsp;
mod_ssl-2.2.15-60.el6_9.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1721.html" target="secadv">RHSA-2017:1721</a> | &nbsp;
perf-2.6.32-696.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1723.html" target="secadv">RHSA-2017:1723</a> | &nbsp;
procps-3.2.8-45.el6_9.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1726.html" target="secadv">RHBA-2017:1726</a> | &nbsp;
procps-devel-3.2.8-45.el6_9.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1726.html" target="secadv">RHBA-2017:1726</a> | &nbsp;
python-perf-2.6.32-696.6.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1723.html" target="secadv">RHSA-2017:1723</a> | &nbsp;
python-rhsm-1.18.7-1.el6_9 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1724.html" target="secadv">RHBA-2017:1724</a> | &nbsp;
python-rhsm-certificates-1.18.7-1.el6_9 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1724.html" target="secadv">RHBA-2017:1724</a> | &nbsp;
selinux-policy-3.7.19-307.el6_9.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1727.html" target="secadv">RHBA-2017:1727</a> | &nbsp;
selinux-policy-doc-3.7.19-307.el6_9.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1727.html" target="secadv">RHBA-2017:1727</a> | &nbsp;
selinux-policy-minimum-3.7.19-307.el6_9.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1727.html" target="secadv">RHBA-2017:1727</a> | &nbsp;
selinux-policy-mls-3.7.19-307.el6_9.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1727.html" target="secadv">RHBA-2017:1727</a> | &nbsp;
selinux-policy-targeted-3.7.19-307.el6_9.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1727.html" target="secadv">RHBA-2017:1727</a> | &nbsp;
