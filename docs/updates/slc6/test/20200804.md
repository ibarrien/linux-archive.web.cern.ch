## 2020-08-04


Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-693.71.2.rt56.670.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3266.html" target="secadv">RHSA-2020:3266</a> | &nbsp;
kernel-rt-debug-3.10.0-693.71.2.rt56.670.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3266.html" target="secadv">RHSA-2020:3266</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-693.71.2.rt56.670.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3266.html" target="secadv">RHSA-2020:3266</a> | &nbsp;
kernel-rt-devel-3.10.0-693.71.2.rt56.670.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3266.html" target="secadv">RHSA-2020:3266</a> | &nbsp;
kernel-rt-doc-3.10.0-693.71.2.rt56.670.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3266.html" target="secadv">RHSA-2020:3266</a> | &nbsp;
kernel-rt-firmware-3.10.0-693.71.2.rt56.670.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3266.html" target="secadv">RHSA-2020:3266</a> | &nbsp;
kernel-rt-trace-3.10.0-693.71.2.rt56.670.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3266.html" target="secadv">RHSA-2020:3266</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-693.71.2.rt56.670.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3266.html" target="secadv">RHSA-2020:3266</a> | &nbsp;
kernel-rt-vanilla-3.10.0-693.71.2.rt56.670.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3266.html" target="secadv">RHSA-2020:3266</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-693.71.2.rt56.670.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3266.html" target="secadv">RHSA-2020:3266</a> | &nbsp;
postgresql-jdbc-8.4.704-4.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3284.html" target="secadv">RHSA-2020:3284</a> | &nbsp;

