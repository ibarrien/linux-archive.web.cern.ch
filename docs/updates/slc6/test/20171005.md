## 2017-10-05

Package | Advisory | Notes
------- | -------- | -----
postgresql-8.4.20-8.el6_9 | &nbsp; &nbsp; | &nbsp;
postgresql-contrib-8.4.20-8.el6_9 | &nbsp; &nbsp; | &nbsp;
postgresql-devel-8.4.20-8.el6_9 | &nbsp; &nbsp; | &nbsp;
postgresql-docs-8.4.20-8.el6_9 | &nbsp; &nbsp; | &nbsp;
postgresql-libs-8.4.20-8.el6_9 | &nbsp; &nbsp; | &nbsp;
postgresql-plperl-8.4.20-8.el6_9 | &nbsp; &nbsp; | &nbsp;
postgresql-plpython-8.4.20-8.el6_9 | &nbsp; &nbsp; | &nbsp;
postgresql-pltcl-8.4.20-8.el6_9 | &nbsp; &nbsp; | &nbsp;
postgresql-server-8.4.20-8.el6_9 | &nbsp; &nbsp; | &nbsp;
postgresql-test-8.4.20-8.el6_9 | &nbsp; &nbsp; | &nbsp;
