## 2017-05-16

Package | Advisory | Notes
------- | -------- | -----
ghostscript-8.70-23.el6_9.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1230.html" target="secadv">RHSA-2017:1230</a> | &nbsp;
ghostscript-devel-8.70-23.el6_9.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1230.html" target="secadv">RHSA-2017:1230</a> | &nbsp;
ghostscript-doc-8.70-23.el6_9.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1230.html" target="secadv">RHSA-2017:1230</a> | &nbsp;
ghostscript-gtk-8.70-23.el6_9.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1230.html" target="secadv">RHSA-2017:1230</a> | &nbsp;
