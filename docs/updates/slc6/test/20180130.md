## 2018-01-30

Package | Advisory | Notes
------- | -------- | -----
sclo-php56-php-pecl-redis-3.1.6-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-apcu-5.1.9-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-apcu-devel-5.1.9-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-redis-3.1.6-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-ssh2-1.1.2-1.el6 | &nbsp; &nbsp; | &nbsp;
tzdata-2018c-1.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2018-0232.html" target="secadv">RHEA-2018:0232</a> | &nbsp;
tzdata-java-2018c-1.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2018-0232.html" target="secadv">RHEA-2018:0232</a> | &nbsp;
