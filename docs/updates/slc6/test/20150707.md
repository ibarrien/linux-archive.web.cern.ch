## 2015-07-07

Package | Advisory | Notes
------- | -------- | -----
abrt-2.0.8-26.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
abrt-addon-ccpp-2.0.8-26.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
abrt-addon-kerneloops-2.0.8-26.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
abrt-addon-python-2.0.8-26.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
abrt-addon-vmcore-2.0.8-26.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
abrt-cli-2.0.8-26.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
abrt-console-notification-2.0.8-26.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
abrt-desktop-2.0.8-26.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
abrt-devel-2.0.8-26.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
abrt-gui-2.0.8-26.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
abrt-libs-2.0.8-26.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
abrt-python-2.0.8-26.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
abrt-tui-2.0.8-26.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-cli-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-compat-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-devel-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-filesystem-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-gtk-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-gtk-devel-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-newt-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-plugin-bugzilla-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-plugin-kerneloops-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-plugin-logger-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-plugin-mailx-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-plugin-reportuploader-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-plugin-rhtsupport-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
libreport-python-2.0.9-21.el6_6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1210.html" target="secadv">RHSA-2015:1210</a> | &nbsp;
