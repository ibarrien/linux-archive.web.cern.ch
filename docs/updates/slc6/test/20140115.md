## 2014-01-15

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-11.2.202.335-1.slc6 | &nbsp; &nbsp; | &nbsp;
cern-java-deployment-ruleset-0.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-openjdk-1.7.0.51-2.4.4.1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0026.html" target="secadv">RHSA-2014:0026</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.51-2.4.4.1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0026.html" target="secadv">RHSA-2014:0026</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.51-2.4.4.1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0026.html" target="secadv">RHSA-2014:0026</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.51-2.4.4.1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0026.html" target="secadv">RHSA-2014:0026</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.51-2.4.4.1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0026.html" target="secadv">RHSA-2014:0026</a> | &nbsp;
java-1.7.0-oracle-1.7.0.51-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-devel-1.7.0.51-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-jdbc-1.7.0.51-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-plugin-1.7.0.51-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-src-1.7.0.51-1.slc6 | &nbsp; &nbsp; | &nbsp;
resource-agents-3.9.2-40.el6_5.5 | &nbsp; &nbsp; | &nbsp;
resource-agents-sap-3.9.2-40.el6_5.5 | &nbsp; &nbsp; | &nbsp;
