## 2014-11-06

Package | Advisory | Notes
------- | -------- | -----
java-1.6.0-openjdk-1.6.0.33-1.13.5.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1804.html" target="secadv">RHBA-2014:1804</a> | &nbsp;
java-1.6.0-openjdk-demo-1.6.0.33-1.13.5.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1804.html" target="secadv">RHBA-2014:1804</a> | &nbsp;
java-1.6.0-openjdk-devel-1.6.0.33-1.13.5.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1804.html" target="secadv">RHBA-2014:1804</a> | &nbsp;
java-1.6.0-openjdk-javadoc-1.6.0.33-1.13.5.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1804.html" target="secadv">RHBA-2014:1804</a> | &nbsp;
java-1.6.0-openjdk-src-1.6.0.33-1.13.5.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1804.html" target="secadv">RHBA-2014:1804</a> | &nbsp;
java-1.7.0-oracle-1.7.0.72-2.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-devel-1.7.0.72-2.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-jdbc-1.7.0.72-2.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-plugin-1.7.0.72-2.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-src-1.7.0.72-2.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-1.8.0.25-2.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-1.8.0.25-3.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.25-2.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.25-3.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.25-2.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.25-3.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.25-2.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.25-3.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.25-2.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.25-3.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.25-2.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.25-3.0.slc6 | &nbsp; &nbsp; | &nbsp;
mod_auth_mellon-0.8.0-3.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1803.html" target="secadv">RHSA-2014:1803</a> | &nbsp;
nfs-utils-1.2.3-39.el6_5.4 | &nbsp; &nbsp; | &nbsp;
zsh-4.3.10-9.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1806.html" target="secadv">RHBA-2014:1806</a> | &nbsp;
zsh-html-4.3.10-9.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1806.html" target="secadv">RHBA-2014:1806</a> | &nbsp;
