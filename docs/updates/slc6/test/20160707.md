## 2016-07-07

Package | Advisory | Notes
------- | -------- | -----
tzdata-2016f-1.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2016-1388.html" target="secadv">RHEA-2016:1388</a> | &nbsp;
tzdata-java-2016f-1.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2016-1388.html" target="secadv">RHEA-2016:1388</a> | &nbsp;
