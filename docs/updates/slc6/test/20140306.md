## 2014-03-06

Package | Advisory | Notes
------- | -------- | -----
kernel-module-openafs-2.6.32-431.1.2.el6-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
kernel-module-openafs-2.6.32-431.3.1.el6-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
kernel-module-openafs-2.6.32-431.5.1.el6-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
mod_dav_svn-1.6.11-10.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0255.html" target="secadv">RHSA-2014:0255</a> | &nbsp;
openafs-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-authlibs-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-authlibs-devel-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-client-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-compat-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-devel-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-docs-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-kernel-source-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-kpasswd-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-krb5-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-server-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
subversion-1.6.11-10.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0255.html" target="secadv">RHSA-2014:0255</a> | &nbsp;
subversion-devel-1.6.11-10.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0255.html" target="secadv">RHSA-2014:0255</a> | &nbsp;
subversion-gnome-1.6.11-10.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0255.html" target="secadv">RHSA-2014:0255</a> | &nbsp;
subversion-javahl-1.6.11-10.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0255.html" target="secadv">RHSA-2014:0255</a> | &nbsp;
subversion-kde-1.6.11-10.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0255.html" target="secadv">RHSA-2014:0255</a> | &nbsp;
subversion-perl-1.6.11-10.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0255.html" target="secadv">RHSA-2014:0255</a> | &nbsp;
subversion-ruby-1.6.11-10.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0255.html" target="secadv">RHSA-2014:0255</a> | &nbsp;
subversion-svn2cl-1.6.11-10.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0255.html" target="secadv">RHSA-2014:0255</a> | &nbsp;
