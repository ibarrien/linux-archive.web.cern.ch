## 2020-05-11


Package | Advisory | Notes
------- | -------- | -----
thunderbird-68.8.0-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2049.html" target="secadv">RHSA-2020:2049</a> | &nbsp;

