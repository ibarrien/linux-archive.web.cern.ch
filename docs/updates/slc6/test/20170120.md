## 2017-01-20

Package | Advisory | Notes
------- | -------- | -----
rt-setup-1.60-19.el6rt | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2017-0178.html" target="secadv">RHEA-2017:0178</a> | &nbsp;
