## 2014-09-17

Package | Advisory | Notes
------- | -------- | -----
axis-1.2.1-7.5.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1193.html" target="secadv">RHSA-2014:1193</a> | &nbsp;
axis-javadoc-1.2.1-7.5.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1193.html" target="secadv">RHSA-2014:1193</a> | &nbsp;
axis-manual-1.2.1-7.5.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1193.html" target="secadv">RHSA-2014:1193</a> | &nbsp;
kexec-tools-2.0.0-273.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1192.html" target="secadv">RHBA-2014:1192</a> | &nbsp;
kmod-lin_tape-2.9.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
lin_taped-2.9.1-1 | &nbsp; &nbsp; | &nbsp;
net-snmp-5.5-49.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1189.html" target="secadv">RHBA-2014:1189</a> | &nbsp;
net-snmp-devel-5.5-49.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1189.html" target="secadv">RHBA-2014:1189</a> | &nbsp;
net-snmp-libs-5.5-49.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1189.html" target="secadv">RHBA-2014:1189</a> | &nbsp;
net-snmp-perl-5.5-49.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1189.html" target="secadv">RHBA-2014:1189</a> | &nbsp;
net-snmp-python-5.5-49.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1189.html" target="secadv">RHBA-2014:1189</a> | &nbsp;
net-snmp-utils-5.5-49.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1189.html" target="secadv">RHBA-2014:1189</a> | &nbsp;
tzdata-2014f-1.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2014-1242.html" target="secadv">RHEA-2014:1242</a> | &nbsp;
tzdata-java-2014f-1.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2014-1242.html" target="secadv">RHEA-2014:1242</a> | &nbsp;
