## 2019-04-18

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-32.0.0.171-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-1.8.0.212.b04-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0774.html" target="secadv">RHSA-2019:0774</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.212.b04-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0774.html" target="secadv">RHSA-2019:0774</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.212.b04-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0774.html" target="secadv">RHSA-2019:0774</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.212.b04-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0774.html" target="secadv">RHSA-2019:0774</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.212.b04-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0774.html" target="secadv">RHSA-2019:0774</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.212.b04-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0774.html" target="secadv">RHSA-2019:0774</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.212.b04-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0774.html" target="secadv">RHSA-2019:0774</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.212.b04-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0774.html" target="secadv">RHSA-2019:0774</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.212.b04-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0774.html" target="secadv">RHSA-2019:0774</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.212.b04-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0774.html" target="secadv">RHSA-2019:0774</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.212.b04-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0774.html" target="secadv">RHSA-2019:0774</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.212.b04-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0774.html" target="secadv">RHSA-2019:0774</a> | &nbsp;
