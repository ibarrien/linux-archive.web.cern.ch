## 2016-06-29

Package | Advisory | Notes
------- | -------- | -----
rt-setup-1.60-13.el6rt | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2016-1350.html" target="secadv">RHEA-2016:1350</a> | &nbsp;
