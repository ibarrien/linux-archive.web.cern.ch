## 2018-05-09

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-693.25.4.rt56.613.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1354.html" target="secadv">RHSA-2018:1354</a> | &nbsp;
kernel-rt-debug-3.10.0-693.25.4.rt56.613.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1354.html" target="secadv">RHSA-2018:1354</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-693.25.4.rt56.613.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1354.html" target="secadv">RHSA-2018:1354</a> | &nbsp;
kernel-rt-devel-3.10.0-693.25.4.rt56.613.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1354.html" target="secadv">RHSA-2018:1354</a> | &nbsp;
kernel-rt-doc-3.10.0-693.25.4.rt56.613.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1354.html" target="secadv">RHSA-2018:1354</a> | &nbsp;
kernel-rt-firmware-3.10.0-693.25.4.rt56.613.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1354.html" target="secadv">RHSA-2018:1354</a> | &nbsp;
kernel-rt-trace-3.10.0-693.25.4.rt56.613.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1354.html" target="secadv">RHSA-2018:1354</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-693.25.4.rt56.613.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1354.html" target="secadv">RHSA-2018:1354</a> | &nbsp;
kernel-rt-vanilla-3.10.0-693.25.4.rt56.613.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1354.html" target="secadv">RHSA-2018:1354</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-693.25.4.rt56.613.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1354.html" target="secadv">RHSA-2018:1354</a> | &nbsp;
kernel-2.6.32-696.28.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1319.html" target="secadv">RHSA-2018:1319</a> | &nbsp;
kernel-abi-whitelists-2.6.32-696.28.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1319.html" target="secadv">RHSA-2018:1319</a> | &nbsp;
kernel-debug-2.6.32-696.28.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1319.html" target="secadv">RHSA-2018:1319</a> | &nbsp;
kernel-debug-devel-2.6.32-696.28.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1319.html" target="secadv">RHSA-2018:1319</a> | &nbsp;
kernel-devel-2.6.32-696.28.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1319.html" target="secadv">RHSA-2018:1319</a> | &nbsp;
kernel-doc-2.6.32-696.28.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1319.html" target="secadv">RHSA-2018:1319</a> | &nbsp;
kernel-firmware-2.6.32-696.28.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1319.html" target="secadv">RHSA-2018:1319</a> | &nbsp;
kernel-headers-2.6.32-696.28.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1319.html" target="secadv">RHSA-2018:1319</a> | &nbsp;
kernel-module-openafs-2.6.32-696.28.1.el6-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
lpadmincern-1.3.21-1.slc6 | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-696.28.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1319.html" target="secadv">RHSA-2018:1319</a> | &nbsp;
python-perf-2.6.32-696.28.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-1319.html" target="secadv">RHSA-2018:1319</a> | &nbsp;
tzdata-2018e-3.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-1339.html" target="secadv">RHBA-2018:1339</a> | &nbsp;
tzdata-java-2018e-3.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2018-1339.html" target="secadv">RHBA-2018:1339</a> | &nbsp;
