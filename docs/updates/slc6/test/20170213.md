## 2017-02-13

Package | Advisory | Notes
------- | -------- | -----
cern-get-keytab-1.0.0-1.slc6 | &nbsp; &nbsp; | &nbsp;
msktutil-1.0-0.git.slc6 | &nbsp; &nbsp; | &nbsp;
