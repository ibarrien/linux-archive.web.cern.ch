## 2015-01-21

Package | Advisory | Notes
------- | -------- | -----
chromium-browser-39.0.2171.99-1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0061.html" target="secadv">RHBA-2015:0061</a> | &nbsp;
java-1.7.0-openjdk-1.7.0.75-2.5.4.0.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0067.html" target="secadv">RHSA-2015:0067</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.75-2.5.4.0.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0067.html" target="secadv">RHSA-2015:0067</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.75-2.5.4.0.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0067.html" target="secadv">RHSA-2015:0067</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.75-2.5.4.0.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0067.html" target="secadv">RHSA-2015:0067</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.75-2.5.4.0.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0067.html" target="secadv">RHSA-2015:0067</a> | &nbsp;
java-1.8.0-openjdk-1.8.0.31-1.b13.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0069.html" target="secadv">RHSA-2015:0069</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.31-1.b13.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0069.html" target="secadv">RHSA-2015:0069</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.31-1.b13.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0069.html" target="secadv">RHSA-2015:0069</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.31-1.b13.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0069.html" target="secadv">RHSA-2015:0069</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.31-1.b13.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0069.html" target="secadv">RHSA-2015:0069</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.31-1.b13.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0069.html" target="secadv">RHSA-2015:0069</a> | &nbsp;
openssl-1.0.1e-30.el6_6.5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0066.html" target="secadv">RHSA-2015:0066</a> | &nbsp;
openssl-devel-1.0.1e-30.el6_6.5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0066.html" target="secadv">RHSA-2015:0066</a> | &nbsp;
openssl-perl-1.0.1e-30.el6_6.5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0066.html" target="secadv">RHSA-2015:0066</a> | &nbsp;
openssl-static-1.0.1e-30.el6_6.5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0066.html" target="secadv">RHSA-2015:0066</a> | &nbsp;
selinux-policy-3.7.19-260.el6_6.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0059.html" target="secadv">RHBA-2015:0059</a> | &nbsp;
selinux-policy-doc-3.7.19-260.el6_6.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0059.html" target="secadv">RHBA-2015:0059</a> | &nbsp;
selinux-policy-minimum-3.7.19-260.el6_6.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0059.html" target="secadv">RHBA-2015:0059</a> | &nbsp;
selinux-policy-mls-3.7.19-260.el6_6.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0059.html" target="secadv">RHBA-2015:0059</a> | &nbsp;
selinux-policy-targeted-3.7.19-260.el6_6.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0059.html" target="secadv">RHBA-2015:0059</a> | &nbsp;
sl-release-6.6-2.slc6.nonpae | &nbsp; &nbsp; | &nbsp;
