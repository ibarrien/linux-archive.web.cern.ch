## 2015-12-10

Package | Advisory | Notes
------- | -------- | -----
rt-tests-0.93-1.el6rt | &nbsp; &nbsp; | &nbsp;
afs_tools-2.1-3.slc6 | &nbsp; &nbsp; | &nbsp;
afs_tools_standalone-2.1-3.slc6 | &nbsp; &nbsp; | &nbsp;
flash-plugin-11.2.202.554-1.slc6 | &nbsp; &nbsp; | &nbsp;
libpng-1.2.49-2.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2594.html" target="secadv">RHSA-2015:2594</a> | &nbsp;
libpng-devel-1.2.49-2.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2594.html" target="secadv">RHSA-2015:2594</a> | &nbsp;
libpng-static-1.2.49-2.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2594.html" target="secadv">RHSA-2015:2594</a> | &nbsp;
libxml2-2.7.6-20.el6_7.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2549.html" target="secadv">RHSA-2015:2549</a> | &nbsp;
libxml2-devel-2.7.6-20.el6_7.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2549.html" target="secadv">RHSA-2015:2549</a> | &nbsp;
libxml2-python-2.7.6-20.el6_7.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2549.html" target="secadv">RHSA-2015:2549</a> | &nbsp;
libxml2-static-2.7.6-20.el6_7.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-2549.html" target="secadv">RHSA-2015:2549</a> | &nbsp;
