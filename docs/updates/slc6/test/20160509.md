## 2016-05-09

Package | Advisory | Notes
------- | -------- | -----
debugmode-9.03.49-1.el6_7.5 | &nbsp; &nbsp; | &nbsp;
initscripts-9.03.49-1.el6_7.5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-0713.html" target="secadv">RHBA-2016:0713</a> | &nbsp;
kdebase-workspace-4.3.4-33.el6_7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-0717.html" target="secadv">RHBA-2016:0717</a> | &nbsp;
kdebase-workspace-akonadi-4.3.4-33.el6_7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-0717.html" target="secadv">RHBA-2016:0717</a> | &nbsp;
kdebase-workspace-devel-4.3.4-33.el6_7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-0717.html" target="secadv">RHBA-2016:0717</a> | &nbsp;
kdebase-workspace-libs-4.3.4-33.el6_7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-0717.html" target="secadv">RHBA-2016:0717</a> | &nbsp;
kdebase-workspace-python-applet-4.3.4-33.el6_7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-0717.html" target="secadv">RHBA-2016:0717</a> | &nbsp;
kdebase-workspace-wallpapers-4.3.4-33.el6_7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-0717.html" target="secadv">RHBA-2016:0717</a> | &nbsp;
kdm-4.3.4-33.el6_7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-0717.html" target="secadv">RHBA-2016:0717</a> | &nbsp;
ksysguardd-4.3.4-33.el6_7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-0717.html" target="secadv">RHBA-2016:0717</a> | &nbsp;
mod_nss-1.0.10-2.el6_7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-0714.html" target="secadv">RHBA-2016:0714</a> | &nbsp;
oxygen-cursor-themes-4.3.4-33.el6_7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-0717.html" target="secadv">RHBA-2016:0717</a> | &nbsp;
