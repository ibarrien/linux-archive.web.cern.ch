## 2015-05-20

Package | Advisory | Notes
------- | -------- | -----
bind-9.8.2-0.30.rc1.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1016.html" target="secadv">RHBA-2015:1016</a> | &nbsp;
bind-chroot-9.8.2-0.30.rc1.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1016.html" target="secadv">RHBA-2015:1016</a> | &nbsp;
bind-devel-9.8.2-0.30.rc1.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1016.html" target="secadv">RHBA-2015:1016</a> | &nbsp;
bind-libs-9.8.2-0.30.rc1.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1016.html" target="secadv">RHBA-2015:1016</a> | &nbsp;
bind-sdb-9.8.2-0.30.rc1.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1016.html" target="secadv">RHBA-2015:1016</a> | &nbsp;
bind-utils-9.8.2-0.30.rc1.el6_6.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-1016.html" target="secadv">RHBA-2015:1016</a> | &nbsp;
cern-get-sso-cookie-0.5.3-2.slc6 | &nbsp; &nbsp; | &nbsp;
perl-WWW-CERNSSO-Auth-0.5.3-2.slc6 | &nbsp; &nbsp; | &nbsp;
