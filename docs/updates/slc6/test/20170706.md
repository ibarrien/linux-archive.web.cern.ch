## 2017-07-06

Package | Advisory | Notes
------- | -------- | -----
bind-9.8.2-0.62.rc1.el6_9.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1679.html" target="secadv">RHSA-2017:1679</a> | &nbsp;
bind-chroot-9.8.2-0.62.rc1.el6_9.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1679.html" target="secadv">RHSA-2017:1679</a> | &nbsp;
bind-devel-9.8.2-0.62.rc1.el6_9.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1679.html" target="secadv">RHSA-2017:1679</a> | &nbsp;
bind-libs-9.8.2-0.62.rc1.el6_9.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1679.html" target="secadv">RHSA-2017:1679</a> | &nbsp;
bind-sdb-9.8.2-0.62.rc1.el6_9.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1679.html" target="secadv">RHSA-2017:1679</a> | &nbsp;
bind-utils-9.8.2-0.62.rc1.el6_9.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-1679.html" target="secadv">RHSA-2017:1679</a> | &nbsp;
