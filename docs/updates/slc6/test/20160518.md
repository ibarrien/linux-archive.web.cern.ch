## 2016-05-18

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-11.2.202.621-1.slc6 | &nbsp; &nbsp; | &nbsp;
rt-setup-1.60-12.el6rt | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2016-1085.html" target="secadv">RHEA-2016:1085</a> | &nbsp;
