## 2014-08-13

Package | Advisory | Notes
------- | -------- | -----
rt-setup-1.55-8.el6rt | &nbsp; &nbsp; | &nbsp;
hwloc-1.5-2.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1048.html" target="secadv">RHBA-2014:1048</a> | &nbsp;
hwloc-devel-1.5-2.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1048.html" target="secadv">RHBA-2014:1048</a> | &nbsp;
