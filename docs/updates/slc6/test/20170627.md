## 2017-06-27

Package | Advisory | Notes
------- | -------- | -----
cern-get-sso-cookie-0.6-1.slc6 | &nbsp; &nbsp; | &nbsp;
perl-WWW-CERNSSO-Auth-0.6-1.slc6 | &nbsp; &nbsp; | &nbsp;
