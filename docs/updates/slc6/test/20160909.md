## 2016-09-09

Package | Advisory | Notes
------- | -------- | -----
libgudev1-147-2.73.el6_8.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1819.html" target="secadv">RHBA-2016:1819</a> | &nbsp;
libgudev1-devel-147-2.73.el6_8.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1819.html" target="secadv">RHBA-2016:1819</a> | &nbsp;
libudev-147-2.73.el6_8.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1819.html" target="secadv">RHBA-2016:1819</a> | &nbsp;
libudev-devel-147-2.73.el6_8.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1819.html" target="secadv">RHBA-2016:1819</a> | &nbsp;
thunderbird-exchangecalendar-3.8.0-1.slc6 | &nbsp; &nbsp; | &nbsp;
udev-147-2.73.el6_8.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1819.html" target="secadv">RHBA-2016:1819</a> | &nbsp;
