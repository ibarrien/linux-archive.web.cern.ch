## 2015-10-09

Package | Advisory | Notes
------- | -------- | -----
rt-setup-1.60-8.el6rt | &nbsp; &nbsp; | &nbsp;
kmod-lin_tape-3.0.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
lin_taped-3.0.1-1 | &nbsp; &nbsp; | &nbsp;
thunderbird-exchangecalendar-3.4.0-1.slc6 | &nbsp; &nbsp; | &nbsp;
tzdata-2015g-2.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-1863.html" target="secadv">RHEA-2015:1863</a> | &nbsp;
tzdata-java-2015g-2.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-1863.html" target="secadv">RHEA-2015:1863</a> | &nbsp;
