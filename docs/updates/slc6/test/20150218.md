## 2015-02-18

Package | Advisory | Notes
------- | -------- | -----
bind-dyndb-ldap-2.3-6.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0231.html" target="secadv">RHBA-2015:0231</a> | &nbsp;
CERN-CA-certs-20150218-1.slc6 | &nbsp; &nbsp; | &nbsp;
cern-get-certificate-0.7-1.slc6 | &nbsp; &nbsp; | &nbsp;
system-config-printer-1.1.16-25.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0224.html" target="secadv">RHBA-2015:0224</a> | &nbsp;
system-config-printer-libs-1.1.16-25.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0224.html" target="secadv">RHBA-2015:0224</a> | &nbsp;
system-config-printer-udev-1.1.16-25.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0224.html" target="secadv">RHBA-2015:0224</a> | &nbsp;
