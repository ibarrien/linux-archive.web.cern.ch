## 2014-10-07

Package | Advisory | Notes
------- | -------- | -----
libxml-security-c17-1.7.2-2.3.slc6 | &nbsp; &nbsp; | &nbsp;
libxml-security-c-devel-1.7.2-2.3.slc6 | &nbsp; &nbsp; | &nbsp;
perl-DBD-Oracle-1.52-2.slc6 | &nbsp; &nbsp; | &nbsp;
shibboleth-2.5.3-1.4.slc6 | &nbsp; &nbsp; | &nbsp;
shibboleth-devel-2.5.3-1.4.slc6 | &nbsp; &nbsp; | &nbsp;
xml-security-c-bin-1.7.2-2.3.slc6 | &nbsp; &nbsp; | &nbsp;
