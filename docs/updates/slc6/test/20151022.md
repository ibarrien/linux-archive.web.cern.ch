## 2015-10-22

Package | Advisory | Notes
------- | -------- | -----
java-1.7.0-openjdk-1.7.0.91-2.6.2.2.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1920.html" target="secadv">RHSA-2015:1920</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.91-2.6.2.2.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1920.html" target="secadv">RHSA-2015:1920</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.91-2.6.2.2.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1920.html" target="secadv">RHSA-2015:1920</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.91-2.6.2.2.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1920.html" target="secadv">RHSA-2015:1920</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.91-2.6.2.2.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1920.html" target="secadv">RHSA-2015:1920</a> | &nbsp;
java-1.8.0-openjdk-1.8.0.65-0.b17.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1919.html" target="secadv">RHSA-2015:1919</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.65-0.b17.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1919.html" target="secadv">RHSA-2015:1919</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.65-0.b17.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1919.html" target="secadv">RHSA-2015:1919</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.65-0.b17.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1919.html" target="secadv">RHSA-2015:1919</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.65-0.b17.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1919.html" target="secadv">RHSA-2015:1919</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.65-0.b17.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1919.html" target="secadv">RHSA-2015:1919</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.65-0.b17.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1919.html" target="secadv">RHSA-2015:1919</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.65-0.b17.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1919.html" target="secadv">RHSA-2015:1919</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.65-0.b17.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1919.html" target="secadv">RHSA-2015:1919</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.65-0.b17.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1919.html" target="secadv">RHSA-2015:1919</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.65-0.b17.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1919.html" target="secadv">RHSA-2015:1919</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.65-0.b17.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1919.html" target="secadv">RHSA-2015:1919</a> | &nbsp;
java-1.8.0-oracle-1.8.0.66-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.66-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.66-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.66-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.66-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.66-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
libwmf-0.2.8.4-25.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1917.html" target="secadv">RHSA-2015:1917</a> | &nbsp;
libwmf-devel-0.2.8.4-25.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1917.html" target="secadv">RHSA-2015:1917</a> | &nbsp;
libwmf-lite-0.2.8.4-25.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1917.html" target="secadv">RHSA-2015:1917</a> | &nbsp;
