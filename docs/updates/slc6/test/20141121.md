## 2014-11-21

Package | Advisory | Notes
------- | -------- | -----
cyrus-sasl-2.1.23-15.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1884.html" target="secadv">RHBA-2014:1884</a> | &nbsp;
cyrus-sasl-devel-2.1.23-15.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1884.html" target="secadv">RHBA-2014:1884</a> | &nbsp;
cyrus-sasl-gssapi-2.1.23-15.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1884.html" target="secadv">RHBA-2014:1884</a> | &nbsp;
cyrus-sasl-ldap-2.1.23-15.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1884.html" target="secadv">RHBA-2014:1884</a> | &nbsp;
cyrus-sasl-lib-2.1.23-15.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1884.html" target="secadv">RHBA-2014:1884</a> | &nbsp;
cyrus-sasl-md5-2.1.23-15.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1884.html" target="secadv">RHBA-2014:1884</a> | &nbsp;
cyrus-sasl-ntlm-2.1.23-15.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1884.html" target="secadv">RHBA-2014:1884</a> | &nbsp;
cyrus-sasl-plain-2.1.23-15.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1884.html" target="secadv">RHBA-2014:1884</a> | &nbsp;
cyrus-sasl-sql-2.1.23-15.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1884.html" target="secadv">RHBA-2014:1884</a> | &nbsp;
device-mapper-multipath-0.4.9-80.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1875.html" target="secadv">RHBA-2014:1875</a> | &nbsp;
device-mapper-multipath-libs-0.4.9-80.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1875.html" target="secadv">RHBA-2014:1875</a> | &nbsp;
gimp-2.6.9-8.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1883.html" target="secadv">RHBA-2014:1883</a> | &nbsp;
gimp-devel-2.6.9-8.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1883.html" target="secadv">RHBA-2014:1883</a> | &nbsp;
gimp-devel-tools-2.6.9-8.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1883.html" target="secadv">RHBA-2014:1883</a> | &nbsp;
gimp-help-browser-2.6.9-8.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1883.html" target="secadv">RHBA-2014:1883</a> | &nbsp;
gimp-libs-2.6.9-8.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1883.html" target="secadv">RHBA-2014:1883</a> | &nbsp;
kpartx-0.4.9-80.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1875.html" target="secadv">RHBA-2014:1875</a> | &nbsp;
webkitgtk-1.4.3-9.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1883.html" target="secadv">RHBA-2014:1883</a> | &nbsp;
webkitgtk-devel-1.4.3-9.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1883.html" target="secadv">RHBA-2014:1883</a> | &nbsp;
webkitgtk-doc-1.4.3-9.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1883.html" target="secadv">RHBA-2014:1883</a> | &nbsp;
