## 2020-03-06

Package | Advisory | Notes
------- | -------- | -----
sudo-1.8.6p3-29.el6_10.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0726.html" target="secadv">RHSA-2020:0726</a> | &nbsp;
sudo-devel-1.8.6p3-29.el6_10.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0726.html" target="secadv">RHSA-2020:0726</a> | &nbsp;
