## 2019-01-28

Package | Advisory | Notes
------- | -------- | -----
shibboleth-2.6.1-3.3.slc6 | &nbsp; &nbsp; | &nbsp;
shibboleth-devel-2.6.1-3.3.slc6 | &nbsp; &nbsp; | &nbsp;
