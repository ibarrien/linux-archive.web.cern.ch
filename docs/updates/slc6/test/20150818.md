## 2015-08-18

Package | Advisory | Notes
------- | -------- | -----
lemon-3.6.20-1.el6_7.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1634.html" target="secadv">RHSA-2015:1634</a> | &nbsp;
mod_dav_svn-1.6.11-15.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1633.html" target="secadv">RHSA-2015:1633</a> | &nbsp;
net-snmp-5.5-54.el6_7.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1636.html" target="secadv">RHSA-2015:1636</a> | &nbsp;
net-snmp-devel-5.5-54.el6_7.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1636.html" target="secadv">RHSA-2015:1636</a> | &nbsp;
net-snmp-libs-5.5-54.el6_7.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1636.html" target="secadv">RHSA-2015:1636</a> | &nbsp;
net-snmp-perl-5.5-54.el6_7.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1636.html" target="secadv">RHSA-2015:1636</a> | &nbsp;
net-snmp-python-5.5-54.el6_7.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1636.html" target="secadv">RHSA-2015:1636</a> | &nbsp;
net-snmp-utils-5.5-54.el6_7.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1636.html" target="secadv">RHSA-2015:1636</a> | &nbsp;
sqlite-3.6.20-1.el6_7.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1634.html" target="secadv">RHSA-2015:1634</a> | &nbsp;
sqlite-devel-3.6.20-1.el6_7.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1634.html" target="secadv">RHSA-2015:1634</a> | &nbsp;
sqlite-doc-3.6.20-1.el6_7.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1634.html" target="secadv">RHSA-2015:1634</a> | &nbsp;
sqlite-tcl-3.6.20-1.el6_7.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1634.html" target="secadv">RHSA-2015:1634</a> | &nbsp;
subversion-1.6.11-15.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1633.html" target="secadv">RHSA-2015:1633</a> | &nbsp;
subversion-devel-1.6.11-15.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1633.html" target="secadv">RHSA-2015:1633</a> | &nbsp;
subversion-gnome-1.6.11-15.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1633.html" target="secadv">RHSA-2015:1633</a> | &nbsp;
subversion-javahl-1.6.11-15.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1633.html" target="secadv">RHSA-2015:1633</a> | &nbsp;
subversion-kde-1.6.11-15.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1633.html" target="secadv">RHSA-2015:1633</a> | &nbsp;
subversion-perl-1.6.11-15.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1633.html" target="secadv">RHSA-2015:1633</a> | &nbsp;
subversion-ruby-1.6.11-15.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1633.html" target="secadv">RHSA-2015:1633</a> | &nbsp;
subversion-svn2cl-1.6.11-15.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1633.html" target="secadv">RHSA-2015:1633</a> | &nbsp;
tzdata-2015f-1.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-1625.html" target="secadv">RHEA-2015:1625</a> | &nbsp;
tzdata-java-2015f-1.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-1625.html" target="secadv">RHEA-2015:1625</a> | &nbsp;
