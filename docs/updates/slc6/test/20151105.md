## 2015-11-05

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-229.rt56.162.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-229.rt56.162.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-229.rt56.162.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-229.rt56.162.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-229.rt56.162.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-firmware-3.10.0-229.rt56.162.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-229.rt56.162.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-229.rt56.162.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-vanilla-3.10.0-229.rt56.162.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-vanilla-devel-3.10.0-229.rt56.162.el6rt | &nbsp; &nbsp; | &nbsp;
firefox-38.4.0-1.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1982.html" target="secadv">RHSA-2015:1982</a> | &nbsp;
nspr-4.10.8-2.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1981.html" target="secadv">RHSA-2015:1981</a> | &nbsp;
nspr-devel-4.10.8-2.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1981.html" target="secadv">RHSA-2015:1981</a> | &nbsp;
nss-3.19.1-5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1981.html" target="secadv">RHSA-2015:1981</a> | &nbsp;
nss-3.19.1-5.el6_7.cern | &nbsp; &nbsp; | &nbsp;
nss-devel-3.19.1-5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1981.html" target="secadv">RHSA-2015:1981</a> | &nbsp;
nss-devel-3.19.1-5.el6_7.cern | &nbsp; &nbsp; | &nbsp;
nss-pkcs11-devel-3.19.1-5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1981.html" target="secadv">RHSA-2015:1981</a> | &nbsp;
nss-pkcs11-devel-3.19.1-5.el6_7.cern | &nbsp; &nbsp; | &nbsp;
nss-sysinit-3.19.1-5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1981.html" target="secadv">RHSA-2015:1981</a> | &nbsp;
nss-sysinit-3.19.1-5.el6_7.cern | &nbsp; &nbsp; | &nbsp;
nss-tools-3.19.1-5.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1981.html" target="secadv">RHSA-2015:1981</a> | &nbsp;
nss-tools-3.19.1-5.el6_7.cern | &nbsp; &nbsp; | &nbsp;
nss-util-3.19.1-2.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1981.html" target="secadv">RHSA-2015:1981</a> | &nbsp;
nss-util-devel-3.19.1-2.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1981.html" target="secadv">RHSA-2015:1981</a> | &nbsp;
