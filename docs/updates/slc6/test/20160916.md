## 2016-09-16

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-327.rt56.197.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1883.html" target="secadv">RHSA-2016:1883</a> | &nbsp;
kernel-rt-debug-3.10.0-327.rt56.197.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1883.html" target="secadv">RHSA-2016:1883</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-327.rt56.197.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1883.html" target="secadv">RHSA-2016:1883</a> | &nbsp;
kernel-rt-devel-3.10.0-327.rt56.197.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1883.html" target="secadv">RHSA-2016:1883</a> | &nbsp;
kernel-rt-doc-3.10.0-327.rt56.197.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1883.html" target="secadv">RHSA-2016:1883</a> | &nbsp;
kernel-rt-firmware-3.10.0-327.rt56.197.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1883.html" target="secadv">RHSA-2016:1883</a> | &nbsp;
kernel-rt-trace-3.10.0-327.rt56.197.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1883.html" target="secadv">RHSA-2016:1883</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-327.rt56.197.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1883.html" target="secadv">RHSA-2016:1883</a> | &nbsp;
kernel-rt-vanilla-3.10.0-327.rt56.197.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1883.html" target="secadv">RHSA-2016:1883</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-327.rt56.197.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1883.html" target="secadv">RHSA-2016:1883</a> | &nbsp;
mattermost-desktop-1.3.0-1.slc6 | &nbsp; &nbsp; | &nbsp;
