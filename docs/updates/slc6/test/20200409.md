## 2020-04-09


Package | Advisory | Notes
------- | -------- | -----
qemu-guest-agent-0.12.1.2-2.506.el6_10.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1403.html" target="secadv">RHSA-2020:1403</a> | &nbsp;
qemu-img-0.12.1.2-2.506.el6_10.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1403.html" target="secadv">RHSA-2020:1403</a> | &nbsp;
qemu-kvm-0.12.1.2-2.506.el6_10.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1403.html" target="secadv">RHSA-2020:1403</a> | &nbsp;
qemu-kvm-tools-0.12.1.2-2.506.el6_10.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1403.html" target="secadv">RHSA-2020:1403</a> | &nbsp;

