## 2019-05-27

Package | Advisory | Notes
------- | -------- | -----
rt-setup-1.60-44.el6rt | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2019-1266.html" target="secadv">RHEA-2019:1266</a> | &nbsp;
firefox-60.7.0-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1267.html" target="secadv">RHSA-2019:1267</a> | &nbsp;
