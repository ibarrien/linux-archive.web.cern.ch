## 2014-04-01

Package | Advisory | Notes
------- | -------- | -----
resource-agents-3.9.2-40.el6_5.7 | &nbsp; &nbsp; | &nbsp;
resource-agents-sap-3.9.2-40.el6_5.7 | &nbsp; &nbsp; | &nbsp;
syslinux-4.02-9.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0337.html" target="secadv">RHBA-2014:0337</a> | &nbsp;
syslinux-devel-4.02-9.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0337.html" target="secadv">RHBA-2014:0337</a> | &nbsp;
syslinux-extlinux-4.02-9.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0337.html" target="secadv">RHBA-2014:0337</a> | &nbsp;
syslinux-perl-4.02-9.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0337.html" target="secadv">RHBA-2014:0337</a> | &nbsp;
syslinux-tftpboot-4.02-9.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0337.html" target="secadv">RHBA-2014:0337</a> | &nbsp;
tzdata-2014b-1.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2014-0338.html" target="secadv">RHEA-2014:0338</a> | &nbsp;
tzdata-java-2014b-1.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2014-0338.html" target="secadv">RHEA-2014:0338</a> | &nbsp;
wireshark-1.8.10-7.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0342.html" target="secadv">RHSA-2014:0342</a> | &nbsp;
wireshark-devel-1.8.10-7.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0342.html" target="secadv">RHSA-2014:0342</a> | &nbsp;
wireshark-gnome-1.8.10-7.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0342.html" target="secadv">RHSA-2014:0342</a> | &nbsp;
kernel-2.6.32-431.11.2.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-2.6.32-431.5.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-abi-whitelists-2.6.32-431.11.2.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-abi-whitelists-2.6.32-431.5.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-debug-2.6.32-431.11.2.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-debug-2.6.32-431.5.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-debug-devel-2.6.32-431.11.2.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-debug-devel-2.6.32-431.5.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-devel-2.6.32-431.11.2.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-devel-2.6.32-431.5.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-doc-2.6.32-431.11.2.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-doc-2.6.32-431.5.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-firmware-2.6.32-431.11.2.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-firmware-2.6.32-431.5.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-headers-2.6.32-431.11.2.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-headers-2.6.32-431.5.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
kernel-module-openafs-2.6.32-431.11.2.el6.nonpae-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
kernel-module-openafs-2.6.32-431.5.1.el6.nonpae-1.6.5-cern2.1.el6 | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-431.11.2.el6.nonpae | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-431.5.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
python-perf-2.6.32-431.11.2.el6.nonpae | &nbsp; &nbsp; | &nbsp;
python-perf-2.6.32-431.5.1.el6.nonpae | &nbsp; &nbsp; | &nbsp;
