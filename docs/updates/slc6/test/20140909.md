## 2014-09-09

Package | Advisory | Notes
------- | -------- | -----
jakarta-commons-httpclient-3.1-0.9.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1166.html" target="secadv">RHSA-2014:1166</a> | &nbsp;
jakarta-commons-httpclient-demo-3.1-0.9.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1166.html" target="secadv">RHSA-2014:1166</a> | &nbsp;
jakarta-commons-httpclient-javadoc-3.1-0.9.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1166.html" target="secadv">RHSA-2014:1166</a> | &nbsp;
jakarta-commons-httpclient-manual-3.1-0.9.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1166.html" target="secadv">RHSA-2014:1166</a> | &nbsp;
VidyoDesktop-3.3.0-027 | &nbsp; &nbsp; | &nbsp;
