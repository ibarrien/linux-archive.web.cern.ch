## 2014-10-09

Package | Advisory | Notes
------- | -------- | -----
splunk-6.1.4-233537 | &nbsp; &nbsp; | &nbsp;
splunkforwarder-6.1.4-233537 | &nbsp; &nbsp; | &nbsp;
rtctl-1.12-4.el6rt | &nbsp; &nbsp; | &nbsp;
rt-firmware-2.0-2.el6rt | &nbsp; &nbsp; | &nbsp;
rt-setup-1.56-2.el6rt | &nbsp; &nbsp; | &nbsp;
at-3.1.10-44.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1362.html" target="secadv">RHBA-2014:1362</a> | &nbsp;
