## 2020-11-13


Package | Advisory | Notes
------- | -------- | -----
firefox-78.4.1-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-5104.html" target="secadv">RHSA-2020:5104</a> | &nbsp;
firefox-78.4.1-1.el6_10.ZE2TMv | &nbsp; &nbsp; | &nbsp;

