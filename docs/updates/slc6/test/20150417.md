## 2015-04-17

Package | Advisory | Notes
------- | -------- | -----
kdelibs-4.3.4-23.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0817.html" target="secadv">RHBA-2015:0817</a> | &nbsp;
kdelibs-apidocs-4.3.4-23.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0817.html" target="secadv">RHBA-2015:0817</a> | &nbsp;
kdelibs-common-4.3.4-23.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0817.html" target="secadv">RHBA-2015:0817</a> | &nbsp;
kdelibs-devel-4.3.4-23.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0817.html" target="secadv">RHBA-2015:0817</a> | &nbsp;
