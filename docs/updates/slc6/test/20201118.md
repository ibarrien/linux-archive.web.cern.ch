## 2020-11-18


Package | Advisory | Notes
------- | -------- | -----
firefox-78.4.1-1.el6_10.ZE2TMv | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-1.8.0.275.b01-0.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2020-5130.html" target="secadv">RHBA-2020:5130</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.275.b01-0.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2020-5130.html" target="secadv">RHBA-2020:5130</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.275.b01-0.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2020-5130.html" target="secadv">RHBA-2020:5130</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.275.b01-0.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2020-5130.html" target="secadv">RHBA-2020:5130</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.275.b01-0.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2020-5130.html" target="secadv">RHBA-2020:5130</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.275.b01-0.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2020-5130.html" target="secadv">RHBA-2020:5130</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.275.b01-0.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2020-5130.html" target="secadv">RHBA-2020:5130</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.275.b01-0.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2020-5130.html" target="secadv">RHBA-2020:5130</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.275.b01-0.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2020-5130.html" target="secadv">RHBA-2020:5130</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.275.b01-0.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2020-5130.html" target="secadv">RHBA-2020:5130</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.275.b01-0.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2020-5130.html" target="secadv">RHBA-2020:5130</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.275.b01-0.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2020-5130.html" target="secadv">RHBA-2020:5130</a> | &nbsp;

