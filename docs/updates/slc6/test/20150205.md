## 2015-02-05

Package | Advisory | Notes
------- | -------- | -----
cern-get-certificate-0.5-1.slc6 | &nbsp; &nbsp; | &nbsp;
cern-get-keytab-0.9.7-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-1.7.0.76-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-devel-1.7.0.76-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-jdbc-1.7.0.76-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-plugin-1.7.0.76-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-oracle-src-1.7.0.76-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-1.8.0.31-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-devel-1.8.0.31-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-headless-1.8.0.31-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-javafx-1.8.0.31-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-plugin-1.8.0.31-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-oracle-src-1.8.0.31-1.0.slc6 | &nbsp; &nbsp; | &nbsp;
xorg-x11-server-common-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
xorg-x11-server-devel-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
xorg-x11-server-source-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
xorg-x11-server-Xdmx-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
xorg-x11-server-Xephyr-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
xorg-x11-server-Xnest-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
xorg-x11-server-Xorg-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
xorg-x11-server-Xvfb-1.15.0-25.1.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0123.html" target="secadv">RHBA-2015:0123</a> | &nbsp;
sl-release-6.6-2.slc6.nonpae | &nbsp; &nbsp; | &nbsp;
