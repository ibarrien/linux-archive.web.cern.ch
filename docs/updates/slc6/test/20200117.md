## 2020-01-17

Package | Advisory | Notes
------- | -------- | -----
thunderbird-68.4.1-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0123.html" target="secadv">RHSA-2020:0123</a> | &nbsp;
