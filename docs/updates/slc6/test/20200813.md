## 2020-08-13


Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-debug-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-devel-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-doc-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-firmware-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-trace-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-vanilla-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-693.72.1.rt56.672.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-3389.html" target="secadv">RHSA-2020:3389</a> | &nbsp;

