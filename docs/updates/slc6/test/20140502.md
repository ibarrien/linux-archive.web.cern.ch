## 2014-05-02

Package | Advisory | Notes
------- | -------- | -----
firefox-24.5.0-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0448.html" target="secadv">RHSA-2014:0448</a> | &nbsp;
openscap-1.0.8-1.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0450.html" target="secadv">RHBA-2014:0450</a> | &nbsp;
openscap-content-1.0.8-1.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0450.html" target="secadv">RHBA-2014:0450</a> | &nbsp;
openscap-devel-1.0.8-1.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0450.html" target="secadv">RHBA-2014:0450</a> | &nbsp;
openscap-engine-sce-1.0.8-1.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0450.html" target="secadv">RHBA-2014:0450</a> | &nbsp;
openscap-engine-sce-devel-1.0.8-1.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0450.html" target="secadv">RHBA-2014:0450</a> | &nbsp;
openscap-extra-probes-1.0.8-1.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0450.html" target="secadv">RHBA-2014:0450</a> | &nbsp;
openscap-python-1.0.8-1.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0450.html" target="secadv">RHBA-2014:0450</a> | &nbsp;
openscap-utils-1.0.8-1.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0450.html" target="secadv">RHBA-2014:0450</a> | &nbsp;
rsync-3.0.6-12.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0451.html" target="secadv">RHBA-2014:0451</a> | &nbsp;
thunderbird-24.5.0-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0449.html" target="secadv">RHSA-2014:0449</a> | &nbsp;
