## 2019-11-25

Package | Advisory | Notes
------- | -------- | -----
rt-setup-1.60-49.el6rt | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2019-3924.html" target="secadv">RHEA-2019:3924</a> | &nbsp;
