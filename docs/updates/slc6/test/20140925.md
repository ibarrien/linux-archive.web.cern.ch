## 2014-09-25

Package | Advisory | Notes
------- | -------- | -----
ioprocess-0.12.0-2.el6ev | &nbsp; &nbsp; | &nbsp;
kexec-tools-2.0.0-280.el6 | &nbsp; &nbsp; | &nbsp;
kexec-tools-eppic-2.0.0-280.el6 | &nbsp; &nbsp; | &nbsp;
mom-0.4.1-1.el6_5 | &nbsp; &nbsp; | &nbsp;
otopi-1.3.0-0.0.1.master.el6ev | &nbsp; &nbsp; | &nbsp;
otopi-devtools-1.3.0-0.0.1.master.el6ev | &nbsp; &nbsp; | &nbsp;
otopi-java-1.3.0-0.0.1.master.el6ev | &nbsp; &nbsp; | &nbsp;
otopi-repolib-1.3.0-0.0.1.master.el6ev | &nbsp; &nbsp; | &nbsp;
ovirt-engine-extension-aaa-ldap-0.0.0-0.0.3.master.el6ev | &nbsp; &nbsp; | &nbsp;
ovirt-host-deploy-1.3.0-0.0.2.master.el6ev | &nbsp; &nbsp; | &nbsp;
ovirt-host-deploy-java-1.3.0-0.0.2.master.el6ev | &nbsp; &nbsp; | &nbsp;
ovirt-host-deploy-offline-1.3.0-0.0.1.master.el6ev | &nbsp; &nbsp; | &nbsp;
ovirt-host-deploy-repolib-1.3.0-0.0.2.master.el6ev | &nbsp; &nbsp; | &nbsp;
ovirt-hosted-engine-ha-1.2.1-0.3.master.el6_5 | &nbsp; &nbsp; | &nbsp;
ovirt-hosted-engine-setup-1.2.0-0.3.beta.el6ev | &nbsp; &nbsp; | &nbsp;
ovirt-node-plugin-vdsm-0.2.0-6.el6ev | &nbsp; &nbsp; | &nbsp;
ovirt-node-plugin-vdsm-recipe-0.2.0-6.el6ev | &nbsp; &nbsp; | &nbsp;
ovirt-optimizer-0.3-3 | &nbsp; &nbsp; | &nbsp;
ovirt-optimizer-jboss7-0.3-3 | &nbsp; &nbsp; | &nbsp;
ovirt-optimizer-ui-0.3-3 | &nbsp; &nbsp; | &nbsp;
python-cpopen-1.3-4.el6_5 | &nbsp; &nbsp; | &nbsp;
python-ioprocess-0.12.0-2.el6ev | &nbsp; &nbsp; | &nbsp;
rcue1-1.0.5-1.el6_5 | &nbsp; &nbsp; | &nbsp;
rhev-guest-tools-iso-3.5-2.el6_5 | &nbsp; &nbsp; | &nbsp;
rhevm-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-backend-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-branding-rhev-3.5.0-0.0.master.20140825194046.el6_5 | &nbsp; &nbsp; | &nbsp;
rhevm-cli-3.5.0.4-1.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-dbscripts-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-dependencies-3.5.0-0.1.master.el6_5 | &nbsp; &nbsp; | &nbsp;
rhevm-dwh-3.5.0-3.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-dwh-setup-3.5.0-3.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-extensions-api-impl-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-extensions-api-impl-javadoc-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-guest-agent-common-1.0.10-1.el6_5 | &nbsp; &nbsp; | &nbsp;
rhevm-guest-agent-gdm-plugin-1.0.10-1.el6_5 | &nbsp; &nbsp; | &nbsp;
rhevm-guest-agent-kdm-plugin-1.0.10-1.el6_5 | &nbsp; &nbsp; | &nbsp;
rhevm-guest-agent-pam-module-1.0.10-1.el6_5 | &nbsp; &nbsp; | &nbsp;
rhevm-image-uploader-3.5.0-0.3.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-iso-uploader-3.5.0-0.3.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-lib-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-log-collector-3.5.0-0.2.master.el6_5 | &nbsp; &nbsp; | &nbsp;
rhevm-reports-3.5.0-4.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-reports-setup-3.5.0-4.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-restapi-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-sdk-python-3.5.0.7-1.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-setup-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-setup-base-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-setup-plugin-allinone-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-setup-plugin-ovirt-engine-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-setup-plugin-ovirt-engine-common-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-setup-plugins-3.5.0-0.3.master.el6_5 | &nbsp; &nbsp; | &nbsp;
rhevm-setup-plugin-websocket-proxy-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-spice-client-x64-cab-3.5-2.el6 | &nbsp; &nbsp; | &nbsp;
rhevm-spice-client-x64-msi-3.5-2.el6 | &nbsp; &nbsp; | &nbsp;
rhevm-spice-client-x86-cab-3.5-2.el6 | &nbsp; &nbsp; | &nbsp;
rhevm-spice-client-x86-msi-3.5-2.el6 | &nbsp; &nbsp; | &nbsp;
rhevm-tools-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-userportal-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-webadmin-portal-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
rhevm-websocket-proxy-3.5.0-0.12.beta.el6ev | &nbsp; &nbsp; | &nbsp;
unboundid-ldapsdk-2.3.7-0.0.snap.r530.el6_5 | &nbsp; &nbsp; | &nbsp;
unboundid-ldapsdk-javadoc-2.3.7-0.0.snap.r530.el6_5 | &nbsp; &nbsp; | &nbsp;
vdsm-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-bootstrap-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-cli-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-debug-plugin-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-hook-ethtool-options-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-hook-faqemu-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-hook-openstacknet-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-hook-qemucmdline-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-hook-vhostmd-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-jsonrpc-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-python-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-python-zombiereaper-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-reg-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-tests-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-xmlrpc-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
vdsm-yajsonrpc-4.16.3-3.el6ev.beta | &nbsp; &nbsp; | &nbsp;
virtio-win-1.7.1-1.el6_5 | &nbsp; &nbsp; | &nbsp;
shibboleth-2.5.3-1.3.slc6 | &nbsp; &nbsp; | &nbsp;
shibboleth-devel-2.5.3-1.3.slc6 | &nbsp; &nbsp; | &nbsp;
bash_mitigate_cve_2014_6271-0.0.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
bash_mitigate_cve_2014_6271-0.0.2-1.slc6 | &nbsp; &nbsp; | &nbsp;
