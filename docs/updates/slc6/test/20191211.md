## 2019-12-11

Package | Advisory | Notes
------- | -------- | -----
microcode_ctl-1.17-33.23.el6_10 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2019-4155.html" target="secadv">RHEA-2019:4155</a> | &nbsp;
nss-softokn-3.44.0-6.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4152.html" target="secadv">RHSA-2019:4152</a> | &nbsp;
nss-softokn-devel-3.44.0-6.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4152.html" target="secadv">RHSA-2019:4152</a> | &nbsp;
nss-softokn-freebl-3.44.0-6.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4152.html" target="secadv">RHSA-2019:4152</a> | &nbsp;
nss-softokn-freebl-devel-3.44.0-6.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4152.html" target="secadv">RHSA-2019:4152</a> | &nbsp;
