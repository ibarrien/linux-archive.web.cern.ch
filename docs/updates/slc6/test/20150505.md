## 2015-05-05

Package | Advisory | Notes
------- | -------- | -----
rhev-hypervisor6-6.6-20150421.0.el6ev | &nbsp; &nbsp; | &nbsp;
chromium-browser-42.0.2311.135-1.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0921.html" target="secadv">RHSA-2015:0921</a> | &nbsp;
