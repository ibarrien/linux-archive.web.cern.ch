## 2019-03-27

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-693.46.1.rt56.639.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0641.html" target="secadv">RHSA-2019:0641</a> | &nbsp;
kernel-rt-debug-3.10.0-693.46.1.rt56.639.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0641.html" target="secadv">RHSA-2019:0641</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-693.46.1.rt56.639.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0641.html" target="secadv">RHSA-2019:0641</a> | &nbsp;
kernel-rt-devel-3.10.0-693.46.1.rt56.639.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0641.html" target="secadv">RHSA-2019:0641</a> | &nbsp;
kernel-rt-doc-3.10.0-693.46.1.rt56.639.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0641.html" target="secadv">RHSA-2019:0641</a> | &nbsp;
kernel-rt-firmware-3.10.0-693.46.1.rt56.639.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0641.html" target="secadv">RHSA-2019:0641</a> | &nbsp;
kernel-rt-trace-3.10.0-693.46.1.rt56.639.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0641.html" target="secadv">RHSA-2019:0641</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-693.46.1.rt56.639.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0641.html" target="secadv">RHSA-2019:0641</a> | &nbsp;
kernel-rt-vanilla-3.10.0-693.46.1.rt56.639.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0641.html" target="secadv">RHSA-2019:0641</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-693.46.1.rt56.639.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0641.html" target="secadv">RHSA-2019:0641</a> | &nbsp;
firefox-60.6.0-3.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0623.html" target="secadv">RHSA-2019:0623</a> | &nbsp;
java-1.7.0-openjdk-1.7.0.211-2.6.17.1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0462.html" target="secadv">RHSA-2019:0462</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.211-2.6.17.1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0462.html" target="secadv">RHSA-2019:0462</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.211-2.6.17.1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0462.html" target="secadv">RHSA-2019:0462</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.211-2.6.17.1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0462.html" target="secadv">RHSA-2019:0462</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.211-2.6.17.1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0462.html" target="secadv">RHSA-2019:0462</a> | &nbsp;
java-1.8.0-openjdk-1.8.0.201.b09-2.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-0479.html" target="secadv">RHBA-2019:0479</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.201.b09-2.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-0479.html" target="secadv">RHBA-2019:0479</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.201.b09-2.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-0479.html" target="secadv">RHBA-2019:0479</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.201.b09-2.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-0479.html" target="secadv">RHBA-2019:0479</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.201.b09-2.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-0479.html" target="secadv">RHBA-2019:0479</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.201.b09-2.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-0479.html" target="secadv">RHBA-2019:0479</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.201.b09-2.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-0479.html" target="secadv">RHBA-2019:0479</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.201.b09-2.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-0479.html" target="secadv">RHBA-2019:0479</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.201.b09-2.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-0479.html" target="secadv">RHBA-2019:0479</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.201.b09-2.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-0479.html" target="secadv">RHBA-2019:0479</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.201.b09-2.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-0479.html" target="secadv">RHBA-2019:0479</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.201.b09-2.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-0479.html" target="secadv">RHBA-2019:0479</a> | &nbsp;
