## 2014-01-08

Package | Advisory | Notes
------- | -------- | -----
libipa_hbac-1.9.2-129.el6_5.4 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0005.html" target="secadv">RHBA-2014:0005</a> | &nbsp;
libipa_hbac-devel-1.9.2-129.el6_5.4 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0005.html" target="secadv">RHBA-2014:0005</a> | &nbsp;
libipa_hbac-python-1.9.2-129.el6_5.4 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0005.html" target="secadv">RHBA-2014:0005</a> | &nbsp;
libsss_autofs-1.9.2-129.el6_5.4 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0005.html" target="secadv">RHBA-2014:0005</a> | &nbsp;
libsss_idmap-1.9.2-129.el6_5.4 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0005.html" target="secadv">RHBA-2014:0005</a> | &nbsp;
libsss_idmap-devel-1.9.2-129.el6_5.4 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0005.html" target="secadv">RHBA-2014:0005</a> | &nbsp;
libsss_sudo-1.9.2-129.el6_5.4 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0005.html" target="secadv">RHBA-2014:0005</a> | &nbsp;
libsss_sudo-devel-1.9.2-129.el6_5.4 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0005.html" target="secadv">RHBA-2014:0005</a> | &nbsp;
sssd-1.9.2-129.el6_5.4 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0005.html" target="secadv">RHBA-2014:0005</a> | &nbsp;
sssd-client-1.9.2-129.el6_5.4 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0005.html" target="secadv">RHBA-2014:0005</a> | &nbsp;
sssd-tools-1.9.2-129.el6_5.4 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0005.html" target="secadv">RHBA-2014:0005</a> | &nbsp;
