## 2015-01-20

Package | Advisory | Notes
------- | -------- | -----
qemu-img-rhev-0.12.1.2-2.446.el6 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-rhev-0.12.1.2-2.446.el6 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-rhev-tools-0.12.1.2-2.446.el6 | &nbsp; &nbsp; | &nbsp;
condor-7.8.10-0.2.el6 | &nbsp; &nbsp; | &nbsp;
condor-aviary-7.8.10-0.2.el6 | &nbsp; &nbsp; | &nbsp;
condor-classads-7.8.10-0.2.el6 | &nbsp; &nbsp; | &nbsp;
condor-classads-devel-7.8.10-0.2.el6 | &nbsp; &nbsp; | &nbsp;
condor-cluster-resource-agent-7.8.10-0.2.el6 | &nbsp; &nbsp; | &nbsp;
condor-deltacloud-gahp-7.8.10-0.2.el6 | &nbsp; &nbsp; | &nbsp;
condor-kbdd-7.8.10-0.2.el6 | &nbsp; &nbsp; | &nbsp;
condor-plumage-7.8.10-0.2.el6 | &nbsp; &nbsp; | &nbsp;
condor-qmf-7.8.10-0.2.el6 | &nbsp; &nbsp; | &nbsp;
condor-vm-gahp-7.8.10-0.2.el6 | &nbsp; &nbsp; | &nbsp;
thermostat1-thermostat-1.0.4-60.6.el6 | &nbsp; &nbsp; | &nbsp;
thermostat1-thermostat-javadoc-1.0.4-60.6.el6 | &nbsp; &nbsp; | &nbsp;
thermostat1-thermostat-webapp-1.0.4-60.6.el6 | &nbsp; &nbsp; | &nbsp;
cern-get-certificate-0.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
sl-release-6.6-2.slc6.nonpae | &nbsp; &nbsp; | &nbsp;
