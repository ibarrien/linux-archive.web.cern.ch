## 2019-11-26

Package | Advisory | Notes
------- | -------- | -----
cern-get-sso-cookie-0.6-2.slc6 | &nbsp; &nbsp; | &nbsp;
perl-WWW-CERNSSO-Auth-0.6-2.slc6 | &nbsp; &nbsp; | &nbsp;
perl-Authen-Krb5-1.9-3.slc6 | &nbsp; &nbsp; | &nbsp;
