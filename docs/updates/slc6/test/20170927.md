## 2017-09-27

Package | Advisory | Notes
------- | -------- | -----
kmod-spl-0.6.5.4-2.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-spl-devel-0.6.5.4-2.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-zfs-0.6.5.4-2.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-zfs-devel-0.6.5.4-2.slc6 | &nbsp; &nbsp; | &nbsp;
kernel-2.6.32-696.10.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2795.html" target="secadv">RHSA-2017:2795</a> | &nbsp;
kernel-abi-whitelists-2.6.32-696.10.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2795.html" target="secadv">RHSA-2017:2795</a> | &nbsp;
kernel-debug-2.6.32-696.10.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2795.html" target="secadv">RHSA-2017:2795</a> | &nbsp;
kernel-debug-devel-2.6.32-696.10.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2795.html" target="secadv">RHSA-2017:2795</a> | &nbsp;
kernel-devel-2.6.32-696.10.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2795.html" target="secadv">RHSA-2017:2795</a> | &nbsp;
kernel-doc-2.6.32-696.10.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2795.html" target="secadv">RHSA-2017:2795</a> | &nbsp;
kernel-firmware-2.6.32-696.10.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2795.html" target="secadv">RHSA-2017:2795</a> | &nbsp;
kernel-headers-2.6.32-696.10.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2795.html" target="secadv">RHSA-2017:2795</a> | &nbsp;
kernel-module-openafs-2.6.32-696.10.3.el6-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-696.10.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2795.html" target="secadv">RHSA-2017:2795</a> | &nbsp;
python-perf-2.6.32-696.10.3.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2795.html" target="secadv">RHSA-2017:2795</a> | &nbsp;
