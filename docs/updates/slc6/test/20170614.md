## 2017-06-14

Package | Advisory | Notes
------- | -------- | -----
rt-firmware-2.4-1.el6rt | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2017-1396.html" target="secadv">RHEA-2017:1396</a> | &nbsp;
ca-certificates-2017.2.14-65.0.1.el6_9 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2017-1432.html" target="secadv">RHEA-2017:1432</a> | &nbsp;
rpcbind-0.2.0-13.el6_9.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-1435.html" target="secadv">RHBA-2017:1435</a> | &nbsp;
