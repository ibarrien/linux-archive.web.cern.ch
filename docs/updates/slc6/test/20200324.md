## 2020-03-24

Package | Advisory | Notes
------- | -------- | -----
thunderbird-68.6.0-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0914.html" target="secadv">RHSA-2020:0914</a> | &nbsp;
tomcat6-6.0.24-114.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0912.html" target="secadv">RHSA-2020:0912</a> | &nbsp;
tomcat6-admin-webapps-6.0.24-114.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0912.html" target="secadv">RHSA-2020:0912</a> | &nbsp;
tomcat6-docs-webapp-6.0.24-114.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0912.html" target="secadv">RHSA-2020:0912</a> | &nbsp;
tomcat6-el-2.1-api-6.0.24-114.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0912.html" target="secadv">RHSA-2020:0912</a> | &nbsp;
tomcat6-javadoc-6.0.24-114.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0912.html" target="secadv">RHSA-2020:0912</a> | &nbsp;
tomcat6-jsp-2.1-api-6.0.24-114.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0912.html" target="secadv">RHSA-2020:0912</a> | &nbsp;
tomcat6-lib-6.0.24-114.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0912.html" target="secadv">RHSA-2020:0912</a> | &nbsp;
tomcat6-servlet-2.5-api-6.0.24-114.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0912.html" target="secadv">RHSA-2020:0912</a> | &nbsp;
tomcat6-webapps-6.0.24-114.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0912.html" target="secadv">RHSA-2020:0912</a> | &nbsp;
