## 2017-01-18

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-514.rt56.210.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0113.html" target="secadv">RHSA-2017:0113</a> | &nbsp;
kernel-rt-debug-3.10.0-514.rt56.210.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0113.html" target="secadv">RHSA-2017:0113</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-514.rt56.210.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0113.html" target="secadv">RHSA-2017:0113</a> | &nbsp;
kernel-rt-devel-3.10.0-514.rt56.210.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0113.html" target="secadv">RHSA-2017:0113</a> | &nbsp;
kernel-rt-doc-3.10.0-514.rt56.210.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0113.html" target="secadv">RHSA-2017:0113</a> | &nbsp;
kernel-rt-firmware-3.10.0-514.rt56.210.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0113.html" target="secadv">RHSA-2017:0113</a> | &nbsp;
kernel-rt-trace-3.10.0-514.rt56.210.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0113.html" target="secadv">RHSA-2017:0113</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-514.rt56.210.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0113.html" target="secadv">RHSA-2017:0113</a> | &nbsp;
kernel-rt-vanilla-3.10.0-514.rt56.210.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0113.html" target="secadv">RHSA-2017:0113</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-514.rt56.210.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0113.html" target="secadv">RHSA-2017:0113</a> | &nbsp;
