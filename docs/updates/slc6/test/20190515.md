## 2019-05-15

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-693.47.2.rt56.641.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1190.html" target="secadv">RHSA-2019:1190</a> | &nbsp;
kernel-rt-debug-3.10.0-693.47.2.rt56.641.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1190.html" target="secadv">RHSA-2019:1190</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-693.47.2.rt56.641.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1190.html" target="secadv">RHSA-2019:1190</a> | &nbsp;
kernel-rt-devel-3.10.0-693.47.2.rt56.641.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1190.html" target="secadv">RHSA-2019:1190</a> | &nbsp;
kernel-rt-doc-3.10.0-693.47.2.rt56.641.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1190.html" target="secadv">RHSA-2019:1190</a> | &nbsp;
kernel-rt-firmware-3.10.0-693.47.2.rt56.641.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1190.html" target="secadv">RHSA-2019:1190</a> | &nbsp;
kernel-rt-trace-3.10.0-693.47.2.rt56.641.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1190.html" target="secadv">RHSA-2019:1190</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-693.47.2.rt56.641.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1190.html" target="secadv">RHSA-2019:1190</a> | &nbsp;
kernel-rt-vanilla-3.10.0-693.47.2.rt56.641.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1190.html" target="secadv">RHSA-2019:1190</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-693.47.2.rt56.641.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1190.html" target="secadv">RHSA-2019:1190</a> | &nbsp;
kernel-2.6.32-754.14.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1169.html" target="secadv">RHSA-2019:1169</a> | &nbsp;
kernel-abi-whitelists-2.6.32-754.14.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1169.html" target="secadv">RHSA-2019:1169</a> | &nbsp;
kernel-debug-2.6.32-754.14.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1169.html" target="secadv">RHSA-2019:1169</a> | &nbsp;
kernel-debug-devel-2.6.32-754.14.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1169.html" target="secadv">RHSA-2019:1169</a> | &nbsp;
kernel-devel-2.6.32-754.14.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1169.html" target="secadv">RHSA-2019:1169</a> | &nbsp;
kernel-doc-2.6.32-754.14.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1169.html" target="secadv">RHSA-2019:1169</a> | &nbsp;
kernel-firmware-2.6.32-754.14.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1169.html" target="secadv">RHSA-2019:1169</a> | &nbsp;
kernel-headers-2.6.32-754.14.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1169.html" target="secadv">RHSA-2019:1169</a> | &nbsp;
kernel-module-openafs-2.6.32-754.14.2.el6-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-oracleasm-2.0.8-16.1.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-1226.html" target="secadv">RHBA-2019:1226</a> | &nbsp;
libvirt-0.10.2-64.el6_10.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1180.html" target="secadv">RHSA-2019:1180</a> | &nbsp;
libvirt-client-0.10.2-64.el6_10.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1180.html" target="secadv">RHSA-2019:1180</a> | &nbsp;
libvirt-devel-0.10.2-64.el6_10.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1180.html" target="secadv">RHSA-2019:1180</a> | &nbsp;
libvirt-lock-sanlock-0.10.2-64.el6_10.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1180.html" target="secadv">RHSA-2019:1180</a> | &nbsp;
libvirt-python-0.10.2-64.el6_10.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1180.html" target="secadv">RHSA-2019:1180</a> | &nbsp;
microcode_ctl-1.17-33.11.el6_10 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2019-1212.html" target="secadv">RHEA-2019:1212</a> | &nbsp;
perf-2.6.32-754.14.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1169.html" target="secadv">RHSA-2019:1169</a> | &nbsp;
python-perf-2.6.32-754.14.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1169.html" target="secadv">RHSA-2019:1169</a> | &nbsp;
qemu-guest-agent-0.12.1.2-2.506.el6_10.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1181.html" target="secadv">RHSA-2019:1181</a> | &nbsp;
qemu-img-0.12.1.2-2.506.el6_10.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1181.html" target="secadv">RHSA-2019:1181</a> | &nbsp;
qemu-kvm-0.12.1.2-2.506.el6_10.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1181.html" target="secadv">RHSA-2019:1181</a> | &nbsp;
qemu-kvm-tools-0.12.1.2-2.506.el6_10.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-1181.html" target="secadv">RHSA-2019:1181</a> | &nbsp;
spice-vdagent-0.14.0-13.el6_10.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-1227.html" target="secadv">RHBA-2019:1227</a> | &nbsp;
