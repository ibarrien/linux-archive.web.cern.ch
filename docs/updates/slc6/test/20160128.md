## 2016-01-28

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-327.rt56.170.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.10.0-327.rt56.170.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.10.0-327.rt56.170.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.10.0-327.rt56.170.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.10.0-327.rt56.170.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-firmware-3.10.0-327.rt56.170.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.10.0-327.rt56.170.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.10.0-327.rt56.170.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-vanilla-3.10.0-327.rt56.170.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-vanilla-devel-3.10.0-327.rt56.170.el6rt | &nbsp; &nbsp; | &nbsp;
bind-9.8.2-0.37.rc1.el6_7.6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0073.html" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-chroot-9.8.2-0.37.rc1.el6_7.6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0073.html" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-devel-9.8.2-0.37.rc1.el6_7.6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0073.html" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-libs-9.8.2-0.37.rc1.el6_7.6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0073.html" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-sdb-9.8.2-0.37.rc1.el6_7.6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0073.html" target="secadv">RHSA-2016:0073</a> | &nbsp;
bind-utils-9.8.2-0.37.rc1.el6_7.6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0073.html" target="secadv">RHSA-2016:0073</a> | &nbsp;
firefox-38.6.0-1.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0071.html" target="secadv">RHSA-2016:0071</a> | &nbsp;
java-1.6.0-openjdk-1.6.0.38-1.13.10.0.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0067.html" target="secadv">RHSA-2016:0067</a> | &nbsp;
java-1.6.0-openjdk-demo-1.6.0.38-1.13.10.0.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0067.html" target="secadv">RHSA-2016:0067</a> | &nbsp;
java-1.6.0-openjdk-devel-1.6.0.38-1.13.10.0.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0067.html" target="secadv">RHSA-2016:0067</a> | &nbsp;
java-1.6.0-openjdk-javadoc-1.6.0.38-1.13.10.0.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0067.html" target="secadv">RHSA-2016:0067</a> | &nbsp;
java-1.6.0-openjdk-src-1.6.0.38-1.13.10.0.el6_7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-0067.html" target="secadv">RHSA-2016:0067</a> | &nbsp;
