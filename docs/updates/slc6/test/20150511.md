## 2015-05-11

Package | Advisory | Notes
------- | -------- | -----
ImageMagick-6.5.4.7-7.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-c++-6.5.4.7-7.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-c++-devel-6.5.4.7-7.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-devel-6.5.4.7-7.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-doc-6.5.4.7-7.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-perl-6.5.4.7-7.slc6 | &nbsp; &nbsp; | &nbsp;
