## 2014-09-24

Package | Advisory | Notes
------- | -------- | -----
devtoolset-2-axis-1.4-23.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-httpcomponents-client-4.2.1-6.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-httpcomponents-client-javadoc-4.2.1-6.el6 | &nbsp; &nbsp; | &nbsp;
mrg-messaging-release-3.0.0-3.el6 | &nbsp; &nbsp; | &nbsp;
python-qpid-0.22-18.el6 | &nbsp; &nbsp; | &nbsp;
python-qpid-qmf-0.22-39.el6 | &nbsp; &nbsp; | &nbsp;
python-saslwrapper-0.22-5.el6 | &nbsp; &nbsp; | &nbsp;
qpid-java-client-0.22-6.el6 | &nbsp; &nbsp; | &nbsp;
qpid-java-common-0.22-6.el6 | &nbsp; &nbsp; | &nbsp;
qpid-java-example-0.22-6.el6 | &nbsp; &nbsp; | &nbsp;
qpid-jca-0.22-2.el6 | &nbsp; &nbsp; | &nbsp;
qpid-jca-xarecovery-0.22-2.el6 | &nbsp; &nbsp; | &nbsp;
qpid-jca-zip-0.22-2.el6 | &nbsp; &nbsp; | &nbsp;
qpid-qmf-0.22-39.el6 | &nbsp; &nbsp; | &nbsp;
qpid-qmf-devel-0.22-39.el6 | &nbsp; &nbsp; | &nbsp;
qpid-tests-0.22-16.el6 | &nbsp; &nbsp; | &nbsp;
qpid-tools-0.22-16.el6 | &nbsp; &nbsp; | &nbsp;
ruby-qpid-qmf-0.22-39.el6 | &nbsp; &nbsp; | &nbsp;
ruby-saslwrapper-0.22-5.el6 | &nbsp; &nbsp; | &nbsp;
saslwrapper-0.22-5.el6 | &nbsp; &nbsp; | &nbsp;
saslwrapper-devel-0.22-5.el6 | &nbsp; &nbsp; | &nbsp;
bash-4.1.2-15.el6_4.1 | &nbsp; &nbsp; | &nbsp;
bash-4.1.2-15.el6_5.1 | &nbsp; &nbsp; | &nbsp;
bash-doc-4.1.2-15.el6_4.1 | &nbsp; &nbsp; | &nbsp;
bash-doc-4.1.2-15.el6_5.1 | &nbsp; &nbsp; | &nbsp;
libxerces-c-3_0-3.0.1-20.slc6 | &nbsp; &nbsp; | &nbsp;
libxerces-c-3_1-3.1.1-2.2.slc6 | &nbsp; &nbsp; | &nbsp;
libxerces-c-3_1-3.1.1-2.3.slc6 | &nbsp; &nbsp; | &nbsp;
libxerces-c-devel-3.1.1-2.2.slc6 | &nbsp; &nbsp; | &nbsp;
libxerces-c-devel-3.1.1-2.3.slc6 | &nbsp; &nbsp; | &nbsp;
xerces-c-bin-3.1.1-2.2.slc6 | &nbsp; &nbsp; | &nbsp;
xerces-c-bin-3.1.1-2.3.slc6 | &nbsp; &nbsp; | &nbsp;
