## 2020-02-11

Package | Advisory | Notes
------- | -------- | -----
spice-glib-0.26-8.el6_10.2 | &nbsp; &nbsp; | &nbsp;
spice-glib-devel-0.26-8.el6_10.2 | &nbsp; &nbsp; | &nbsp;
spice-gtk-0.26-8.el6_10.2 | &nbsp; &nbsp; | &nbsp;
spice-gtk-devel-0.26-8.el6_10.2 | &nbsp; &nbsp; | &nbsp;
spice-gtk-python-0.26-8.el6_10.2 | &nbsp; &nbsp; | &nbsp;
spice-gtk-tools-0.26-8.el6_10.2 | &nbsp; &nbsp; | &nbsp;
