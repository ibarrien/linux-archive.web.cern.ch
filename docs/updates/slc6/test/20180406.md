## 2018-04-06

Package | Advisory | Notes
------- | -------- | -----
libvorbis-1.2.3-5.el6_9.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0649.html" target="secadv">RHSA-2018:0649</a> | &nbsp;
libvorbis-devel-1.2.3-5.el6_9.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0649.html" target="secadv">RHSA-2018:0649</a> | &nbsp;
libvorbis-devel-docs-1.2.3-5.el6_9.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0649.html" target="secadv">RHSA-2018:0649</a> | &nbsp;
thunderbird-52.7.0-1.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0647.html" target="secadv">RHSA-2018:0647</a> | &nbsp;
