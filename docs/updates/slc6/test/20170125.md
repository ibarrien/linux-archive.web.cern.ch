## 2017-01-25

Package | Advisory | Notes
------- | -------- | -----
rhev-hypervisor7-7.3-20170118.0.el6ev | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2017-0181.html" target="secadv">RHEA-2017:0181</a> | &nbsp;
sclo-php56-php-pecl-igbinary-2.0.1-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-pecl-igbinary-devel-2.0.1-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-igbinary-2.0.1-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php70-php-pecl-igbinary-devel-2.0.1-1.el6 | &nbsp; &nbsp; | &nbsp;
kmod-rtsx_pci-642-1.el6_8 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2017-0188.html" target="secadv">RHEA-2017:0188</a> | &nbsp;
mysql-5.1.73-8.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0184.html" target="secadv">RHSA-2017:0184</a> | &nbsp;
mysql-bench-5.1.73-8.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0184.html" target="secadv">RHSA-2017:0184</a> | &nbsp;
mysql-devel-5.1.73-8.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0184.html" target="secadv">RHSA-2017:0184</a> | &nbsp;
mysql-embedded-5.1.73-8.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0184.html" target="secadv">RHSA-2017:0184</a> | &nbsp;
mysql-embedded-devel-5.1.73-8.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0184.html" target="secadv">RHSA-2017:0184</a> | &nbsp;
mysql-libs-5.1.73-8.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0184.html" target="secadv">RHSA-2017:0184</a> | &nbsp;
mysql-server-5.1.73-8.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0184.html" target="secadv">RHSA-2017:0184</a> | &nbsp;
mysql-test-5.1.73-8.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0184.html" target="secadv">RHSA-2017:0184</a> | &nbsp;
squid34-3.4.14-9.el6_8.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0183.html" target="secadv">RHSA-2017:0183</a> | &nbsp;
