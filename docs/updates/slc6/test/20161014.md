## 2016-10-14

Package | Advisory | Notes
------- | -------- | -----
alpine-2.20-3.slc6 | &nbsp; &nbsp; | &nbsp;
alpine-2.20-4.slc6 | &nbsp; &nbsp; | &nbsp;
cern-get-sso-cookie-0.5.7-1.slc6 | &nbsp; &nbsp; | &nbsp;
perl-WWW-CERNSSO-Auth-0.5.7-1.slc6 | &nbsp; &nbsp; | &nbsp;
