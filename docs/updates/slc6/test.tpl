# Latest testing system updates for Scientific Linux CERN 6 (SLC6)

Please verify that your system is up to date, running as root:

```bash
/usr/bin/yum --enablerepo=*testing check-update 
```

If the above command shows you available updates apply these, running as root:

```bash
/usr/bin/yum --enablerepo=*testing update
```

or if you only want to apply security updates, run as root:

```bash
/usr/bin/yum --enablerepo=*testing --security update
```

For more information about update system please check: [Scientific Linux CERN 6 update documentation](/scientific6/docs/quickupdate)

For more information about software repositories please check: [SLC6 software repositories](/updates/slc6/)

**Update types:**

* <div class="adv_s">[S] - security</div>
* <div class="adv_b">[B] - bug fix</div>
* <div class="adv_e">[E] - enhancement</div>
