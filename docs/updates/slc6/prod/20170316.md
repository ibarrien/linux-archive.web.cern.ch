## 2017-03-16

Package | Advisory | Notes
------- | -------- | -----
rt-setup-1.60-11.el6rt | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2016-0600.html" target="secadv">RHEA-2016:0600</a> | &nbsp;
cern-get-keytab-1.0.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
cln-1.3.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
cln-devel-1.3.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
firefox-45.8.0-2.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0459.html" target="secadv">RHSA-2017:0459</a> | &nbsp;
HEP_OSlibs_SL6-1.0.17-0.el6 | &nbsp; &nbsp; | &nbsp;
kernel-module-openafs-2.6.32-642.13.1.el6-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
kernel-module-openafs-2.6.32-642.13.2.el6-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
kernel-module-openafs-2.6.32-642.15.1.el6-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-authlibs-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-authlibs-devel-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-client-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-compat-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-devel-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-docs-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-kernel-source-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-kpasswd-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-krb5-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
openafs-server-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
thunderbird-45.8.0-1.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0498.html" target="secadv">RHSA-2017:0498</a> | &nbsp;
tzdata-2017a-1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-0472.html" target="secadv">RHBA-2017:0472</a> | &nbsp;
tzdata-java-2017a-1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2017-0472.html" target="secadv">RHBA-2017:0472</a> | &nbsp;
