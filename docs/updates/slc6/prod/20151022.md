## 2015-10-22

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-11.2.202.540-3.slc6 | &nbsp; &nbsp; | &nbsp;
cern-config-users-1.8.2-1.slc6 | &nbsp; &nbsp; | &nbsp;
cern-config-users-1.8.2-2.slc6 | &nbsp; &nbsp; | &nbsp;
