## 2014-09-09

Package | Advisory | Notes
------- | -------- | -----
avahi-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-autoipd-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-compat-howl-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-compat-howl-devel-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-compat-libdns_sd-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-compat-libdns_sd-devel-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-devel-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-dnsconfd-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-glib-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-glib-devel-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-gobject-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-gobject-devel-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-libs-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-qt3-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-qt3-devel-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-qt4-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-qt4-devel-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-tools-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-ui-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-ui-devel-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
avahi-ui-tools-0.6.25-12.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1140.html" target="secadv">RHBA-2014:1140</a> | &nbsp;
cpio-2.10-12.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1150.html" target="secadv">RHBA-2014:1150</a> | &nbsp;
debugmode-9.03.40-2.el6_5.4 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1139.html" target="secadv">RHBA-2014:1139</a> | &nbsp;
firefox-24.8.0-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1144.html" target="secadv">RHSA-2014:1144</a> | &nbsp;
glibc-2.12-1.107.el6_4.6 | &nbsp; &nbsp; | &nbsp;
glibc-common-2.12-1.107.el6_4.6 | &nbsp; &nbsp; | &nbsp;
glibc-devel-2.12-1.107.el6_4.6 | &nbsp; &nbsp; | &nbsp;
glibc-headers-2.12-1.107.el6_4.6 | &nbsp; &nbsp; | &nbsp;
glibc-static-2.12-1.107.el6_4.6 | &nbsp; &nbsp; | &nbsp;
glibc-utils-2.12-1.107.el6_4.6 | &nbsp; &nbsp; | &nbsp;
initscripts-9.03.40-2.el6_5.4 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1139.html" target="secadv">RHBA-2014:1139</a> | &nbsp;
kmod-memstick_dup-0.1_rh1-1.el6_5 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2014-1111.html" target="secadv">RHEA-2014:1111</a> | &nbsp;
kmod-mmc_block_dup-0.1_rh1-1.el6_5 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2014-1111.html" target="secadv">RHEA-2014:1111</a> | &nbsp;
kmod-mmc_core_dup-0.1_rh1-1.el6_5 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2014-1111.html" target="secadv">RHEA-2014:1111</a> | &nbsp;
kmod-rtsx_pci-0.1_rh1-1.el6_5 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2014-1111.html" target="secadv">RHEA-2014:1111</a> | &nbsp;
kmod-rtsx_pci_ms-0.1_rh1-1.el6_5 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2014-1111.html" target="secadv">RHEA-2014:1111</a> | &nbsp;
kmod-rtsx_pci_sdmmc-0.1_rh1-1.el6_5 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2014-1111.html" target="secadv">RHEA-2014:1111</a> | &nbsp;
nscd-2.12-1.107.el6_4.6 | &nbsp; &nbsp; | &nbsp;
openscap-1.0.8-1.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1141.html" target="secadv">RHBA-2014:1141</a> | &nbsp;
openscap-content-1.0.8-1.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1141.html" target="secadv">RHBA-2014:1141</a> | &nbsp;
openscap-devel-1.0.8-1.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1141.html" target="secadv">RHBA-2014:1141</a> | &nbsp;
openscap-engine-sce-1.0.8-1.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1141.html" target="secadv">RHBA-2014:1141</a> | &nbsp;
openscap-engine-sce-devel-1.0.8-1.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1141.html" target="secadv">RHBA-2014:1141</a> | &nbsp;
openscap-extra-probes-1.0.8-1.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1141.html" target="secadv">RHBA-2014:1141</a> | &nbsp;
openscap-python-1.0.8-1.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1141.html" target="secadv">RHBA-2014:1141</a> | &nbsp;
openscap-utils-1.0.8-1.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-1141.html" target="secadv">RHBA-2014:1141</a> | &nbsp;
squid-3.1.10-22.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1148.html" target="secadv">RHSA-2014:1148</a> | &nbsp;
thunderbird-24.8.0-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-1145.html" target="secadv">RHSA-2014:1145</a> | &nbsp;
