## 2018-12-06

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-31.0.0.153-1.slc6 | &nbsp; &nbsp; | &nbsp;
perl-Authen-PAM-0.16-8.slc6 | &nbsp; &nbsp; | &nbsp;
thunderbird-60.2.1-5.el6 | &nbsp; &nbsp; | &nbsp;
yum-autoupdate-4.5.1-2.slc6 | &nbsp; &nbsp; | &nbsp;
