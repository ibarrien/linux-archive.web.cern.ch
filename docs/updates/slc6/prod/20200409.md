## 2020-04-09


Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-693.65.1.rt56.663.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1353.html" target="secadv">RHSA-2020:1353</a> | &nbsp;
kernel-rt-debug-3.10.0-693.65.1.rt56.663.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1353.html" target="secadv">RHSA-2020:1353</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-693.65.1.rt56.663.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1353.html" target="secadv">RHSA-2020:1353</a> | &nbsp;
kernel-rt-devel-3.10.0-693.65.1.rt56.663.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1353.html" target="secadv">RHSA-2020:1353</a> | &nbsp;
kernel-rt-doc-3.10.0-693.65.1.rt56.663.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1353.html" target="secadv">RHSA-2020:1353</a> | &nbsp;
kernel-rt-firmware-3.10.0-693.65.1.rt56.663.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1353.html" target="secadv">RHSA-2020:1353</a> | &nbsp;
kernel-rt-trace-3.10.0-693.65.1.rt56.663.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1353.html" target="secadv">RHSA-2020:1353</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-693.65.1.rt56.663.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1353.html" target="secadv">RHSA-2020:1353</a> | &nbsp;
kernel-rt-vanilla-3.10.0-693.65.1.rt56.663.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1353.html" target="secadv">RHSA-2020:1353</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-693.65.1.rt56.663.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1353.html" target="secadv">RHSA-2020:1353</a> | &nbsp;
firefox-68.6.1-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1339.html" target="secadv">RHSA-2020:1339</a> | &nbsp;
krb5-appl-clients-1.0.1-10.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1349.html" target="secadv">RHSA-2020:1349</a> | &nbsp;
krb5-appl-servers-1.0.1-10.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1349.html" target="secadv">RHSA-2020:1349</a> | &nbsp;
telnet-0.17-49.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1335.html" target="secadv">RHSA-2020:1335</a> | &nbsp;
telnet-server-0.17-49.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1335.html" target="secadv">RHSA-2020:1335</a> | &nbsp;

