## 2015-10-15

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-11.2.202.535-1.slc6 | &nbsp; &nbsp; | &nbsp;
rt-setup-1.60-8.el6rt | &nbsp; &nbsp; | &nbsp;
spice-server-0.12.4-12.el6_7.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1889.html" target="secadv">RHSA-2015:1889</a> | &nbsp;
spice-server-devel-0.12.4-12.el6_7.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-1889.html" target="secadv">RHSA-2015:1889</a> | &nbsp;
thunderbird-exchangecalendar-3.4.0-1.slc6 | &nbsp; &nbsp; | &nbsp;
tzdata-2015g-2.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-1863.html" target="secadv">RHEA-2015:1863</a> | &nbsp;
tzdata-java-2015g-2.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-1863.html" target="secadv">RHEA-2015:1863</a> | &nbsp;
