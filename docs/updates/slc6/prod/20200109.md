## 2020-01-09

Package | Advisory | Notes
------- | -------- | -----
ca-certificates-2019.2.32-65.1.el6_10 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2019-4252.html" target="secadv">RHEA-2019:4252</a> | &nbsp;
cern-wrappers-1-18.slc6 | &nbsp; &nbsp; | &nbsp;
cern-wrappers-passwd-1-18.slc6 | &nbsp; &nbsp; | &nbsp;
curl-7.19.7-54.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-4253.html" target="secadv">RHBA-2019:4253</a> | &nbsp;
freetype-2.3.11-19.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4254.html" target="secadv">RHSA-2019:4254</a> | &nbsp;
freetype-demos-2.3.11-19.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4254.html" target="secadv">RHSA-2019:4254</a> | &nbsp;
freetype-devel-2.3.11-19.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4254.html" target="secadv">RHSA-2019:4254</a> | &nbsp;
hepix-4.9.10-0.slc6 | &nbsp; &nbsp; | &nbsp;
kernel-2.6.32-754.25.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4256.html" target="secadv">RHSA-2019:4256</a> | &nbsp;
kernel-abi-whitelists-2.6.32-754.25.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4256.html" target="secadv">RHSA-2019:4256</a> | &nbsp;
kernel-debug-2.6.32-754.25.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4256.html" target="secadv">RHSA-2019:4256</a> | &nbsp;
kernel-debug-devel-2.6.32-754.25.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4256.html" target="secadv">RHSA-2019:4256</a> | &nbsp;
kernel-devel-2.6.32-754.25.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4256.html" target="secadv">RHSA-2019:4256</a> | &nbsp;
kernel-doc-2.6.32-754.25.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4256.html" target="secadv">RHSA-2019:4256</a> | &nbsp;
kernel-firmware-2.6.32-754.25.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4256.html" target="secadv">RHSA-2019:4256</a> | &nbsp;
kernel-headers-2.6.32-754.25.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4256.html" target="secadv">RHSA-2019:4256</a> | &nbsp;
kernel-module-openafs-2.6.32-754.25.1.el6-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
libcurl-7.19.7-54.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-4253.html" target="secadv">RHBA-2019:4253</a> | &nbsp;
libcurl-devel-7.19.7-54.el6_10 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-4253.html" target="secadv">RHBA-2019:4253</a> | &nbsp;
net-snmp-5.5-60.el6_10.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-4251.html" target="secadv">RHBA-2019:4251</a> | &nbsp;
net-snmp-devel-5.5-60.el6_10.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-4251.html" target="secadv">RHBA-2019:4251</a> | &nbsp;
net-snmp-libs-5.5-60.el6_10.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-4251.html" target="secadv">RHBA-2019:4251</a> | &nbsp;
net-snmp-perl-5.5-60.el6_10.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-4251.html" target="secadv">RHBA-2019:4251</a> | &nbsp;
net-snmp-python-5.5-60.el6_10.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-4251.html" target="secadv">RHBA-2019:4251</a> | &nbsp;
net-snmp-utils-5.5-60.el6_10.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-4251.html" target="secadv">RHBA-2019:4251</a> | &nbsp;
perf-2.6.32-754.25.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4256.html" target="secadv">RHSA-2019:4256</a> | &nbsp;
python-perf-2.6.32-754.25.1.el6 | &nbsp; &nbsp; | &nbsp;
thunderbird-68.3.0-3.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-4205.html" target="secadv">RHSA-2019:4205</a> | &nbsp;
