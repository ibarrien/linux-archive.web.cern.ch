## 2017-10-19

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.17-1.slc6 | &nbsp; &nbsp; | &nbsp;
aims2-server-2.17-1.slc6 | &nbsp; &nbsp; | &nbsp;
thunderbird-52.4.0-2.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-2885.html" target="secadv">RHSA-2017:2885</a> | &nbsp;
