## 2014-04-10

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-11.2.202.350-1.slc6 | &nbsp; &nbsp; | &nbsp;
pidgin-sipe-1.18.0-2.slc6 | &nbsp; &nbsp; | &nbsp;
httpd-2.2.15-30.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0370.html" target="secadv">RHSA-2014:0370</a> | &nbsp;
httpd-devel-2.2.15-30.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0370.html" target="secadv">RHSA-2014:0370</a> | &nbsp;
httpd-manual-2.2.15-30.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0370.html" target="secadv">RHSA-2014:0370</a> | &nbsp;
httpd-tools-2.2.15-30.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0370.html" target="secadv">RHSA-2014:0370</a> | &nbsp;
krb5-devel-1.10.3-15.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0359.html" target="secadv">RHBA-2014:0359</a> | &nbsp;
krb5-libs-1.10.3-15.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0359.html" target="secadv">RHBA-2014:0359</a> | &nbsp;
krb5-pkinit-openssl-1.10.3-15.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0359.html" target="secadv">RHBA-2014:0359</a> | &nbsp;
krb5-server-1.10.3-15.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0359.html" target="secadv">RHBA-2014:0359</a> | &nbsp;
krb5-server-ldap-1.10.3-15.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0359.html" target="secadv">RHBA-2014:0359</a> | &nbsp;
krb5-workstation-1.10.3-15.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0359.html" target="secadv">RHBA-2014:0359</a> | &nbsp;
libvirt-0.10.2-29.el6_5.7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0375.html" target="secadv">RHBA-2014:0375</a> | &nbsp;
libvirt-client-0.10.2-29.el6_5.7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0375.html" target="secadv">RHBA-2014:0375</a> | &nbsp;
libvirt-devel-0.10.2-29.el6_5.7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0375.html" target="secadv">RHBA-2014:0375</a> | &nbsp;
libvirt-lock-sanlock-0.10.2-29.el6_5.7 | &nbsp; &nbsp; | &nbsp;
libvirt-python-0.10.2-29.el6_5.7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0375.html" target="secadv">RHBA-2014:0375</a> | &nbsp;
mod_ssl-2.2.15-30.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0370.html" target="secadv">RHSA-2014:0370</a> | &nbsp;
qemu-guest-agent-0.12.1.2-2.415.el6_5.7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0360.html" target="secadv">RHBA-2014:0360</a> | &nbsp;
qemu-img-0.12.1.2-2.415.el6_5.7 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-0.12.1.2-2.415.el6_5.7 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-tools-0.12.1.2-2.415.el6_5.7 | &nbsp; &nbsp; | &nbsp;
rrdtool-1.3.8-7.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0356.html" target="secadv">RHBA-2014:0356</a> | &nbsp;
rrdtool-devel-1.3.8-7.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0356.html" target="secadv">RHBA-2014:0356</a> | &nbsp;
rrdtool-doc-1.3.8-7.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0356.html" target="secadv">RHBA-2014:0356</a> | &nbsp;
rrdtool-perl-1.3.8-7.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0356.html" target="secadv">RHBA-2014:0356</a> | &nbsp;
rrdtool-php-1.3.8-7.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0356.html" target="secadv">RHBA-2014:0356</a> | &nbsp;
rrdtool-python-1.3.8-7.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0356.html" target="secadv">RHBA-2014:0356</a> | &nbsp;
rrdtool-ruby-1.3.8-7.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0356.html" target="secadv">RHBA-2014:0356</a> | &nbsp;
rrdtool-tcl-1.3.8-7.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0356.html" target="secadv">RHBA-2014:0356</a> | &nbsp;
xalan-j2-2.7.0-9.9.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0348.html" target="secadv">RHSA-2014:0348</a> | &nbsp;
xalan-j2-demo-2.7.0-9.9.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0348.html" target="secadv">RHSA-2014:0348</a> | &nbsp;
xalan-j2-javadoc-2.7.0-9.9.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0348.html" target="secadv">RHSA-2014:0348</a> | &nbsp;
xalan-j2-manual-2.7.0-9.9.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0348.html" target="secadv">RHSA-2014:0348</a> | &nbsp;
xalan-j2-xsltc-2.7.0-9.9.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0348.html" target="secadv">RHSA-2014:0348</a> | &nbsp;
