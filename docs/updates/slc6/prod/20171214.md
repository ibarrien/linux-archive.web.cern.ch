## 2017-12-14

Package | Advisory | Notes
------- | -------- | -----
rt-setup-1.60-29.el6rt | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2017-3377.html" target="secadv">RHEA-2017:3377</a> | &nbsp;
firefox-52.5.1-1.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-3382.html" target="secadv">RHSA-2017:3382</a> | &nbsp;
hepix-4.0.14-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.7.0-openjdk-1.7.0.161-2.6.12.0.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-3392.html" target="secadv">RHSA-2017:3392</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.161-2.6.12.0.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-3392.html" target="secadv">RHSA-2017:3392</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.161-2.6.12.0.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-3392.html" target="secadv">RHSA-2017:3392</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.161-2.6.12.0.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-3392.html" target="secadv">RHSA-2017:3392</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.161-2.6.12.0.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-3392.html" target="secadv">RHSA-2017:3392</a> | &nbsp;
