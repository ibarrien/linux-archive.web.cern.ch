## 2020-04-23


Package | Advisory | Notes
------- | -------- | -----
CERN-CA-certs-20200420-1.slc6 | &nbsp; &nbsp; | &nbsp;
firefox-68.7.0-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1429.html" target="secadv">RHSA-2020:1429</a> | &nbsp;
java-1.7.0-openjdk-1.7.0.261-2.6.22.1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1508.html" target="secadv">RHSA-2020:1508</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.261-2.6.22.1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1508.html" target="secadv">RHSA-2020:1508</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.261-2.6.22.1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1508.html" target="secadv">RHSA-2020:1508</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.261-2.6.22.1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1508.html" target="secadv">RHSA-2020:1508</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.261-2.6.22.1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1508.html" target="secadv">RHSA-2020:1508</a> | &nbsp;
java-1.8.0-openjdk-1.8.0.252.b09-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1506.html" target="secadv">RHSA-2020:1506</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.252.b09-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1506.html" target="secadv">RHSA-2020:1506</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.252.b09-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1506.html" target="secadv">RHSA-2020:1506</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.252.b09-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1506.html" target="secadv">RHSA-2020:1506</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.252.b09-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1506.html" target="secadv">RHSA-2020:1506</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.252.b09-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1506.html" target="secadv">RHSA-2020:1506</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.252.b09-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1506.html" target="secadv">RHSA-2020:1506</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.252.b09-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1506.html" target="secadv">RHSA-2020:1506</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.252.b09-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1506.html" target="secadv">RHSA-2020:1506</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.252.b09-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1506.html" target="secadv">RHSA-2020:1506</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.252.b09-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1506.html" target="secadv">RHSA-2020:1506</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.252.b09-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1506.html" target="secadv">RHSA-2020:1506</a> | &nbsp;
qemu-guest-agent-0.12.1.2-2.506.el6_10.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1403.html" target="secadv">RHSA-2020:1403</a> | &nbsp;
qemu-img-0.12.1.2-2.506.el6_10.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1403.html" target="secadv">RHSA-2020:1403</a> | &nbsp;
qemu-kvm-0.12.1.2-2.506.el6_10.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1403.html" target="secadv">RHSA-2020:1403</a> | &nbsp;
qemu-kvm-tools-0.12.1.2-2.506.el6_10.7 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1403.html" target="secadv">RHSA-2020:1403</a> | &nbsp;
thunderbird-68.7.0-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-1488.html" target="secadv">RHSA-2020:1488</a> | &nbsp;

