## 2020-03-12

Package | Advisory | Notes
------- | -------- | -----
insights-client-3.0.13-1.el6_10 | &nbsp; &nbsp; | &nbsp;
nfs-utils-1.2.3-78.el6_10.2 | &nbsp; &nbsp; | &nbsp;
qemu-guest-agent-0.12.1.2-2.506.el6_10.6 | &nbsp; &nbsp; | &nbsp;
qemu-img-0.12.1.2-2.506.el6_10.6 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-0.12.1.2-2.506.el6_10.6 | &nbsp; &nbsp; | &nbsp;
qemu-kvm-tools-0.12.1.2-2.506.el6_10.6 | &nbsp; &nbsp; | &nbsp;
sudo-1.8.6p3-29.el6_10.3 | &nbsp; &nbsp; | &nbsp;
sudo-devel-1.8.6p3-29.el6_10.3 | &nbsp; &nbsp; | &nbsp;
