## 2015-04-29

Package | Advisory | Notes
------- | -------- | -----
kmod-spl-2.6.32-504.16.2.el6.x86_64-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-spl-devel-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-spl-devel-2.6.32-504.16.2.el6.x86_64-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-zfs-2.6.32-504.16.2.el6.x86_64-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-zfs-devel-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-zfs-devel-2.6.32-504.16.2.el6.x86_64-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
libnvpair1-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
libzfs2-devel-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
zfs-dracut-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
zfs-test-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
dbus-1.2.24-8.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0871.html" target="secadv">RHBA-2015:0871</a> | &nbsp;
dbus-devel-1.2.24-8.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0871.html" target="secadv">RHBA-2015:0871</a> | &nbsp;
dbus-doc-1.2.24-8.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0871.html" target="secadv">RHBA-2015:0871</a> | &nbsp;
dbus-libs-1.2.24-8.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0871.html" target="secadv">RHBA-2015:0871</a> | &nbsp;
dbus-x11-1.2.24-8.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0871.html" target="secadv">RHBA-2015:0871</a> | &nbsp;
kernel-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-abi-whitelists-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-debug-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-debug-devel-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-devel-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-doc-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-firmware-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-headers-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
kernel-module-openafs-2.6.32-504.16.2.el6-1.6.6-cern3.0.slc6 | &nbsp; &nbsp; | &nbsp;
kexec-tools-2.0.0-280.el6_6.2 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-0811.html" target="secadv">RHEA-2015:0811</a> | &nbsp;
kexec-tools-eppic-2.0.0-280.el6_6.2 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2015-0811.html" target="secadv">RHEA-2015:0811</a> | &nbsp;
libuutil1-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
libzfs2-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
libzpool2-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
python-perf-2.6.32-504.16.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0864.html" target="secadv">RHSA-2015:0864</a> | &nbsp;
spl-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
zfs-0.6.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
