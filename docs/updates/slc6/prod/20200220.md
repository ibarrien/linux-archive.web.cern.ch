## 2020-02-20

Package | Advisory | Notes
------- | -------- | -----
firefox-68.5.0-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0521.html" target="secadv">RHSA-2020:0521</a> | &nbsp;
ksh-20120801-38.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0515.html" target="secadv">RHSA-2020:0515</a> | &nbsp;
