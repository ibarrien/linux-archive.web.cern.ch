## 2019-10-31

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.10.0-693.60.1.rt56.654.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3165.html" target="secadv">RHSA-2019:3165</a> | &nbsp;
kernel-rt-debug-3.10.0-693.60.1.rt56.654.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3165.html" target="secadv">RHSA-2019:3165</a> | &nbsp;
kernel-rt-debug-devel-3.10.0-693.60.1.rt56.654.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3165.html" target="secadv">RHSA-2019:3165</a> | &nbsp;
kernel-rt-devel-3.10.0-693.60.1.rt56.654.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3165.html" target="secadv">RHSA-2019:3165</a> | &nbsp;
kernel-rt-doc-3.10.0-693.60.1.rt56.654.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3165.html" target="secadv">RHSA-2019:3165</a> | &nbsp;
kernel-rt-firmware-3.10.0-693.60.1.rt56.654.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3165.html" target="secadv">RHSA-2019:3165</a> | &nbsp;
kernel-rt-trace-3.10.0-693.60.1.rt56.654.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3165.html" target="secadv">RHSA-2019:3165</a> | &nbsp;
kernel-rt-trace-devel-3.10.0-693.60.1.rt56.654.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3165.html" target="secadv">RHSA-2019:3165</a> | &nbsp;
kernel-rt-vanilla-3.10.0-693.60.1.rt56.654.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3165.html" target="secadv">RHSA-2019:3165</a> | &nbsp;
kernel-rt-vanilla-devel-3.10.0-693.60.1.rt56.654.el6rt | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3165.html" target="secadv">RHSA-2019:3165</a> | &nbsp;
java-1.7.0-openjdk-1.7.0.241-2.6.20.0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3158.html" target="secadv">RHSA-2019:3158</a> | &nbsp;
java-1.7.0-openjdk-demo-1.7.0.241-2.6.20.0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3158.html" target="secadv">RHSA-2019:3158</a> | &nbsp;
java-1.7.0-openjdk-devel-1.7.0.241-2.6.20.0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3158.html" target="secadv">RHSA-2019:3158</a> | &nbsp;
java-1.7.0-openjdk-javadoc-1.7.0.241-2.6.20.0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3158.html" target="secadv">RHSA-2019:3158</a> | &nbsp;
java-1.7.0-openjdk-src-1.7.0.241-2.6.20.0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-3158.html" target="secadv">RHSA-2019:3158</a> | &nbsp;
