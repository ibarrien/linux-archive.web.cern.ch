## 2019-02-28

Package | Advisory | Notes
------- | -------- | -----
aims2-client-2.25-2.slc6 | &nbsp; &nbsp; | &nbsp;
aims2-server-2.25-2.slc6 | &nbsp; &nbsp; | &nbsp;
firefox-60.5.1-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2019-0373.html" target="secadv">RHSA-2019:0373</a> | &nbsp;
oracle-instantclient-tnsnames.ora-1.4.3-1.slc6 | &nbsp; &nbsp; | &nbsp;
