## 2020-10-29


Package | Advisory | Notes
------- | -------- | -----
firefox-78.4.0-2.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4330.html" target="secadv">RHSA-2020:4330</a> | &nbsp;
java-1.8.0-openjdk-1.8.0.272.b10-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4348.html" target="secadv">RHSA-2020:4348</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.272.b10-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4348.html" target="secadv">RHSA-2020:4348</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.272.b10-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4348.html" target="secadv">RHSA-2020:4348</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.272.b10-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4348.html" target="secadv">RHSA-2020:4348</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.272.b10-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4348.html" target="secadv">RHSA-2020:4348</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.272.b10-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4348.html" target="secadv">RHSA-2020:4348</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.272.b10-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4348.html" target="secadv">RHSA-2020:4348</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.272.b10-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4348.html" target="secadv">RHSA-2020:4348</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.272.b10-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4348.html" target="secadv">RHSA-2020:4348</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.272.b10-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4348.html" target="secadv">RHSA-2020:4348</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.272.b10-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4348.html" target="secadv">RHSA-2020:4348</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.272.b10-0.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4348.html" target="secadv">RHSA-2020:4348</a> | &nbsp;
tzdata-2020b-2.el6_4 | &nbsp; &nbsp; | &nbsp;
tzdata-2020d-1.el6_4 | &nbsp; &nbsp; | &nbsp;
tzdata-java-2020b-2.el6_4 | &nbsp; &nbsp; | &nbsp;
tzdata-java-2020d-1.el6_4 | &nbsp; &nbsp; | &nbsp;

