## 2019-07-11

Package | Advisory | Notes
------- | -------- | -----
cern-linuxsupport-access-1.0-1.slc6 | &nbsp; &nbsp; | &nbsp;
kernel-2.6.32-754.17.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-1651.html" target="secadv">RHBA-2019:1651</a> | &nbsp;
kernel-abi-whitelists-2.6.32-754.17.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-1651.html" target="secadv">RHBA-2019:1651</a> | &nbsp;
kernel-debug-2.6.32-754.17.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-1651.html" target="secadv">RHBA-2019:1651</a> | &nbsp;
kernel-debug-devel-2.6.32-754.17.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-1651.html" target="secadv">RHBA-2019:1651</a> | &nbsp;
kernel-devel-2.6.32-754.17.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-1651.html" target="secadv">RHBA-2019:1651</a> | &nbsp;
kernel-doc-2.6.32-754.17.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-1651.html" target="secadv">RHBA-2019:1651</a> | &nbsp;
kernel-firmware-2.6.32-754.17.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-1651.html" target="secadv">RHBA-2019:1651</a> | &nbsp;
kernel-headers-2.6.32-754.17.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-1651.html" target="secadv">RHBA-2019:1651</a> | &nbsp;
kernel-module-openafs-2.6.32-754.17.1.el6-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
koji-1.17.0-7.slc6 | &nbsp; &nbsp; | &nbsp;
koji-builder-1.17.0-7.slc6 | &nbsp; &nbsp; | &nbsp;
koji-builder-plugins-1.17.0-7.slc6 | &nbsp; &nbsp; | &nbsp;
koji-hub-1.17.0-7.slc6 | &nbsp; &nbsp; | &nbsp;
koji-hub-plugins-1.17.0-7.slc6 | &nbsp; &nbsp; | &nbsp;
koji-utils-1.17.0-7.slc6 | &nbsp; &nbsp; | &nbsp;
koji-vm-1.17.0-7.slc6 | &nbsp; &nbsp; | &nbsp;
koji-web-1.17.0-7.slc6 | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-754.17.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-1651.html" target="secadv">RHBA-2019:1651</a> | &nbsp;
python2-koji-1.17.0-7.slc6 | &nbsp; &nbsp; | &nbsp;
python2-koji-cli-plugins-1.17.0-7.slc6 | &nbsp; &nbsp; | &nbsp;
python2-koji-hub-1.17.0-7.slc6 | &nbsp; &nbsp; | &nbsp;
python2-koji-hub-plugins-1.17.0-7.slc6 | &nbsp; &nbsp; | &nbsp;
python2-koji-web-1.17.0-7.slc6 | &nbsp; &nbsp; | &nbsp;
python-perf-2.6.32-754.17.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-1651.html" target="secadv">RHBA-2019:1651</a> | &nbsp;
tzdata-2019b-2.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-1703.html" target="secadv">RHBA-2019:1703</a> | &nbsp;
tzdata-java-2019b-2.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2019-1703.html" target="secadv">RHBA-2019:1703</a> | &nbsp;
