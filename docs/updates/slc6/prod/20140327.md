## 2014-03-27

Package | Advisory | Notes
------- | -------- | -----
devtoolset-2-2.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-batik-1.8-0.13.svn1230816.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-binutils-2.23.52.0.1-10.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-binutils-devel-2.23.52.0.1-10.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-build-2.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-dyninst-8.0-6dw.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-dyninst-devel-8.0-6dw.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-dyninst-doc-8.0-6dw.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-dyninst-static-8.0-6dw.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-dyninst-testsuite-8.0-6dw.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-cdt-8.2.1-3.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-cdt-parsers-8.2.1-3.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-cdt-sdk-8.2.1-3.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-cdt-tests-8.2.1-3.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-changelog-2.8.3-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-ecf-core-3.6.1-1.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-egit-3.1.0-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-emf-2.9.1-1.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-emf-core-2.9.1-1.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-emf-examples-2.9.1-1.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-emf-sdk-2.9.1-1.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-emf-xsd-2.9.1-1.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-emf-xsd-sdk-2.9.1-1.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-equinox-osgi-4.3.1-6.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-gcov-2.2.0-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-gef-3.9.1-0.3.gitb9f2e9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-gef-examples-3.9.1-0.3.gitb9f2e9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-gef-sdk-3.9.1-0.3.gitb9f2e9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-gprof-2.2.0-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-jdt-4.3.1-6.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-jgit-3.1.0-3.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-linuxtools-2.2.0-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-linuxtools-tests-2.2.0-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-manpage-2.2.0-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-builds-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-builds-hudson-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-context-cdt-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-context-java-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-context-pde-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-context-team-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-docs-epub-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-docs-htmltext-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-docs-wikitext-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-ide-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-sdk-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-tasks-bugzilla-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-tasks-trac-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-tasks-web-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-versions-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-versions-cvs-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-mylyn-versions-git-3.9.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-oprofile-2.2.0-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-pde-4.3.1-6.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-perf-2.2.0-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-platform-4.3.1-6.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-rpm-editor-2.2.0-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-rpmstubby-2.2.0-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-rse-3.5.1-0.1.RC4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-rse-server-3.5.1-0.1.RC4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-swt-4.3.1-6.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-systemtap-2.2.0-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-tests-4.3.0-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-tests-4.3.1-6.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-eclipse-valgrind-2.2.0-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-elfutils-0.157-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-elfutils-devel-0.157-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-elfutils-libelf-0.157-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-elfutils-libelf-devel-0.157-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-elfutils-libs-0.157-2.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-emacs-git-1.8.4-9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-emacs-git-el-1.8.4-9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-gcc-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-gcc-c++-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-gcc-gfortran-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-gcc-plugin-devel-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-gdb-7.6.1-47.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-gdb-doc-7.6.1-47.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-gdb-gdbserver-7.6.1-47.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-git-1.8.4-9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-git-all-1.8.4-9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-git-cvs-1.8.4-9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-git-daemon-1.8.4-9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-git-email-1.8.4-9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-git-gui-1.8.4-9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-gitk-1.8.4-9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-git-svn-1.8.4-9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-gitweb-1.8.4-9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-ide-2.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-javaewah-0.6.12-3.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-jetty-continuation-8.1.5-8.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-jetty-http-8.1.5-8.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-jetty-io-8.1.5-8.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-jetty-javadoc-8.1.5-8.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-jetty-project-8.1.5-8.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-jetty-security-8.1.5-8.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-jetty-server-8.1.5-8.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-jetty-servlet-8.1.5-8.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-jetty-util-8.1.5-8.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-jgit-3.1.0-3.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-jgit-javadoc-3.1.0-3.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-libasan-devel-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-libatomic-devel-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-libitm-devel-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-libquadmath-devel-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-libstdc++-devel-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-libstdc++-docs-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-libtsan-devel-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-oprofile-0.9.8-6.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-oprofile-devel-0.9.8-6.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-oprofile-gui-0.9.8-6.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-oprofile-jit-0.9.8-6.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-perftools-2.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-perl-Git-1.8.4-9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-perl-Git-SVN-1.8.4-9.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-runtime-2.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-strace-4.7-13.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-swt-chart-0.8.0-10.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-swt-chart-javadoc-0.8.0-10.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-toolchain-2.1-4.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-valgrind-3.8.1-30.8.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-valgrind-devel-3.8.1-30.8.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-valgrind-openmpi-3.8.1-30.8.el6 | &nbsp; &nbsp; | &nbsp;
devtoolset-2-vc-2.1-4.el6 | &nbsp; &nbsp; | &nbsp;
libasan-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
libatomic-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
libitm-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
libtsan-4.8.2-15.el6 | &nbsp; &nbsp; | &nbsp;
scl-utils-20120927-8.el6_5 | &nbsp; &nbsp; | &nbsp;
scl-utils-build-20120927-8.el6_5 | &nbsp; &nbsp; | &nbsp;
autofs-5.0.5-89.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0314.html" target="secadv">RHBA-2014:0314</a> | &nbsp;
CERN-CA-certs-20140325-2.slc6 | &nbsp; &nbsp; | &nbsp;
dmidecode-2.12-5.el6_5 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2014-0326.html" target="secadv">RHEA-2014:0326</a> | &nbsp;
environment-modules-3.2.10-1.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0327.html" target="secadv">RHBA-2014:0327</a> | &nbsp;
grep-2.6.3-4.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0318.html" target="secadv">RHBA-2014:0318</a> | &nbsp;
kernel-2.6.32-431.11.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0328.html" target="secadv">RHSA-2014:0328</a> | &nbsp;
kernel-abi-whitelists-2.6.32-431.11.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0328.html" target="secadv">RHSA-2014:0328</a> | &nbsp;
kernel-debug-2.6.32-431.11.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0328.html" target="secadv">RHSA-2014:0328</a> | &nbsp;
kernel-debug-devel-2.6.32-431.11.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0328.html" target="secadv">RHSA-2014:0328</a> | &nbsp;
kernel-devel-2.6.32-431.11.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0328.html" target="secadv">RHSA-2014:0328</a> | &nbsp;
kernel-doc-2.6.32-431.11.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0328.html" target="secadv">RHSA-2014:0328</a> | &nbsp;
kernel-firmware-2.6.32-431.11.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0328.html" target="secadv">RHSA-2014:0328</a> | &nbsp;
kernel-headers-2.6.32-431.11.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0328.html" target="secadv">RHSA-2014:0328</a> | &nbsp;
kernel-module-openafs-2.6.32-431.11.2.el6-1.6.6-cern2.0.slc6 | &nbsp; &nbsp; | &nbsp;
libsmbclient-3.6.9-168.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0330.html" target="secadv">RHSA-2014:0330</a> | &nbsp;
libsmbclient-devel-3.6.9-168.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0330.html" target="secadv">RHSA-2014:0330</a> | &nbsp;
man-pages-overrides-6.5.3-1.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0333.html" target="secadv">RHBA-2014:0333</a> | &nbsp;
net-snmp-5.5-49.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0321.html" target="secadv">RHSA-2014:0321</a> | &nbsp;
net-snmp-devel-5.5-49.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0321.html" target="secadv">RHSA-2014:0321</a> | &nbsp;
net-snmp-libs-5.5-49.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0321.html" target="secadv">RHSA-2014:0321</a> | &nbsp;
net-snmp-perl-5.5-49.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0321.html" target="secadv">RHSA-2014:0321</a> | &nbsp;
net-snmp-python-5.5-49.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0321.html" target="secadv">RHSA-2014:0321</a> | &nbsp;
net-snmp-utils-5.5-49.el6_5.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0321.html" target="secadv">RHSA-2014:0321</a> | &nbsp;
perf-2.6.32-431.11.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0328.html" target="secadv">RHSA-2014:0328</a> | &nbsp;
python-perf-2.6.32-431.11.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0328.html" target="secadv">RHSA-2014:0328</a> | &nbsp;
quota-3.17-21.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0313.html" target="secadv">RHBA-2014:0313</a> | &nbsp;
quota-devel-3.17-21.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0313.html" target="secadv">RHBA-2014:0313</a> | &nbsp;
samba-3.6.9-168.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0330.html" target="secadv">RHSA-2014:0330</a> | &nbsp;
samba-client-3.6.9-168.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0330.html" target="secadv">RHSA-2014:0330</a> | &nbsp;
samba-common-3.6.9-168.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0330.html" target="secadv">RHSA-2014:0330</a> | &nbsp;
samba-doc-3.6.9-168.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0330.html" target="secadv">RHSA-2014:0330</a> | &nbsp;
samba-domainjoin-gui-3.6.9-168.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0330.html" target="secadv">RHSA-2014:0330</a> | &nbsp;
samba-swat-3.6.9-168.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0330.html" target="secadv">RHSA-2014:0330</a> | &nbsp;
samba-winbind-3.6.9-168.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0330.html" target="secadv">RHSA-2014:0330</a> | &nbsp;
samba-winbind-clients-3.6.9-168.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0330.html" target="secadv">RHSA-2014:0330</a> | &nbsp;
samba-winbind-devel-3.6.9-168.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0330.html" target="secadv">RHSA-2014:0330</a> | &nbsp;
samba-winbind-krb5-locator-3.6.9-168.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0330.html" target="secadv">RHSA-2014:0330</a> | &nbsp;
selinux-policy-3.7.19-231.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0324.html" target="secadv">RHBA-2014:0324</a> | &nbsp;
selinux-policy-doc-3.7.19-231.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0324.html" target="secadv">RHBA-2014:0324</a> | &nbsp;
selinux-policy-minimum-3.7.19-231.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0324.html" target="secadv">RHBA-2014:0324</a> | &nbsp;
selinux-policy-mls-3.7.19-231.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0324.html" target="secadv">RHBA-2014:0324</a> | &nbsp;
selinux-policy-targeted-3.7.19-231.el6_5.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0324.html" target="secadv">RHBA-2014:0324</a> | &nbsp;
spice-vdagent-0.14.0-3.el6_5 | &nbsp; &nbsp; | &nbsp;
star-1.5-11.1.el6_5 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0332.html" target="secadv">RHBA-2014:0332</a> | &nbsp;
thunderbird-24.4.0-1.el6_5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0316.html" target="secadv">RHSA-2014:0316</a> | &nbsp;
