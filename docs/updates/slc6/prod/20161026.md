## 2016-10-26

Package | Advisory | Notes
------- | -------- | -----
java-1.8.0-openjdk-1.8.0.111-0.b15.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2079.html" target="secadv">RHSA-2016:2079</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.111-0.b15.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2079.html" target="secadv">RHSA-2016:2079</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.111-0.b15.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2079.html" target="secadv">RHSA-2016:2079</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.111-0.b15.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2079.html" target="secadv">RHSA-2016:2079</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.111-0.b15.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2079.html" target="secadv">RHSA-2016:2079</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.111-0.b15.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2079.html" target="secadv">RHSA-2016:2079</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.111-0.b15.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2079.html" target="secadv">RHSA-2016:2079</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.111-0.b15.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2079.html" target="secadv">RHSA-2016:2079</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.111-0.b15.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2079.html" target="secadv">RHSA-2016:2079</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.111-0.b15.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2079.html" target="secadv">RHSA-2016:2079</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.111-0.b15.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2079.html" target="secadv">RHSA-2016:2079</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.111-0.b15.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2079.html" target="secadv">RHSA-2016:2079</a> | &nbsp;
kernel-2.6.32-642.6.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2105.html" target="secadv">RHSA-2016:2105</a> | &nbsp;
kernel-abi-whitelists-2.6.32-642.6.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2105.html" target="secadv">RHSA-2016:2105</a> | &nbsp;
kernel-debug-2.6.32-642.6.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2105.html" target="secadv">RHSA-2016:2105</a> | &nbsp;
kernel-debug-devel-2.6.32-642.6.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2105.html" target="secadv">RHSA-2016:2105</a> | &nbsp;
kernel-devel-2.6.32-642.6.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2105.html" target="secadv">RHSA-2016:2105</a> | &nbsp;
kernel-doc-2.6.32-642.6.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2105.html" target="secadv">RHSA-2016:2105</a> | &nbsp;
kernel-firmware-2.6.32-642.6.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2105.html" target="secadv">RHSA-2016:2105</a> | &nbsp;
kernel-headers-2.6.32-642.6.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2105.html" target="secadv">RHSA-2016:2105</a> | &nbsp;
kernel-module-openafs-2.6.32-642.6.2.el6-1.6.6-cern4.1.slc6 | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-642.6.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2105.html" target="secadv">RHSA-2016:2105</a> | &nbsp;
python-perf-2.6.32-642.6.2.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2105.html" target="secadv">RHSA-2016:2105</a> | &nbsp;
tzdata-2016h-1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-2096.html" target="secadv">RHBA-2016:2096</a> | &nbsp;
tzdata-java-2016h-1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-2096.html" target="secadv">RHBA-2016:2096</a> | &nbsp;
