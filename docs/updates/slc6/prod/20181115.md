## 2018-11-15

Package | Advisory | Notes
------- | -------- | -----
rh-perl524-mod_perl-2.0.9-10.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2826.html" target="secadv">RHSA-2018:2826</a> | &nbsp;
rh-perl524-mod_perl-devel-2.0.9-10.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-2826.html" target="secadv">RHSA-2018:2826</a> | &nbsp;
yum-autoupdate-4.5.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
