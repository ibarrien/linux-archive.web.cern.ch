## 2016-08-11

Package | Advisory | Notes
------- | -------- | -----
rt-setup-1.60-14.el6rt | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2016-1575.html" target="secadv">RHEA-2016:1575</a> | &nbsp;
arc-44-40.6.slc6 | &nbsp; &nbsp; | &nbsp;
arc-server-44-40.6.slc6 | &nbsp; &nbsp; | &nbsp;
firefox-45.3.0-1.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1551.html" target="secadv">RHSA-2016:1551</a> | &nbsp;
qemu-guest-agent-0.12.1.2-2.491.el6_8.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1585.html" target="secadv">RHSA-2016:1585</a> | &nbsp;
qemu-img-0.12.1.2-2.491.el6_8.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1585.html" target="secadv">RHSA-2016:1585</a> | &nbsp;
qemu-kvm-0.12.1.2-2.491.el6_8.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1585.html" target="secadv">RHSA-2016:1585</a> | &nbsp;
qemu-kvm-tools-0.12.1.2-2.491.el6_8.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1585.html" target="secadv">RHSA-2016:1585</a> | &nbsp;
squid-3.1.23-16.el6_8.6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1573.html" target="secadv">RHSA-2016:1573</a> | &nbsp;
