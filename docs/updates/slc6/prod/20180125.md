## 2018-01-25

Package | Advisory | Notes
------- | -------- | -----
bind-9.8.2-0.62.rc1.el6_9.5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0101.html" target="secadv">RHSA-2018:0101</a> | &nbsp;
bind-chroot-9.8.2-0.62.rc1.el6_9.5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0101.html" target="secadv">RHSA-2018:0101</a> | &nbsp;
bind-devel-9.8.2-0.62.rc1.el6_9.5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0101.html" target="secadv">RHSA-2018:0101</a> | &nbsp;
bind-libs-9.8.2-0.62.rc1.el6_9.5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0101.html" target="secadv">RHSA-2018:0101</a> | &nbsp;
bind-sdb-9.8.2-0.62.rc1.el6_9.5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0101.html" target="secadv">RHSA-2018:0101</a> | &nbsp;
bind-utils-9.8.2-0.62.rc1.el6_9.5 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0101.html" target="secadv">RHSA-2018:0101</a> | &nbsp;
curl-openssl-7.57.0-1.1.slc6 | &nbsp; &nbsp; | &nbsp;
flash-plugin-28.0.0.137-1.slc6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-1.8.0.161-3.b14.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0095.html" target="secadv">RHSA-2018:0095</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.161-3.b14.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0095.html" target="secadv">RHSA-2018:0095</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.161-3.b14.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0095.html" target="secadv">RHSA-2018:0095</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.161-3.b14.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0095.html" target="secadv">RHSA-2018:0095</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.161-3.b14.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0095.html" target="secadv">RHSA-2018:0095</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.161-3.b14.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0095.html" target="secadv">RHSA-2018:0095</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.161-3.b14.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0095.html" target="secadv">RHSA-2018:0095</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.161-3.b14.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0095.html" target="secadv">RHSA-2018:0095</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.161-3.b14.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0095.html" target="secadv">RHSA-2018:0095</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.161-3.b14.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0095.html" target="secadv">RHSA-2018:0095</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.161-3.b14.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0095.html" target="secadv">RHSA-2018:0095</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.161-3.b14.el6_9 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2018-0095.html" target="secadv">RHSA-2018:0095</a> | &nbsp;
libcurl-openssl-7.57.0-1.1.slc6 | &nbsp; &nbsp; | &nbsp;
libcurl-openssl-devel-7.57.0-1.1.slc6 | &nbsp; &nbsp; | &nbsp;
liblog4shib1-1.0.9-3.3.slc6 | &nbsp; &nbsp; | &nbsp;
liblog4shib-devel-1.0.9-3.3.slc6 | &nbsp; &nbsp; | &nbsp;
libsaml9-2.6.1-3.1.slc6 | &nbsp; &nbsp; | &nbsp;
libsaml-devel-2.6.1-3.1.slc6 | &nbsp; &nbsp; | &nbsp;
libshibresolver1-1.0.0-4.5.slc6.1 | &nbsp; &nbsp; | &nbsp;
libshibresolver-devel-1.0.0-4.5.slc6.1 | &nbsp; &nbsp; | &nbsp;
libxerces-c-3_1-3.1.4-1.2.slc6 | &nbsp; &nbsp; | &nbsp;
libxerces-c-devel-3.1.4-1.2.slc6 | &nbsp; &nbsp; | &nbsp;
libxml-security-c17-1.7.3-3.2.slc6.1 | &nbsp; &nbsp; | &nbsp;
libxml-security-c-devel-1.7.3-3.2.slc6.1 | &nbsp; &nbsp; | &nbsp;
libxmltooling7-1.6.3-3.1.slc6 | &nbsp; &nbsp; | &nbsp;
libxmltooling-devel-1.6.3-3.1.slc6 | &nbsp; &nbsp; | &nbsp;
opensaml-bin-2.6.1-3.1.slc6 | &nbsp; &nbsp; | &nbsp;
opensaml-schemas-2.6.1-3.1.slc6 | &nbsp; &nbsp; | &nbsp;
shibboleth-2.6.1-3.1.slc6 | &nbsp; &nbsp; | &nbsp;
shibboleth-devel-2.6.1-3.1.slc6 | &nbsp; &nbsp; | &nbsp;
shibboleth-embedded-ds-1.2.0-4.3.slc6 | &nbsp; &nbsp; | &nbsp;
xerces-c-bin-3.1.4-1.2.slc6 | &nbsp; &nbsp; | &nbsp;
xml-security-c-bin-1.7.3-3.2.slc6.1 | &nbsp; &nbsp; | &nbsp;
xmltooling-schemas-1.6.3-3.1.slc6 | &nbsp; &nbsp; | &nbsp;
