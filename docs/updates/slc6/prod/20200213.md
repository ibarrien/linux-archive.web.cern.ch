## 2020-02-13

Package | Advisory | Notes
------- | -------- | -----
spice-glib-0.26-8.el6_10.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0471.html" target="secadv">RHSA-2020:0471</a> | &nbsp;
spice-glib-devel-0.26-8.el6_10.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0471.html" target="secadv">RHSA-2020:0471</a> | &nbsp;
spice-gtk-0.26-8.el6_10.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0471.html" target="secadv">RHSA-2020:0471</a> | &nbsp;
spice-gtk-devel-0.26-8.el6_10.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0471.html" target="secadv">RHSA-2020:0471</a> | &nbsp;
spice-gtk-python-0.26-8.el6_10.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0471.html" target="secadv">RHSA-2020:0471</a> | &nbsp;
spice-gtk-tools-0.26-8.el6_10.2 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-0471.html" target="secadv">RHSA-2020:0471</a> | &nbsp;
