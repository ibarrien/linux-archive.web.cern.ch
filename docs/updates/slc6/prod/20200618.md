## 2020-06-18


Package | Advisory | Notes
------- | -------- | -----
insights-client-3.0.14-4.el6_10 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2020-2517.html" target="secadv">RHEA-2020:2517</a> | &nbsp;
libexif-0.6.21-6.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2516.html" target="secadv">RHSA-2020:2516</a> | &nbsp;
libexif-devel-0.6.21-6.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2516.html" target="secadv">RHSA-2020:2516</a> | &nbsp;
tomcat6-6.0.24-115.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2529.html" target="secadv">RHSA-2020:2529</a> | &nbsp;
tomcat6-admin-webapps-6.0.24-115.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2529.html" target="secadv">RHSA-2020:2529</a> | &nbsp;
tomcat6-docs-webapp-6.0.24-115.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2529.html" target="secadv">RHSA-2020:2529</a> | &nbsp;
tomcat6-el-2.1-api-6.0.24-115.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2529.html" target="secadv">RHSA-2020:2529</a> | &nbsp;
tomcat6-javadoc-6.0.24-115.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2529.html" target="secadv">RHSA-2020:2529</a> | &nbsp;
tomcat6-jsp-2.1-api-6.0.24-115.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2529.html" target="secadv">RHSA-2020:2529</a> | &nbsp;
tomcat6-lib-6.0.24-115.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2529.html" target="secadv">RHSA-2020:2529</a> | &nbsp;
tomcat6-servlet-2.5-api-6.0.24-115.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2529.html" target="secadv">RHSA-2020:2529</a> | &nbsp;
tomcat6-webapps-6.0.24-115.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-2529.html" target="secadv">RHSA-2020:2529</a> | &nbsp;

