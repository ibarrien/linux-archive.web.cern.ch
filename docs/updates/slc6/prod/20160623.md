## 2016-06-23

Package | Advisory | Notes
------- | -------- | -----
firefox-45.1.1-1.el6_7 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1230.html" target="secadv">RHBA-2016:1230</a> | &nbsp;
flash-plugin-11.2.202.626-1.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-6.7.2.7-5.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1237.html" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-6.7.2.7-5.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-c++-6.7.2.7-5.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1237.html" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-c++-6.7.2.7-5.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-c++-devel-6.7.2.7-5.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1237.html" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-c++-devel-6.7.2.7-5.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-devel-6.7.2.7-5.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1237.html" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-devel-6.7.2.7-5.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-doc-6.7.2.7-5.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1237.html" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-doc-6.7.2.7-5.slc6 | &nbsp; &nbsp; | &nbsp;
ImageMagick-perl-6.7.2.7-5.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1237.html" target="secadv">RHSA-2016:1237</a> | &nbsp;
ImageMagick-perl-6.7.2.7-5.slc6 | &nbsp; &nbsp; | &nbsp;
setroubleshoot-3.0.47-12.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1267.html" target="secadv">RHSA-2016:1267</a> | &nbsp;
setroubleshoot-doc-3.0.47-12.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1267.html" target="secadv">RHSA-2016:1267</a> | &nbsp;
setroubleshoot-plugins-3.0.40-3.1.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1267.html" target="secadv">RHSA-2016:1267</a> | &nbsp;
setroubleshoot-server-3.0.47-12.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1267.html" target="secadv">RHSA-2016:1267</a> | &nbsp;
tzdata-2016e-1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1266.html" target="secadv">RHBA-2016:1266</a> | &nbsp;
tzdata-java-2016e-1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1266.html" target="secadv">RHBA-2016:1266</a> | &nbsp;
