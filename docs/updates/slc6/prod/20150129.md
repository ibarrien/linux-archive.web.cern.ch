## 2015-01-29

Package | Advisory | Notes
------- | -------- | -----
flash-plugin-11.2.202.438-1.slc6 | &nbsp; &nbsp; | &nbsp;
flash-plugin-11.2.202.440-1.slc6 | &nbsp; &nbsp; | &nbsp;
kmod-lin_tape-2.9.4-1.slc6 | &nbsp; &nbsp; | &nbsp;
lin_taped-2.9.4-1 | &nbsp; &nbsp; | &nbsp;
alsa-utils-1.0.22-9.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0084.html" target="secadv">RHBA-2015:0084</a> | &nbsp;
dhclient-4.1.1-43.P1.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0083.html" target="secadv">RHBA-2015:0083</a> | &nbsp;
dhcp-4.1.1-43.P1.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0083.html" target="secadv">RHBA-2015:0083</a> | &nbsp;
dhcp-common-4.1.1-43.P1.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0083.html" target="secadv">RHBA-2015:0083</a> | &nbsp;
dhcp-devel-4.1.1-43.P1.el6_6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0083.html" target="secadv">RHBA-2015:0083</a> | &nbsp;
gdbm-1.8.0-38.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0089.html" target="secadv">RHBA-2015:0089</a> | &nbsp;
gdbm-devel-1.8.0-38.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0089.html" target="secadv">RHBA-2015:0089</a> | &nbsp;
jasper-1.900.1-16.el6_6.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0074.html" target="secadv">RHSA-2015:0074</a> | &nbsp;
jasper-devel-1.900.1-16.el6_6.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0074.html" target="secadv">RHSA-2015:0074</a> | &nbsp;
jasper-libs-1.900.1-16.el6_6.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0074.html" target="secadv">RHSA-2015:0074</a> | &nbsp;
jasper-utils-1.900.1-16.el6_6.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0074.html" target="secadv">RHSA-2015:0074</a> | &nbsp;
java-1.6.0-openjdk-1.6.0.34-1.13.6.1.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0085.html" target="secadv">RHSA-2015:0085</a> | &nbsp;
java-1.6.0-openjdk-demo-1.6.0.34-1.13.6.1.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0085.html" target="secadv">RHSA-2015:0085</a> | &nbsp;
java-1.6.0-openjdk-devel-1.6.0.34-1.13.6.1.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0085.html" target="secadv">RHSA-2015:0085</a> | &nbsp;
java-1.6.0-openjdk-javadoc-1.6.0.34-1.13.6.1.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0085.html" target="secadv">RHSA-2015:0085</a> | &nbsp;
java-1.6.0-openjdk-src-1.6.0.34-1.13.6.1.el6_6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0085.html" target="secadv">RHSA-2015:0085</a> | &nbsp;
kdebase-workspace-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
kdebase-workspace-akonadi-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
kdebase-workspace-devel-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
kdebase-workspace-libs-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
kdebase-workspace-python-applet-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
kdebase-workspace-wallpapers-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
kdm-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
kernel-2.6.32-504.8.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0087.html" target="secadv">RHSA-2015:0087</a> | &nbsp;
kernel-abi-whitelists-2.6.32-504.8.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0087.html" target="secadv">RHSA-2015:0087</a> | &nbsp;
kernel-debug-2.6.32-504.8.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0087.html" target="secadv">RHSA-2015:0087</a> | &nbsp;
kernel-debug-devel-2.6.32-504.8.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0087.html" target="secadv">RHSA-2015:0087</a> | &nbsp;
kernel-devel-2.6.32-504.8.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0087.html" target="secadv">RHSA-2015:0087</a> | &nbsp;
kernel-doc-2.6.32-504.8.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0087.html" target="secadv">RHSA-2015:0087</a> | &nbsp;
kernel-firmware-2.6.32-504.8.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0087.html" target="secadv">RHSA-2015:0087</a> | &nbsp;
kernel-headers-2.6.32-504.8.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0087.html" target="secadv">RHSA-2015:0087</a> | &nbsp;
kernel-module-openafs-2.6.32-504.8.1.el6-1.6.6-cern3.0.slc6 | &nbsp; &nbsp; | &nbsp;
ksysguardd-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
oxygen-cursor-themes-4.3.4-29.el6_6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2015-0073.html" target="secadv">RHBA-2015:0073</a> | &nbsp;
perf-2.6.32-504.8.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0087.html" target="secadv">RHSA-2015:0087</a> | &nbsp;
python-perf-2.6.32-504.8.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2015-0087.html" target="secadv">RHSA-2015:0087</a> | &nbsp;
