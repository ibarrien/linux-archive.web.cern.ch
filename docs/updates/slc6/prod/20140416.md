## 2014-04-16

Package | Advisory | Notes
------- | -------- | -----
kernel-rt-3.8.13-rt27.40.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-3.8.13-rt27.40.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-debug-devel-3.8.13-rt27.40.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-devel-3.8.13-rt27.40.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-doc-3.8.13-rt27.40.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-firmware-3.8.13-rt27.40.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-3.8.13-rt27.40.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-trace-devel-3.8.13-rt27.40.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-vanilla-3.8.13-rt27.40.el6rt | &nbsp; &nbsp; | &nbsp;
kernel-rt-vanilla-devel-3.8.13-rt27.40.el6rt | &nbsp; &nbsp; | &nbsp;
samba4-4.0.0-61.el6_5.rc4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0383.html" target="secadv">RHSA-2014:0383</a> | &nbsp;
samba4-client-4.0.0-61.el6_5.rc4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0383.html" target="secadv">RHSA-2014:0383</a> | &nbsp;
samba4-common-4.0.0-61.el6_5.rc4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0383.html" target="secadv">RHSA-2014:0383</a> | &nbsp;
samba4-dc-4.0.0-61.el6_5.rc4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0383.html" target="secadv">RHSA-2014:0383</a> | &nbsp;
samba4-dc-libs-4.0.0-61.el6_5.rc4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0383.html" target="secadv">RHSA-2014:0383</a> | &nbsp;
samba4-devel-4.0.0-61.el6_5.rc4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0383.html" target="secadv">RHSA-2014:0383</a> | &nbsp;
samba4-libs-4.0.0-61.el6_5.rc4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0383.html" target="secadv">RHSA-2014:0383</a> | &nbsp;
samba4-pidl-4.0.0-61.el6_5.rc4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0383.html" target="secadv">RHSA-2014:0383</a> | &nbsp;
samba4-python-4.0.0-61.el6_5.rc4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0383.html" target="secadv">RHSA-2014:0383</a> | &nbsp;
samba4-swat-4.0.0-61.el6_5.rc4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0383.html" target="secadv">RHSA-2014:0383</a> | &nbsp;
samba4-test-4.0.0-61.el6_5.rc4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0383.html" target="secadv">RHSA-2014:0383</a> | &nbsp;
samba4-winbind-4.0.0-61.el6_5.rc4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0383.html" target="secadv">RHSA-2014:0383</a> | &nbsp;
samba4-winbind-clients-4.0.0-61.el6_5.rc4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0383.html" target="secadv">RHSA-2014:0383</a> | &nbsp;
samba4-winbind-krb5-locator-4.0.0-61.el6_5.rc4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2014-0383.html" target="secadv">RHSA-2014:0383</a> | &nbsp;
