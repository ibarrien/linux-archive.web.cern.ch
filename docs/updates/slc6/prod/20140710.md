## 2014-07-10

Package | Advisory | Notes
------- | -------- | -----
kmod-redeemer-1.1-1.slc6 | &nbsp; &nbsp; | &nbsp;
debugmode-9.03.38-1.el6_4.4 | &nbsp; &nbsp; | &nbsp;
debugmode-9.03.40-2.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0838.html" target="secadv">RHBA-2014:0838</a> | &nbsp;
initscripts-9.03.38-1.el6_4.4 | &nbsp; &nbsp; | &nbsp;
initscripts-9.03.40-2.el6_5.2 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0838.html" target="secadv">RHBA-2014:0838</a> | &nbsp;
libvisual-0.4.0-10.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0840.html" target="secadv">RHBA-2014:0840</a> | &nbsp;
libvisual-devel-0.4.0-10.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0840.html" target="secadv">RHBA-2014:0840</a> | &nbsp;
nfs-utils-1.2.3-39.el6_5.3 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0828.html" target="secadv">RHBA-2014:0828</a> | &nbsp;
system-config-firewall-1.2.27-7.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0837.html" target="secadv">RHBA-2014:0837</a> | &nbsp;
system-config-firewall-base-1.2.27-7.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0837.html" target="secadv">RHBA-2014:0837</a> | &nbsp;
system-config-firewall-tui-1.2.27-7.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2014-0837.html" target="secadv">RHBA-2014:0837</a> | &nbsp;
