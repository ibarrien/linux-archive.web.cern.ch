## 2016-07-28

Package | Advisory | Notes
------- | -------- | -----
httpd24-curl-7.47.1-1.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1154.html" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-httpd-2.4.18-11.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1420.html" target="secadv">RHSA-2016:1420</a> | &nbsp;
httpd24-httpd-devel-2.4.18-11.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1420.html" target="secadv">RHSA-2016:1420</a> | &nbsp;
httpd24-httpd-manual-2.4.18-11.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1420.html" target="secadv">RHSA-2016:1420</a> | &nbsp;
httpd24-httpd-tools-2.4.18-11.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1420.html" target="secadv">RHSA-2016:1420</a> | &nbsp;
httpd24-libcurl-7.47.1-1.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1154.html" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-libcurl-devel-7.47.1-1.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1154.html" target="secadv">RHBA-2016:1154</a> | &nbsp;
httpd24-mod_ldap-2.4.18-11.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1420.html" target="secadv">RHSA-2016:1420</a> | &nbsp;
httpd24-mod_proxy_html-2.4.18-11.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1420.html" target="secadv">RHSA-2016:1420</a> | &nbsp;
httpd24-mod_session-2.4.18-11.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1420.html" target="secadv">RHSA-2016:1420</a> | &nbsp;
httpd24-mod_ssl-2.4.18-11.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1420.html" target="secadv">RHSA-2016:1420</a> | &nbsp;
sclo-php54-php-pecl-http-2.5.6-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php54-php-pecl-http-devel-2.5.6-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php55-php-pecl-http-2.5.6-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php55-php-pecl-http-devel-2.5.6-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-pecl-http-2.5.6-1.el6 | &nbsp; &nbsp; | &nbsp;
sclo-php56-php-pecl-http-devel-2.5.6-1.el6 | &nbsp; &nbsp; | &nbsp;
java-1.8.0-openjdk-1.8.0.101-3.b13.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1458.html" target="secadv">RHSA-2016:1458</a> | &nbsp;
java-1.8.0-openjdk-debug-1.8.0.101-3.b13.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1458.html" target="secadv">RHSA-2016:1458</a> | &nbsp;
java-1.8.0-openjdk-demo-1.8.0.101-3.b13.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1458.html" target="secadv">RHSA-2016:1458</a> | &nbsp;
java-1.8.0-openjdk-demo-debug-1.8.0.101-3.b13.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1458.html" target="secadv">RHSA-2016:1458</a> | &nbsp;
java-1.8.0-openjdk-devel-1.8.0.101-3.b13.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1458.html" target="secadv">RHSA-2016:1458</a> | &nbsp;
java-1.8.0-openjdk-devel-debug-1.8.0.101-3.b13.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1458.html" target="secadv">RHSA-2016:1458</a> | &nbsp;
java-1.8.0-openjdk-headless-1.8.0.101-3.b13.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1458.html" target="secadv">RHSA-2016:1458</a> | &nbsp;
java-1.8.0-openjdk-headless-debug-1.8.0.101-3.b13.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1458.html" target="secadv">RHSA-2016:1458</a> | &nbsp;
java-1.8.0-openjdk-javadoc-1.8.0.101-3.b13.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1458.html" target="secadv">RHSA-2016:1458</a> | &nbsp;
java-1.8.0-openjdk-javadoc-debug-1.8.0.101-3.b13.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1458.html" target="secadv">RHSA-2016:1458</a> | &nbsp;
java-1.8.0-openjdk-src-1.8.0.101-3.b13.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1458.html" target="secadv">RHSA-2016:1458</a> | &nbsp;
java-1.8.0-openjdk-src-debug-1.8.0.101-3.b13.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1458.html" target="secadv">RHSA-2016:1458</a> | &nbsp;
samba4-4.2.10-7.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1487.html" target="secadv">RHSA-2016:1487</a> | &nbsp;
samba4-client-4.2.10-7.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1487.html" target="secadv">RHSA-2016:1487</a> | &nbsp;
samba4-common-4.2.10-7.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1487.html" target="secadv">RHSA-2016:1487</a> | &nbsp;
samba4-dc-4.2.10-7.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1487.html" target="secadv">RHSA-2016:1487</a> | &nbsp;
samba4-dc-libs-4.2.10-7.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1487.html" target="secadv">RHSA-2016:1487</a> | &nbsp;
samba4-devel-4.2.10-7.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1487.html" target="secadv">RHSA-2016:1487</a> | &nbsp;
samba4-libs-4.2.10-7.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1487.html" target="secadv">RHSA-2016:1487</a> | &nbsp;
samba4-pidl-4.2.10-7.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1487.html" target="secadv">RHSA-2016:1487</a> | &nbsp;
samba4-python-4.2.10-7.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1487.html" target="secadv">RHSA-2016:1487</a> | &nbsp;
samba4-test-4.2.10-7.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1487.html" target="secadv">RHSA-2016:1487</a> | &nbsp;
samba4-winbind-4.2.10-7.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1487.html" target="secadv">RHSA-2016:1487</a> | &nbsp;
samba4-winbind-clients-4.2.10-7.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1487.html" target="secadv">RHSA-2016:1487</a> | &nbsp;
samba4-winbind-krb5-locator-4.2.10-7.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1487.html" target="secadv">RHSA-2016:1487</a> | &nbsp;
