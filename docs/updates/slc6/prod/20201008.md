## 2020-10-08


Package | Advisory | Notes
------- | -------- | -----
bind-9.8.2-0.68.rc1.el6_10.8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4183.html" target="secadv">RHSA-2020:4183</a> | &nbsp;
bind-chroot-9.8.2-0.68.rc1.el6_10.8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4183.html" target="secadv">RHSA-2020:4183</a> | &nbsp;
bind-devel-9.8.2-0.68.rc1.el6_10.8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4183.html" target="secadv">RHSA-2020:4183</a> | &nbsp;
bind-libs-9.8.2-0.68.rc1.el6_10.8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4183.html" target="secadv">RHSA-2020:4183</a> | &nbsp;
bind-sdb-9.8.2-0.68.rc1.el6_10.8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4183.html" target="secadv">RHSA-2020:4183</a> | &nbsp;
bind-utils-9.8.2-0.68.rc1.el6_10.8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4183.html" target="secadv">RHSA-2020:4183</a> | &nbsp;
kernel-2.6.32-754.35.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4182.html" target="secadv">RHSA-2020:4182</a> | &nbsp;
kernel-abi-whitelists-2.6.32-754.35.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4182.html" target="secadv">RHSA-2020:4182</a> | &nbsp;
kernel-debug-2.6.32-754.35.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4182.html" target="secadv">RHSA-2020:4182</a> | &nbsp;
kernel-debug-devel-2.6.32-754.35.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4182.html" target="secadv">RHSA-2020:4182</a> | &nbsp;
kernel-devel-2.6.32-754.35.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4182.html" target="secadv">RHSA-2020:4182</a> | &nbsp;
kernel-doc-2.6.32-754.35.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4182.html" target="secadv">RHSA-2020:4182</a> | &nbsp;
kernel-firmware-2.6.32-754.35.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4182.html" target="secadv">RHSA-2020:4182</a> | &nbsp;
kernel-headers-2.6.32-754.35.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4182.html" target="secadv">RHSA-2020:4182</a> | &nbsp;
kernel-module-openafs-2.6.32-754.35.1.el6-1.6.6-cern5.1.slc6 | &nbsp; &nbsp; | &nbsp;
perf-2.6.32-754.35.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4182.html" target="secadv">RHSA-2020:4182</a> | &nbsp;
python-perf-2.6.32-754.35.1.el6 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4182.html" target="secadv">RHSA-2020:4182</a> | &nbsp;
thunderbird-78.3.1-1.el6_10 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2020-4158.html" target="secadv">RHSA-2020:4158</a> | &nbsp;

