## 2019-11-28

Package | Advisory | Notes
------- | -------- | -----
rt-setup-1.60-49.el6rt | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2019-3924.html" target="secadv">RHEA-2019:3924</a> | &nbsp;
cern-get-sso-cookie-0.6-2.slc6 | &nbsp; &nbsp; | &nbsp;
perl-Authen-Krb5-1.9-3.slc6 | &nbsp; &nbsp; | &nbsp;
perl-WWW-CERNSSO-Auth-0.6-2.slc6 | &nbsp; &nbsp; | &nbsp;
