## 2016-11-24

Package | Advisory | Notes
------- | -------- | -----
dracut-modules-growroot-0.23-4.el6 | <div class="adv_e">[E]</div> <a href="http://rhn.redhat.com/errata/RHEA-2016-2701.html" target="secadv">RHEA-2016:2701</a> | &nbsp;
lcm-profile-1-20161121.slc6 | &nbsp; &nbsp; | &nbsp;
memcached-1.4.4-3.el6_8.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2820.html" target="secadv">RHSA-2016:2820</a> | &nbsp;
memcached-devel-1.4.4-3.el6_8.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-2820.html" target="secadv">RHSA-2016:2820</a> | &nbsp;
