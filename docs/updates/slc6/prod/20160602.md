## 2016-06-02

Package | Advisory | Notes
------- | -------- | -----
hpijs-3.14.6-4.el6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1186.html" target="secadv">RHBA-2016:1186</a> | &nbsp;
hplip-3.14.6-4.el6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1186.html" target="secadv">RHBA-2016:1186</a> | &nbsp;
hplip-common-3.14.6-4.el6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1186.html" target="secadv">RHBA-2016:1186</a> | &nbsp;
hplip-gui-3.14.6-4.el6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1186.html" target="secadv">RHBA-2016:1186</a> | &nbsp;
hplip-libs-3.14.6-4.el6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1186.html" target="secadv">RHBA-2016:1186</a> | &nbsp;
kernel-2.6.32-642.1.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1185.html" target="secadv">RHBA-2016:1185</a> | &nbsp;
kernel-abi-whitelists-2.6.32-642.1.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1185.html" target="secadv">RHBA-2016:1185</a> | &nbsp;
kernel-debug-2.6.32-642.1.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1185.html" target="secadv">RHBA-2016:1185</a> | &nbsp;
kernel-debug-devel-2.6.32-642.1.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1185.html" target="secadv">RHBA-2016:1185</a> | &nbsp;
kernel-devel-2.6.32-642.1.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1185.html" target="secadv">RHBA-2016:1185</a> | &nbsp;
kernel-doc-2.6.32-642.1.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1185.html" target="secadv">RHBA-2016:1185</a> | &nbsp;
kernel-firmware-2.6.32-642.1.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1185.html" target="secadv">RHBA-2016:1185</a> | &nbsp;
kernel-headers-2.6.32-642.1.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1185.html" target="secadv">RHBA-2016:1185</a> | &nbsp;
kernel-module-openafs-2.6.32-642.1.1.el6-1.6.6-cern4.1.slc6 | &nbsp; &nbsp; | &nbsp;
libsane-hpaio-3.14.6-4.el6.1 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1186.html" target="secadv">RHBA-2016:1186</a> | &nbsp;
ntp-4.2.6p5-10.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1141.html" target="secadv">RHSA-2016:1141</a> | &nbsp;
ntpdate-4.2.6p5-10.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1141.html" target="secadv">RHSA-2016:1141</a> | &nbsp;
ntp-doc-4.2.6p5-10.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1141.html" target="secadv">RHSA-2016:1141</a> | &nbsp;
ntp-perl-4.2.6p5-10.el6.1 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1141.html" target="secadv">RHSA-2016:1141</a> | &nbsp;
perf-2.6.32-642.1.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1185.html" target="secadv">RHBA-2016:1185</a> | &nbsp;
python-perf-2.6.32-642.1.1.el6 | <div class="adv_b">[B]</div> <a href="http://rhn.redhat.com/errata/RHBA-2016-1185.html" target="secadv">RHBA-2016:1185</a> | &nbsp;
squid-3.1.23-16.el6_8.4 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1138.html" target="secadv">RHSA-2016:1138</a> | &nbsp;
squid34-3.4.14-9.el6_8.3 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2016-1140.html" target="secadv">RHSA-2016:1140</a> | &nbsp;
