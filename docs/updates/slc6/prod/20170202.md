## 2017-02-02

Package | Advisory | Notes
------- | -------- | -----
firefox-45.7.0-1.el6_8 | <div class="adv_s">[S]</div> <a href="http://rhn.redhat.com/errata/RHSA-2017-0190.html" target="secadv">RHSA-2017:0190</a> | &nbsp;
splunk-6.5.2-67571ef4b87d | &nbsp; &nbsp; | &nbsp;
splunkforwarder-6.5.2-67571ef4b87d | &nbsp; &nbsp; | &nbsp;
