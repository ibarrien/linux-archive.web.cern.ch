# System updates

## CentOS 8 (C8)

* [Updates/Errata](/updates/c8/prod/latest_updates)
* [TEST Updates/Errata](/updates/c8/test/latest_updates)
* [Software repositories](/updates/c8/)

## CERN CentOS 7 (CC7)

* [Updates/Errata](/updates/cc7/prod/latest_updates)
* [TEST Updates/Errata](/updates/cc7/test/latest_updates)
* [Software repositories](/updates/cc7/)

## Scientific Linux CERN 6 (SLC6)

* [Updates/Errata](/updates/slc6/prod/latest_updates)
* [TEST Updates/Errata](/updates/slc6/test/latest_updates)
* [Software repositories](/updates/slc6/)
