# Archived Linux @ CERN

This secondary page serves the purpose of archiving <https://linux.cern.ch> content that is no longer relevant.

## News


### SLC6 End of support

Following [OTG0054345: End of support for SLC6 (Scientific Linux CERN 6)](https://cern.service-now.com/service-portal?id=outage&n=OTG0054345) we have moved SLC6 updates to our archived web.


From the 30th November 2020, SLC6 will enter End of Life and support will cease. Also from this date, software updates will no longer be made available.

Current users of SLC6 are strongly encouraged to migrate to CC7 or C8 before 30.11.2020.

Please refer to the "Linux @ CERN" website for more details on CC7 (http://linux.web.cern.ch/linux/centos7) and C8 (http://linux.web.cern.ch/linux/centos8/)


Also related to this:

* [OTG0059140: Closure of SLC6 lxbatch resources](https://cern.service-now.com/service-portal?id=outage&n=OTG0059140)

* [OTG0059455: SLC6 End of Life - hosts with openings in CERN's external perimeter firewall](https://cern.service-now.com/service-portal?id=outage&n=OTG0059455)