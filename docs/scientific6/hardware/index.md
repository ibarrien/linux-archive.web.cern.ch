<!--#include virtual="/linux/layout/header" -->
# SLC6: Standard hardware
<h2>Installation and Configuration for CERN standard hardware PC models and peripherials.</h2>

This page describes SLC6 installation and configuration procedures
for standard CERN PC hardware models sold by CERN stores.
While many more types of PC hardware are known to operate correctly
while running Scientific Linux CERN 6, only models listed below have been tested
by the Linux Support team.<br>

<br>
Please note: Lists below describe only PC hardware available for
purchase from CERN stores starting 2009.
Previous models are also supported unless stated
otherwise on this pages.

<ul>
<li><a href="#desktops">Desktops</a> (<em>Please note: as of April 2016 we DO NOT TEST desktops on SLC6 anymore</em>)
<li><a href="#laptops">Laptops</a> (<em>Please note: as of 2011 we DO NOT TEST laptops anymore</em>)
<li><a href="#other">Other</a>
<li><a href="#virtualmachines">Virtual Machines</a>
</ul>

<hr>

<h2><a name="desktops"></a>Desktops</h2>

<ul>
<li><a href="#hp800g2">HP 800G2</li>
<li><a href="#TTLTeknoPro">TTL TeknoPro</li>
<li><a href="#dell7010">Dell Optiplex 7010</li>
<li><a href="#hp8300">HP 8300 Elite</li>
<li><a href="#hpdc8200">HP Compaq DC8200 Elite</li>
<li><a href="#dell790">Dell Optiplex 790</li>
<!--
<li><a href="#dell780">Dell Optiplex 780</li>
<li><a href="#hpdc8000">HP Compaq DC8000</li>
<li><a href="#hpdc7900_2010">HP Compaq DC7900 (2010 model)</li>
-->
<li><a href="#hpdc7900">HP Compaq DC7900</li>
<!--
<li><a href="#hpdc5850">HP Compaq DC5850</li>
-->
<li><a href="#hpdc5750">HP Compaq DC5750</li>
</ul>
<hr>
<h2><a name="hp800g2"></a>HP 800G2</h2>
<img src="images/small_hp800g2.jpg">

<ul>
    <li><em>Note:</em> Built-in graphics card support is limited to 1024x768 resolution in unaccelerated mode with single display only.
    <br />
    Since April 2016 as a workaround, we recommend for better performance, stability and multi-screen support the purchase of an additional Nvidia card.
    <br />
    Please read carefully the known limitations and specifications of the proposed card <a href="#hpnvidiagt730">HP Nvidia GT 730</a>.
    <li><em>Note:</em> Requires Scientific Linux CERN <b>6.7</b> or newer.
    <li>Please followup <a href="/linux/scientific6/docs/install">standard installation procedure</a>.
    <li> If you use the addtional Nvidia card you must disable Integrated Video in HP BIOS: Enter Bios, go to the advanced tab and unclick "Integrated Video".
    <br  />
    <img src="images/bioshp800g2.png" alt="Bios Screenshot" \>
</ul>

<hr>
<h2><a name="dell7010"></a>Dell Optiplex 7010</h2>
<a href="images/dell7010.jpg" target="snapshot"><img src="images/small_dell7010.jpg"></a>

<ul>
<li>Please followup <a href="/linux/scientific6/docs/install">standard installation procedure</a>.
</ul>

<hr>
<h2><a name="TTLTeknoPro"></a>TTL TeknoPro</h2>
<img src="images/small_ttl.png">

<ul>
<li>Please followup <a href="/linux/scientific6/docs/install">standard installation procedure</a>.
</ul>


<hr>
<h2><a name="hp8300"></a>HP 8300 Elite</h2>
<a href="images/hpdc8200.jpg" target="snapshot"><img src="images/small_hpdc8200.jpg"></a>

<ul>
<li>Please followup <a href="/linux/scientific6/docs/install">standard installation procedure</a>.
</ul>

<hr>
<h2><a name="hpdc8200"></a>HP Compaq DC 8200 Elite</h2>
<a href="images/hpdc8200.jpg" target="snapshot"><img src="images/small_hpdc8200.jpg"></a>

<ul>
<li><strike>[SLC6] Please note there is a special step for Hardware including Nvidia NVS 300 Graphic Card. Please follow <a href="/linux/scientific6/docs/bootparameter">this</a> procedure.</strike> Starting with SLC 6.3 this procedure is not needed anymore.
<li>Please followup <a href="/linux/scientific6/docs/install">standard installation procedure</a>.
</ul>

<hr>
<h2><a name="dell790"></a>Dell Optiplex 790</h2>
<a href="images/delloptiplex790.jpg" target="snapshot"><img src="images/small_delloptiplex790.jpg"></a>

<ul>
<li>Please followup <a href="/linux/scientific6/docs/install">standard installation procedure</a>.
</ul>

<hr>
<h2><a name="hpdc7900"></a>HP Compaq DC 7900</h2>
<a href="images/hpdc7900.jpg" target="snapshot"><img src="images/small_hpdc7900.jpg"></a>

<ul>
<li>This model has not been formally tested under SLC6, but it is known to work.
<li>Please followup <a href="/linux/scientific6/docs/install">standard installation procedure</a>.
<!--
 <li><em>Note:</em> On early models, only <b>VGA</b> video output is supported, the <b>DisplayPort</b> connector
     is not functional on SLC5 as of March 2009 (even with <b>DisplayPort</b> to <b>DVI</b> adapter):
     Therefore <em>dual-screen setups cannot be used</em>.<br>
     Update: As of December 2009 the DisplayPort adapter works on <b>some</b> configurations providing
     fully updated SLC 5.4 version is used.
-->
</ul>

<hr>

<h2><a name="hpdc5750"></a>HP Compaq DC 5750</h2>
<a href="images/hpdc5850.jpg" target="snapshot"><img src="images/hpdc5850.jpg"></a>

<ul>
<li>This model has not been formally tested under SLC6, but it is known to work.
<li>Installation procedure:
 <ul>
  <li>Change disk access mode to <b>Native Mode IDE</b> in the system BIOS.
  <li><font color="red">Please note:</font> Do not use <b>RAID</b> mode in the BIOS: this will prevent dual-boot setups from working.
 </ul>
 <li>Please followup <a href="/linux/scientific6/docs/install">standard installation procedure</a>.
</ul>

<hr>


<h2><a name="laptops"></a>Laptops</h2>

<ul>
 <li><a href="#hpelitebook6930p">HP EliteBook 6930p</a></li>
</ul>
</p>

<hr>

<a name="hpelitebook6930p"></a><h2>HP EliteBook 6930p</h2>
<a href="images/hpelitebook6930p.jpg" target="snapshot"><img src="images/small_hpelitebook6930p.jpg"></a>

<ul>
<li>This model has not been formally tested under SLC6, but it is known to work.
<li>Please followup <a href="/linux/scientific6/docs/install">standard installation procedure</a>.
</ul>

<hr>
<h2><a name="virtualmachines"></a>Virtual Machines</h2>

<ul>
 <li><a href="#cvihyperv">CVI VMs</a></li>
</ul>
</p>

<hr>

<a name="cvihyperv"></a><h2>CVI VMs</h2>

CVI service is no longer available at CERN.

<!--

<ul>
<li> SLC6 is supported on Virtual Machines in the <a href="http://cern.ch/cvi">CVI service</a>, and templates exist.
<li> For manual installation, please follow the <a href="/linux/scientific6/docs/install">standard installation procedure</a>.
<li> Known issues and workarounds:
     <ul>
     <li> use ntpdate to make sure the system has the correct clock at boot time<br>
          <tt># /sbin/chkconfig --level 345 ntpdate on</tt>
     <li> network stops functioning on VMs with more than 1 vCPU, with irqbalance daemon started<br>
          <tt># /sbin/chkconfig irqbalance off</tt><br>
          Tracked at <a href="https://bugzilla.redhat.com/show_bug.cgi?id=655855">RedHat Bugzilla</a> and directly with Microsoft
     <li> Don't run NetworkManager, as it will force a short hostname<br>
          <tt># /sbin/chkconfig --del NetworkManager</tt>
     <li> We recommend you to change the Network Interface Card to a <em>synthetic</em> card. <br>
          To do so, click <a href="https://vmm.cern.ch/vmmgtsvc/service.asmx?op=VMSetNetworkAdapterType">here</a>,
          and specify the name of your VM, and set <em>Synthetic: true</em> before you click "Invoke". <br>
          Note that this involves a restart of your VM
     <li> To avoid drive timeouts for certain operations, we recommend to increase the timeout value by installing a udev rule:<br>
          <tt># yum install hyperv-scsi-udev</tt><br>
          Followed by a reboot.
     <li> Prior to the SLC 6.4 release, we recommended to install the Integration
          Components RPM to avoid time keeping issues, and to improve performance and functionality. The so-called hv drivers have
          been integrated in the SLC 6.4 kernel, and there is no need to install the addon RPM any longer.
     <li> Note: The SLC6 templates available in the CVI service contain the workarounds for the issues listed above
     </ul>
</ul>
-->
<h2><a name="#other">Other</h2>

    <hr>
    <h2><a name="hpnvidiagt730"></a>HP NVIDIA GeForce GT 730 2GB PCIe </h2>
    <img src="images/nvidia-gt730.png">
    <ul>
        <li> Note: Test have been done with 2 monitors ; 1 connected through DVI and the other one through Display port.
        <li> <em color="red">Note</em>: The highest supported resolution over DVI is 1920x1080 (As of April 2016, higher resolution showed graphic artifacts and unreadable fonts).
        <br />
        <li> <em color>Note</em>: Disable integrated graphic in your BIOS before starting the installation.
        <li> <em color>Note</em>: At first boot X will not start, switch to a terminal and log as root and enable the nvidia driver.
        <li> Install nvidia drivers from ElRepo repository:
        <pre>
        &#35; yum --enablerepo=elrepo install nvidia-x11-drv
        &#35; reboot
        </pre>
</ul>

<hr>



