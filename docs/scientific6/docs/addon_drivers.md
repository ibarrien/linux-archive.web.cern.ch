<!--#include virtual="/linux/layout/header" -->
# SLC6 - addon drivers
<h2>Additional / Replacement drivers for SLC6</h2>

For some hardware in use in CERN Computer Center and/or individual desktops updated (replacement) add-on drivers may be required.
On SLC6 we do not provide these drivers ourselves anymore, instead we rely on drivers provided by <a href="http://elrepo.org">El Repo</a> repository.
<p>
To install such drivers please proceed following way:
<p>
As root on you system execute:<br>
<pre>&#35; yum --enablerepo=elrepo install kmod-SOMEDRIVER</pre>
<p>
To see all available drivers execute:
<pre>&#35; yum --enablerepo=elrepo search kmod</pre>
<p>
To see more information about particular driver found with the above command, execute:<br>
<pre>&#35; yum --enablerepo=elrepo info kmod-SOMEDRIVER</pre>
<p>
All El Repo drivers are build according to Red Hat <a href="http://driverupdateprogram.com/">Driver Update Program</a>
which introduces a way of packaging additional kernel drivers in a manner which eases the system upgrade process and reduces
maintenance costs.
<br>
Traditionally .. on SLC3/4/5 add-on kernel modules were packaged as <i>kernel-module-NAME-KERNELVERSION</i>
and with every kernel upgrade an updated kernel-modules package release was needed.
<br>
 This is no longer necessary for add-on modules packaged according to the guidelines of Red Hat Driver Update
 Program: modules packaged as <i>kmod-NAME</i> should maintain compatibility accross the whole lifecycle
 of SLC6 support.

