<!--#include virtual="/linux/layout/header" -->
# Scientific Linux CERN 6 Documentation

<ul>
    <li><a href="install">Installation instructions</a>
        <ul>
            <li><a href="bootmedia">Boot Media preparation</a>
            <li><a href="kickstart">Simple Kickstart file example</a>
            <li><a href="../hardware/">Hardware-specific instructions</a>
        </ul>
    <li><a href="quickupdate">Quick Update instructions</a>
    <li><a href="softwaremgmt">Software Management documentation</a>
    <li><a href="addon_drivers">Add on / replacement drivers for SLC6</a>
<!--
   <ul>
    <li><a href="yumex">Yumex Software Management GUI</a>
   </ul>
-->
    <li><a href="configmgmt">Configuration Management documentation</a>
    <li><a href="/docs/account-mgmt">Advanced user account management using LDAP</a>
    <li><a href="/docs/printing">Printing at CERN</a>
    <li><a href="subversion17">Using subversion 1.7.X on SLC6</a> <em>NEW!</em>
    <li><a href="softwarecollections">Software Collections - addtional software for SLC6</a>
<!--  <li><a href="https://twiki.cern.ch/twiki/bin/view/LinuxSupport/WebHome">Linux Support TWiki</a> and <a href="https://twiki.cern.ch/twiki/bin/view/LinuxSupport/LinuxSupportSLCFAQ">F.A.Q</a>  -->
<!-- <li><a href="dhcp-client">Configuring SLC3 as a DHCP client</a>
 <li><a href="printing">Command Line and Desktop Printing at CERN</a>

 <li><a href="gnome-slow">Configuring GNOME for slow machines</a>

 <li><a href="email">Email client configuration at CERN</a>
 <li><a href="calendaring">Calendaring applications on SLC3</a>
 <li><a href="sound">Sound card configuration</a>
 <li><a href="wireless">Wireless Network (Wi-Fi) cards</a>
 <li><a href="multimedia">Available multimedia applications</a>
-->
    <li>Interoperability with CERN Microsoft Windows environment (NICE)
        <ul>
<!--
            <li><a href="vpn">Using CERN Microsoft VPN</a>
-->
            <li><a href="/docs/msexchange">Thunderbird integration with MS Exchange 2007/2010 Calendaring and Tasks</a>
            <li><a href="/docs/ssokrb5">Using Kerberos (passwordless) authentication for CERN Single Sign On (SSO)</a>
	    <li><a href="/docs/mailkrb5">Using Kerberos (passwordless) authentication for CERN e-mail services</a>

	    <li><a href="/docs/lyncmsg">Pidgin Instant Messaging integration with MS Lync / OCS messaging service</a>
	    <li><a href="/docs/cviconsole">Direct access to CERN CVI (Hyper-V) guest system console</a>
            <li><a href="/docs/cernssocookie">CERN Single Sign on authentication using scripts/programs</a>
            <li><a href="/docs/smartcards">Using CERN Smart Cards</a> <em>PILOT</em>


            <li><a href="dfsaccess">Accessing Microsoft DFS at CERN (<i>//dfs/cern.ch</i>)</a>
		<ul>
		<li><a href="mountdfs">Mounting DFS file system on Linux</a>.
		</ul>
<!--
            <li><a href="evolution">Setting up Evolution as Microsoft Exchange Mail/Calendar/Public Folders client</a>
-->
            <li><a href="wts">Connecting to Microsoft Windows Terminal Services at CERN</a>
            <li><a href="shibboleth">Apache integration with CERN Single Sign On (SSO) using shibboleth</a>
            <li><a href="mod_auth_mellon">Apache integration with CERN Single Sign On (SSO) using mod_auth_mellon</a> <em>New!</em>
            <li><a href="/docs/certificate-autoenroll">CERN Host Certificate AutoEnrollement and AutoRenewal</a> <em>New!</em>
       </ul>
<!--
 <li><a href="netboot">Netbooted Scientific Linux CERN 3</a>
 <li><a href="faq/">FAQ (Frequently Asked Questions - and Answers)</a>
-->
</ul>

<h2>Red Hat Enterprise Linux 6 Documentation (local copy)</h2>
<p>
Most of <a href="http://docs.redhat.com/docs/en-US/Red_Hat_Enterprise_Linux/index.html">RHEL 6 documentation</a> applies to SLC6 systems notable exception being: installation
instructions, Red Hat Network and up2date agent setup instructions. For the former
please refer to SLC6 specific documentation hereabove.
</p>


<ul>
 <li><a href="rhel/Installation_Guide/">Installation Guide</a>
 <li><a href="rhel/Deployment_Guide/">Deployment Guide</a>
 <li><a href="rhel/6.0_Release_Notes/">6.0 Release Notes</a>
 <li><a href="rhel/Technical_Notes/">6.0 Technical Notes</a>
 <li><a href="rhel/6.1_Release_Notes/">6.1 Release Notes</a>
 <li><a href="rhel/6.1_Technical_Notes/">6.1 Technical Notes</a>
 <li><a href="rhel/6.2_Release_Notes/">6.2 Release Notes</a>
 <li><a href="rhel/6.2_Technical_Notes/">6.2 Technical Notes</a>
 <li><a href="rhel/Cluster_Administration/">Cluster Administration</a>
 <li><a href="rhel/Developer_Guide/">Developer Guide</a>
 <li><a href="rhel/DM_Multipath/">DM Multipath</a>
 <li><a href="rhel/Global_File_System_2/">Global File System 2</a>
 <li><a href="rhel/High_Availability_Add-On_Overview/">High Availability Add-On Overview</a>
 <li><a href="rhel/Hypervisor_Deployment_Guide/">Hypervisor Deployment Guide</a>
 <li><a href="rhel/Identity_Management_Guide/">Identity Management Guide</a>
 <li><a href="rhel/Load_Balancer_Administration/">Load Balancer Administration</a>
 <li><a href="rhel/Logical_Volume_Manager_Administration/">Logical Volume Manager Administration</a>
 <li><a href="rhel/Managing_Confined_Services/">Managing Confined Services</a>
 <li><a href="rhel/Managing_Smart_Cards/">Managing Smart Cards</a>
 <li><a href="rhel/Migration_Planning_Guide/">Migration Planning Guide</a>
 <li><a href="rhel/Performance_Tuning_Guide/">Performance Tuning Guide/</a>
 <li><a href="rhel/Power_Management_Guide/">Power Management Guide</a>
 <li><a href="rhel/Resource_Management_Guide/">Resource Management Guide</a>
 <li><a href="rhel/Security-Enhanced_Linux/">Security-Enhanced Linux</a>
 <li><a href="rhel/Security_Guide/">Security Guide</a>
 <li><a href="rhel/Storage_Administration_Guide/">Storage Administration Guide</a>
 <li><a href="rhel/SystemTap_Beginners_Guide/">SystemTap Beginners Guide</a>
 <li><a href="rhel/SystemTap_Tapset_Reference/">SystemTap Tapset Reference</a>
 <li><a href="rhel/V2V_Guide/">V2V Guide</a>
 <li><a href="rhel/Virtualization_Administration_Guide/">Virtualization Administration Guide</a>
 <li><a href="rhel/Virtualization_Getting_Started_Guide/">Virtualization Getting Started_Guide</a>
 <li><a href="rhel/Virtualization_Host_Configuration_and_Guest_Installation_Guide/">Virtualization Host Configuration and Guest Installation Guide</a>
 <li><a href="http://www.redhat.com/docs/" target="errata">Errata to above guides (on Red Hat site)</a>
</ul>



