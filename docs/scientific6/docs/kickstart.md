<!--#include virtual="/linux/layout/header" -->
# SLC6: Kickstart

<h3>Kickstart files</h3>

Kickstart is a mechanism allowing automated and unattended system installations. At CERN kickstart installations
can be performed by AIMS service (including PXE boot).
For AIMS documentation please refer to: <a href="https://twiki.cern.ch/twiki/bin/view/LinuxSupport/Aims2">AIMS2 documentation</a>.
For full documentation on all available Kickstart options, please consult
the Red Hat <a href="/scientific6/docs/rhel/Installation_Guide/">Installation Guide</a>.

<p>

An example Kickstart file, describing the installation of a "Software Development Workstation (CERN Recommended Setup)" desktop machine,
can be downloaded <a href="../kickstart-example.ks">here</a>. Please note that this example file (most likely) needs to be tailored for your hardware, and for your software needs.

<p>

<img alt="Kickstart generator GUI" src="../miscshots/system-config-kickstart-small.png" style="float: right;" align="right" hspace="10" vspace="10">
Alternatively, you can also use the system utility <tt>/usr/sbin/system-config-kickstart</tt> to generate a template file,
and then edit the generated file to include the "CERN recommended" package groups listed in the <a href="../kickstart-example.ks">example</a> mentioned above.

<p>

Please be aware that Kickstart is not very user-friendly, typos or syntax errors may result in impressive-looking python tracebacks (where the most valuable information is at the <b>bottom</b>...).


