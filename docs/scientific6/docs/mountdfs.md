<!--#include virtual="/linux/layout/header" -->
# Mounting  CERN DFS file system on linux
<h2>Mounting CERN DFS file system on linux</h2>
CERN uses <a href="https://dfsweb.web.cern.ch/dfsweb/">Microsoft DFS</a> file system for storing users and application data for Windows systems.
<p>
This documentation outlines the setup process allowing Linux clients to mount and access CERN DFS file system.
<p>
While the initial installation of required software is specific to CERN SLC6
Linux distribution, following configuration steps shall be applicable on any modern Linux platform,
providing it features <b>cifs-utils</b> version <b>4.8.1</b> or newer and <b>kernel</b> version <b>2.6.38</b> or newer,
or a patched cifs kernel module on older kernels (see <a href="http://thread.gmane.org/gmane.linux.kernel.cifs/4498/focus=4520">the patch discussion here</a>).


<h2>Software installation</h2>

As root on your <b>fully updated</b> (if in doubt, execute as root: <b>yum update</b>...) SLC6 system run:
<pre>
&#35; yum  install cifs-utils <!-- autofs --> <strike>kernel-module-cifs-`uname -r`</strike>
</pre>
(As of SLC 6.3 - kernel <b>2.6.32-279.el6</b> an additional cifs kernel module is not needed, the in-kernel one works correctly)
<h2>Configuration</h2>
As root on your system:
<p>
Please verify that your host keytab is valid:
<pre>
&#35; klist -k
</pre>
it should show output similar to:
<pre>
Keytab name: WRFILE:/etc/krb5.keytab
KVNO Principal
---- --------------------------------------------------------------------------
  10 host/<tt>yourhost</tt>.cern.ch@CERN.CH
   9 host/<tt>yourhost</tt>.cern.ch@CERN.CH
   1 host/<tt>yourhost</tt>.cern.ch@CERN.CH
</pre>
(actual output may vary depending on when and how keytab was set)
<p>
Edit <b>/etc/request-key.conf</b> and add following lines at the end:
<pre>
&#35; added for CIFS
create  cifs.spnego    * * /usr/sbin/cifs.upcall %k
create  dns_resolver   * * /usr/sbin/cifs.upcall %k
</pre>
Create mountpoint:
<pre>
&#35; mkdir /dfs
</pre>
Create <b>/etc/cron.d/host-kinit</b> file with following content:
<pre>
&#35; This cron job will reacquire host credentials every 12 hours
01 */12 * * * root /usr/bin/kinit -k
</pre>

<h2>Filesystem mount</h2>
Please choose one of the following two methods on your system.

<h4>Mounting with other filesystems</h4>
Edit <b>/etc/rc.local</b> and insert there these lines:
<pre>
&#35; Mount DFS
/usr/bin/kinit -k
/bin/mount /dfs
</pre>
Edit <b>/etc/fstab</b> and add at the end this line:
<pre>
//cerndfs.cern.ch/dfs   /dfs            cifs    noauto,nocase,sec=krb5,multiuser,uid=0,gid=0    0 0
</pre>
Next, execute:
<pre>
&#35; /etc/rc.local
</pre>
On subsequent system reboots DFS will be mounted automatically.

<h4>Mounting with automounter</h4>

Edit <b>/etc/auto.master</b> and add following line:
<pre>
/dfs/ /etc/auto.dfs
</pre>
Create <b>/etc/auto.dfs</b> with following content:
<pre>
&#35;!/bin/sh
[ !`/usr/bin/kinit -k 2>&1 >> /dev/null` ] && echo " -fstype=cifs,sec=krb5,multiuser,user=0,uid=0,gid=0 ://cerndfs.cern.ch/dfs/&"
</pre>
Execute:
<pre>
&#35; chmod 755 /etc/auto.dfs
</pre>
<p>
To finish the configuration please enable and restart the automounter:
<pre>
&#35; /sbin/chkconfig --levels 345 autofs on
&#35; /sbin/service autofs restart
</pre>
<em>Note:</em> The DFS filesystem is automounted: therefore until user accesses it nothing is
visible under <b>/dfs/</b>: try <b>ls /dfs/Users</b> or <b>ls /dfs/Applications</b> to see the content.

<h2>Usage notes</h2>
<ul>
<li>This method of accessing DFS requires a valid Kerberos host key - which can be allocated ONLY to systems on CERN network.
<li>User access to files requires a valid Kerberos ticket from CERN KDC, please check yours using: <b>klist</b>.
<li> <strike>On some DFS mounts sometimes part of DFS hierarchy is not accessible showing: <b>Object is remote</b> error, to make the problem go away please either use <b>ls /dfs/PROBLEMATIC/PATH/</b> (note trailing slash!) or <b>cd /dfs/PROBLEMATIC/PATH/</b>.</strike>(this problem seems to be solved in current SLC6 kernels).
<li> Case sensitivity: DFS mount on Linux emulates Windows behaviour Files/Folders are case-sensitive upon creation, but case-insensitive for later access.
<li> User ownership and permissions on files/directories are shown as full root user permissions and root ownership:
<pre>
ls -l /dfs/
total 140
drwxr-xr-x 1 root root   32768 Feb  9 14:41 Applications
[...]
</pre>
but actual access permisions are mapped correctly, if you create files these will be created with default Windows permissions in given folder.
<li>Ownership/access mode changing using chmod/chown will not work on DFS files.
<li>Getting/setting ACLs for DFS is not supported with current kernels (do not try <it>cifsacl</it> mount option it <b>is</b> buggy ...)
<!-- <li>Directory 'size' will show '0' until content of the directory is accessed. -->
<li>...
</ul>


