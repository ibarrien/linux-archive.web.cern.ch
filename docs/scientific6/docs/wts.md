<!--#include virtual="/linux/layout/header4" -->
# SLC6: Windows Terminal Services access

<h2>Using CERN Windows Terminal Services on Scientific Linux CERN</h2>

<h3>Prerequisites</h3>
<ul>
<li>Read <a href="https://espace.cern.ch/winservices-help/Terminal%20Services/Pages/default.aspx/">CERN Windows Terminal Services documentation</a>.
</li>
<li>Ensure that the rdesktop and (optionally) tsclient packages are installed. <br>
If this is not the case, you can install them manually (as <tt>root</tt>):
<pre>% /usr/bin/yum install tsclient rdesktop</pre>
</ul>

<h3>Command line tool: Remote Desktop (rdesktop)</h3>

Example: to connect to CERN WTS, run
<pre>
rdesktop -u CERN-account -d CERN cernts.cern.ch
</pre>
This command will open a connection to the cernts.cern.ch Terminal Service, using login name <tt>CERN-account</tt>, and the <tt>CERN</tt> Windows domain.
For more options see <tt>man rdesktop</tt>.
<br>

<h3>Graphical user interface: Terminal Server Client (tsclient)</h3>

<table border="0">

<tr>
    <td><a href="../miscshots/wts1.png"><img src="../miscshots/wts1-mini.png"></a></td>
    <td>
        <font size="-1">
        Start Terminal Server Client by selecting it from G/K Menu -> Internet -> Terminal Server Client or typing
        <b>tsclient</b> on command line. When interface starts, click on "Add Connection", and select "Windows Terminal Service".
       </font>
    </td>
</tr>
<tr>
    <td><a href="../miscshots/wts2.png"><img src="../miscshots/wts2-mini.png"></a></td>
    <td>
        <font size="-1">
        In the "Edit Connection" window, enter the following information:<br>
        <i>Name</i>: <i><b>cernts</b></i><br>
        <i>Host</i>: <i><b>cernts.cern.ch</b></i><br>
        <i>Username</i>: <i><b>CERN account</b></i><br>
        <i>Password</i>: <i><b>CERN password</b></i><br>
        <i>Domain</i>: <i><b>CERN</b></i><br>
        and select the window size you want.
        </font>
    </td>
</tr>
<tr>
    <td><a href="../miscshots/wts3.png"><img src="../miscshots/wts3-mini.png"></a></td>
    <td>
        <font size="-1">
        Double clicking on the new 'cernts' icon will bring you to Windows Terminal Server login screen.
        After Windows login sequence completes you will be able to use installed (subset of) MS Windows applications.
        </font>
    </td>
</tr>

</table>

<h3>Support</h3>

In case of problems please report to <a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a>

<br>

<div align="right"><a href="mailto:Linux.Support@cern.ch">Linux.Support@cern.ch</a></div>

