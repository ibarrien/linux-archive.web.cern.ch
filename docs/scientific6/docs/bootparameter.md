<!--#include virtual="/linux/layout/header" -->
# SLC6: Boot with special Kernel Parameter
<h2>SLC6: Boot with special Kernel Parameter</h2>
This doc show you how to boot the linux installation with a special kernel parameters "nomodeset".

Please read the <a href="../install">standard installation procedure</a> introduction first (Before you start).

<table>
<tr>
<td>
<a href="../miscshots/bootparameter1.png" target="snapshot"><img src="../miscshots/bootparameter1.png" width="300"></a>
</td>
<td> Select <b>Expert Operating System Install Menu</b> from the menu.
</td>
</tr>
<tr>
<td>
<a href="../miscshots/bootparameter2.png" target="snapshot"><img src="../miscshots/bootparameter2.png" width="300"></a>
</td>
<td>
Select <b>Scientific Linux CERN (SLC) Install Menu</b> from the menu.<br>
</td>
</tr>
<tr>
<td>
<a href="../miscshots/bootparameter3.png" target="snapshot"><img src="../miscshots/bootparameter3.png" width="300"></a>
</td>
<td>
Select the <b>Install Scientific Linux CERN 6.2 (SLC6)</b> 32bits or 64bit entry, and press <b>TAB</b>
</td>
</tr>
<tr>
<td>
<a href="../miscshots/bootparameter4.png" target="snapshot"><img src="../miscshots/bootparameter4.png" width="300"></a>
</td>
<td>
It will allow you to edit the boot parameters.<br>
Type a space and add like on the screenshot " nomodeset".
</td>
</tr>
</table>
<br>
Now you can follow the <a href="../install">standard installation</a> procedure.


