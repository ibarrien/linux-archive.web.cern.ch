<!--#include virtual="/linux/layout/header" -->
# SLC6 - How to update
<h3>Quick Update Mini guide</h3>

As <i>root</i> on your system run:
<pre>&#35; /usr/bin/yum update</pre>

<!--
Or use graphical user interface to update system:
<pre>&#35; /usr/bin/yumex</pre>
-->

<br>
For information about system updates please check <a href="../softwaremgmt">Software Management</a> page.
<br>
For information about system configuration please check <a href="../configmgmt">Configuration Management</a> page.




