<!--#include virtual="/linux/layout/header" -->
# SLC6: Accessing CERN DFS from Linux
<h2>Accessing CERN DFS /dfs/cern.ch from Linux</h2>

CERN uses <a href="https://dfsweb.web.cern.ch/dfsweb/">Microsoft DFS</a> file system for storing users and application data for Windows systems.
<p>
This documentation outlines the setup process allowing Linux clients to access this file system.
<p>
 While the configuration may be specific to CERN SLC6 Linux distribution, it should be applicable to a modern Linux platform, providing it features at least: <b>samba 3.5.X</b>,
<b>Gnome 2.28</b> and <b>Kerberos 5</b>.
<h3>Mounting CERN DFS as a Linux file system</h3>
Starting with SLC 6.2 it is possible to mount DFS as any other remote file system (with some limitations): please see <a href="../mountdfs">documentation</a> for details.
<h3>Access via native SMB/CIFS protocol</h3>
<h4>Nautilus</h4>
Nautilus can access CERN DFS filesystem using pre-installed gvfs smb addon. SLC6 nautilus/gvfs is an kerberized application allowing access to CERN DFS using Keberos credentials,
without the need to type username/password.
<table>
<tr>
<td>
<a href="../miscshots/nautilus1.png" target="snapshot"><img src="../miscshots/nautilus1.png" width="150"></a>
</td>
<td>
Open Nautilus from <b>Places menu</b>, Select <b>Connect to Server ...</b> from <b>File</b> menu.
</td>
</tr>
<tr>
<td>
<a href="../miscshots/nautilus2.png" target="snapshot"><img src="../miscshots/nautilus2.png" width="300"></a>
</td>
<td>
Select <b>Windows share</b> as <b>Service type</b>.<br>
Enter <b>cerndfs.cern.ch</b> as <b>Server</b><br>
Enter <b>dfs</b> as <b>Share</b>.<br>
<em>DO NOT</em> fill in <b>User Name</b> nor <b>Domain Name</b>.<br>
(your SLC6 Linux system will use Kerberos authentication with your valid Kerberos credentials - to check validity of these run <pre>&#35; klist</pre>).<br>
Optionally check <b>Add bookmark</b> and enter <b>Bookmark name</b><br>
then clck <b>Connect</b> to open connection.
</td>
</tr>
<tr>
<td>
<a href="../miscshots/nautilus3.png" target="snapshot"><img src="../miscshots/nautilus3.png" width="300"></a>
</td>
<td>
From there you can browse CERN DFS file space.
</td>
</tr>
<tr>
<td>
<a href="../miscshots/nautilus5.png" target="snapshot"><img src="../miscshots/nautilus5.png" width="300"></a>
</td>
<td>
<em>Note:</em> Due to a bug in current GVFS/Nautilus implementation it appears that creating new Folders/Files in a DFS path - to which access is granted - is not possible.
As a workaround you may open a second Nautilus window on your local filesystem and drag-and-drop files from there.
</td>
</tr>
</table>
Nautilus uses GVFS (virtual filesystem for GNOME desktop) as its backend to access diverse types of filesystems. It is also possible to access GVFS-mounted filessystems from command line.
<p>
To mount DFS from command line use:
<pre>
&#35; gvfs-mount smb://cerndfs.cern.ch/dfs/
</pre>
To list DFS content:
<pre>
gvfs-ls  smb://cerndfs.cern.ch/dfs/
</pre>
Other useful gvfs related commands are:
<pre>
gvfs-cat            gvfs-less           gvfs-monitor-dir    gvfs-move           gvfs-rm             gvfs-trash
gvfs-copy           gvfs-ls             gvfs-monitor-file   gvfs-open           gvfs-save           gvfs-tree
gvfs-info           gvfs-mkdir          gvfs-mount          gvfs-rename         gvfs-set-attribute
</pre>
<h4>smbclient</h4>
Smbclient is an ftp-like client to access SMB/CIFS resources on servers. SLC6 smbclient is an kerberized application allowing access to CERN DFS using Keberos credentials,
without the need to type username/password.
<p>
To access CERN DFS using smbclient type:
<pre>
smbclient -k //cerndfs.cern.ch/dfs
</pre>
For smbclient commands please see:
<pre>
&#35; man smbclient
</pre>

<hr>
<h3>Access using Web DAV clients</h3>
<h4>Nautilus</h4>
Nautilus can access Web DAV using built-in Web DAV protocol handler.
<p>
Open Nautilus from <b>Places menu</b>. From the nautilus windows <i>File</i> menu you can select <i>Connect to Server</i>, this will bring up a dialog box. Select/enter the following:

<table>
<tr>
<td>
<a href="../miscshots/nautilus-webdav-1.png" target="snapshot"><img src="../miscshots/nautilus-webdav-1.png" width="250"></a>
</td>
<td>Service Type: <b>Secure WebDAV (HTTPS)</b><br>
Server:  <b>dfs.cern.ch</b><br>
Folder: <b>dfs</b><br>
<em>DO NOT</em> fill in <b>User Name</b> (or it will prevent password authentication).<br>
optionally check <b>Add bookmark</b> and enter a <b>Bookmark name</b><br>,
next click <b>Connect</b> to initiate connection.
</td>
</tr>
<tr>
<td>
<a href="../miscshots/nautilus-webdav-2.png" target="snapshot"><img src="../miscshots/nautilus-webdav-2.png" width="250"></a>
</td>
<td>Provide your CERN account <b>login</b><br>
and <b>password</b>,<br>
Select if the password should be: immediately forgotten, stored temporarily or permanently.
</td>
</tr>
<table>
<h4>DFS Explorer (CERN Web DAV DFS gateway)</h4>
CERN DFS Explorer can be accessed with all browsers at:
<a href="https://dfs.cern.ch/dfs">https://dfs.cern.ch/dfs</a>



