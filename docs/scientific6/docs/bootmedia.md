<!--#include virtual="/linux/layout/header" -->
# SLC6 - preparing boot media
<h2>Boot Media preparation</h2>
<div style="text-align: left;">
<h3>Download:</h3>
</div>



<ul>
<li>Download Boot CD for i386 (<a href="repository/cern/slc6X/i386/images/boot.iso">boot.iso</a>)</li>
<br>or<br>
<li>Download Boot CD for Athlon 64/Opteron/Intel with EM64T/AMD64 (x86_64): (<a href="repository/cern/slc6X/x86_64/images/boot.iso">boot.iso</a>)</li>
<br>
     (in some browsers you may need to right-click on the media link, then choose "Save")
<br>
</ul>

<h3>Create boot CD:</h3>

<ul>

      <li>On Linux systems:<br>
      &nbsp;&nbsp;execute the following command: <i>cdrecord dev=X,Y,Z boot.iso</i>  (where X,Y,Z is the SCSI bus/device/LUN of your CD writer, can
      be seen with <i>cdrecord -scanbus</i> command)<br>
      &nbsp;&nbsp;or use a graphical frontend like <i>xcdroast</i>.
      </li>
      <br>
      <li>On MS Windows systems:<br>
      &nbsp;&nbsp; <font size="-1">(use Nero Burning ROM or similar in <span style="text-decoration: underline;">raw</span> mode)</font>
      </li>
</ul>

<!--

<h3>Booting from USB pendrive</h3>

If your system BIOS supports booting from USB devices, you may download one of following images:

<ul>
<li>Download USB Boot image for i386 (<a href="repository/cern/slc6X/i386/images/diskboot.img">diskboot.img</a>)</li>
<br>or<br>
<li>Download USB Boot image for Athlon 64/Opteron/Intel with EM64T/AMD64 (x86_64): (<a href="repository/cern/slc6X/x86_64/images/diskboot.img">diskboot.img</a>)</li>
</ul>

then write it to your USB device using <b>dd</b>:
<pre>
  /bin/dd if=diskboot.img of=/dev/sd<b>X</b>
</pre>
<font color="red"><b>WARNING:</b></font> Please make sure that <b>X</b> in the above corresponds to your USB pen drive: writing to wrong
device can destroy your hard disk content!.<br> 
In case of doubt please check the content of <i>/var/log/messages</i> after connecting the pen drive. You shall see messages similar to following:
<font size="-1">
<pre>
kernel: scsi5 : SCSI emulation for USB Mass Storage devices
kernel:   Vendor: JetFlash  Model: 128MB             Rev: 1.11
kernel:   Type:   Direct-Access                      ANSI SCSI revision: 02
kernel: SCSI device sd<b>X</b>: 258048 512-byte hdwr sectors (132 MB)
kernel: Attached scsi removable disk sd<b>X</b> at scsi5, channel 0, id 0, lun 0
</pre>
</font> 

-->


