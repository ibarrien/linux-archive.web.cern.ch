<!--#include virtual="/linux/layout/header" -->
# SLC6 - Managing software
<h2>Software Management</h2>

<ul>
<li><a href="#intro">Introduction</a>
<li><a href="#sysconf">System configuration</a>
   <ul>
    <li><a href="#sysconffiles">configuration files</a>
    <li><a href="#sysconfenable">enabling/disabling updates</a>
   </ul>
<li><a href="#update">Updating packages</a>
 <ul>
   <li><a href="#ignorepkg">Excluding packages from upgrade</a>
 </ul>

<li><a href="#install">Installing new packages</a>
<li><a href="#remove">Removing installed packages</a>
<li><a href="#yumshell">Interactive yum shell</a>

</ul>

<a name="intro"></a><h4>Introduction</h4>
Scientific Linux CERN 6 systems are using <b>YUM</b> (Yellowdog Updater, Modified)
and its graphical frontend <b>Yumex</b> as software management utilities.
<br>
The original YUM was implemented for Yellowdog Linux. The current YUM implementation
is maintained by <a href="http://linux.duke.edu/projects/yum/">Linux@DUKE</a> and is used
by Red Hat Enterprise Linux and Fedora Linux distributions as default update mechanism.
<br>
In the default setup the automatic software updates mechanism
is enabled.

<a name="sysconf"></a><h4>System Configuration</h4>

Additional extensions to yum update system are distributed in following packages: <b>yum-autoupdate</b>, <b>yum-installonlyn</b>,
<b>yum-versionlock</b>, <b>yum-tsflags</b>, <b>yum-changelog</b>, <b>yum-protectbase</b>, <b>yum-kernel-module</b> and <b>yum-utils</b>.
System configuration files are distributed as <b>sl-release</b> package. All of the above are installed by default on SLC6 systems.
<br>
<br>

<a name="sysconffiles"></a><h4>Configuration files</h4>

<b>/etc/yum.repos.d/slc6-os.repo</b><br>- contains information about
location of system software repository:<br>
<pre>
[slc6-os]
name=Scientific Linux CERN 6 (SLC6) base system packages
baseurl=http://linuxsoft.cern.ch/cern/slc6X/$basearch/yum/os/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern
gpgcheck=1
enabled=1
protect=1
</pre>
<br>
<b>/etc/yum.repos.d/slc6-extras.repo</b><br>- contains information about
location of addon software repository:<br>
<pre>
[slc6-extras]
name=Scientific Linux CERN 6 (SLC6) add-on packages, no formal support
baseurl=http://linuxsoft.cern.ch/cern/slc6X/$basearch/yum/extras/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern
gpgcheck=1
enabled=1
protect=1
</pre>
<br>
<br>
<b>/etc/yum.repos.d/slc6-updates.repo</b><br>- contains information about
location of system updates software repository:<br>
<pre>
[slc6-updates]
name=Scientific Linux CERN 6 (SLC6) bugfix and security updates
baseurl=http://linuxsoft.cern.ch/cern/slc6X/$basearch/yum/updates/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern
gpgcheck=1
enabled=1
protect=1
</pre>
<br>
<br>
<b>/etc/yum.repos.d/slc6-testing.repo</b><br>- contains information about
location of software testing repository:<br>
<pre>
[slc6-testing]
name=Scientific Linux CERN 6 (SLC6) packages in testing phase
baseurl=http://linuxsoft.cern.ch/cern/slc6X/$basearch/yum/testing/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern
gpgcheck=1
enabled=1
protect=1
</pre>
<br>
<br>
<b>/etc/yum.repos.d/slc6-cernonly.repo</b><br>- contains information about
location of addon packages that are only available from within the CERN site:<br>
<pre>
[slc6-cernonly]
name=Scientific Linux CERN 6 (SLC6) CERN-only packages
baseurl=http://linuxsoft.cern.ch/onlycern/slc6X/$basearch/yum/cernonly/
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-cern
gpgcheck=1
enabled=1
protect=1
</pre>
<br>
<br>
<b>/etc/yum.repos.d/epel.repo</b><br>- contains information about
location of addon EPEL (Extra Packages for Enterprise Linux) software repository:<br>
<pre>
[epel]
name=UNSUPPORTED: Extra Packages for Enterprise Linux add-ons, no formal support from CERN
baseurl=http://linuxsoft.cern.ch/epel/6/$basearch
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6
gpgcheck=1
enabled=1
protect=0
</pre>
<br>

(<b>enabled=1</b> in above configuration files means that given repository is active by default,
<b>protect=1</b> means that given repository takes precedence over one which does not have
protect flag enabled)

Note that each of these configuration files also contains information about the source and
debuginfo packages associated to the repositories.

<p>

<br>
<b>/etc/sysconfig/yum-autoupdate</b><br> - contains settings of automatic updates system:<br>
<pre>
&#35; YUMHOUR=4 (0..23)
</pre>
Hour at which the automatic update system shall act (when invoked from <i>/etc/cron.hourly/yum-autoupdate</i>). Please note that this is not the exact time at which the update will start: The system will first wait for a random (but shorter than one hour) time interval.
(Please avoid setting YUMHOUR to 0 or 1 - at that time the software repositories are synchronized between servers and in some cases it could lead to transient errors)
<pre>
&#35; YUMMAIL=1 (1/0)
</pre>
- enables / disables e-mail messages from the update system.
<pre>
&#35; YUMMAILTO="root" (whoever@somewhere.there)
</pre>/
- e-mail address where information messages are sent.
<pre>
&#35; YUMUPDATE=1 (1|0)
</pre>
If set to <b>1</b> updates are applied, if <b>0</b> system only checks the availability of updates and sends e-mail notification.
<pre>
&#35; YUMUPDATESECONLY=0 (1|0)
</pre>
If set to <b>1</b> only security updates are applied / checked for, if to <b>0</b> all updates are applied / checked for.
<pre>
&#35; YUMRANDOMWAIT=30 (0..60)
</pre>
Maximum time od random wait (past <i>YUMHOUR</i>) before update system starts.
<pre>
&#35; YUMCLEAN=1 (1|0)
</pre>
If set to <b>1</b> system will clean up unused local package cache contents (<i>/var/cache/yum</i>).

<a name="sysconfenable"></a><h4>Enabling/Disabling update system</h4>

In order to enable the automatic update system run following commands:<br>
<pre>/sbin/chkconfig --add yum-autoupdate</pre>
<pre>/sbin/service yum-autoupdate start</pre>
<br>
In order to disable the system run:<br>
<pre>/sbin/service yum-autoupdate stop</pre>
<pre>/sbin/chkconfig --del yum-autoupdate</pre>
<br>

<a name="update"></a><h3>Updating packages</h3>

To update installed software set to latest versions run:
<pre>
/usr/bin/yum update
</pre>
which will perform necessary dependency resolution, download packages and install them.

<!--
For above also the GUI (Graphical User Interface) called <a href="yumex">yumex</a> can be used.
-->

<a name="ignorepkg"></a><h4>Excluding packages from upgrade</h4>

The update operation will apply all
upgrades available from the package repositories.
<br>
If for some reason some packages should be excluded from such upgrade
it is possible to 'lock' a list of packages at given versions:

<pre>
echo "PACKAGENAME-VERSION-RELEASE" >> /etc/yum/pluginconf.d/versionlock.list
</pre>
<br>
Please note that packages depending on 'locked' package will also be excluded from future upgrades.
<br>
To restore default system upgrade procedure please edit <i>/etc/yum/pluginconf.d/versionlock.list</i> file.


<a name="install"></a><h3>Installing packages</h3>

In order to install additional packages available in repositories
run:

<pre>
/usr/bin/yum install <i>packagename</i>
</pre>
which will download and install <i>packagename</i> along with its dependencies.

For above also the <a href="yumex">yumex</a> GUI can be used.

<a name="remove"></a><h3>Removing packages</h3>

In order to remove an installed package from the system run:

<pre>
/usr/bin/yum remove <i>packagename</i>
</pre>
which will remove the package (along with all packages depending on it, so review
the output of the command carefully...).

<!--
For above also the <a href="yumex">yumex</a> GUI can be used.
-->

<a name="clean"></a><h3>Cleaning local package repository</h3>

Over time the local yum cache (located at: <i>/var/cache/yum/</i>) can
grow considerably. In order to clean it run:
<pre>
/usr/bin/yum clean all
</pre>


<a name="yumshell"></a><h3>Interactive yum shell</h3>

Current versions of yum come with an interactive shell
providing few more interesting features (like listing of
available packages etc).

To start the shell run:
<pre>
/usr/bin/yum shell
</pre>

To list all packages available run:
<pre>
> list
</pre>

more commands and help can be found by typing:
<pre>
> help
</pre>


<a name="moreinfo"></a><h3>More information</h3>

For detailed instructions on how to perform software management using yum please
refer to the man pages:
<pre>
man yum
</pre>

<!--
For detailed instructions on how to perform software management using
yumex GUI tool please check <a href="yumex">Yumex instructions</a>.
-->

<p>
<div align=right>
<a href="mailto:Jaroslaw.Polok@cern.ch">Jaroslaw.Polok@cern.ch</a>
</div>
</p>


