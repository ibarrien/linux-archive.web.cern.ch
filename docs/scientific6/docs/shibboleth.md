<!--&#35;include virtual="/linux/layout/header" -->
# SLC6: CERN Single Sign-On

<h2>Windows Single Sign On (SSO) / CERN Authentication integration on Scientific Linux CERN / Apache and Shibboleth.</h2>

<h3>About CERN Single Sign On and Shibboleth</h3>
<ul>
<li><a href="https://espace.cern.ch/authentication/default.aspx">CERN Single Sign On documentation</a>.
</li>
<li><a href="http://shibboleth.internet2.edu/">Shibboleth information and documentation</a>.
</ul>
<br>

<h3>Installation</h3>

As root on your system run:

<pre>
&#35; yum install shibboleth log4shib xmltooling-schemas opensaml-schemas
</pre>
(above command will pull in all dependencies for above packages,<br>
including: curl-openssl, xerces-c, xml-security-c, opensaml and log4cpp<br>
coming from extras repository for SLC6<br>
<br>
<em>Note:</em> The SELinux policy has not been implemented for Shibboleth 2
therefore SELinux must be changed to run in <i>permissive mode</i> on your system for
Single Sign On to work. For this please edit <b>/etc/sysconfig/selinux</b> file,
and replace the line:
<pre>
SELINUX=enforcing
</pre>
by
<pre>
SELINUX=permissive
</pre>
Next reboot your system or run:
<pre>
/usr/sbin/setenforce Permissive
</pre>
for the change to take effect.
<br>

<strike>SLC6 libcurl is precompiled against NSS, while shibboleth requires a libcurl compiled agains OpenSSL: the curl-openssl packages
provide such modified version of libcurl library which is installed in parallel to the standard curl libraries.
<p>
In order to make shibboleth daemon use this library, edit <i>/etc/sysconfig/shibd</i> file and insert there following
two lines:
<pre>
LD_PRELOAD=/opt/shibboleth/lib64/libcurl.so.4
export LD_PRELOAD
</pre>
(change lib64 to lib for installation on a 32bit system)
</strike>
this is not needed , updated shibboleth packages will use a special libcurl-openssl version solving the problem.
<h3>Configuration for CERN Single Sign On</h3>
<ul>
<li>We assume that at this point your apache web service (httpd) is already configured and running.
<br>
<li>Enable automatic startup of shibboleth daemon:
<pre>
&#35; /sbin/chkconfig --levels 345 shibd on
</pre>
<li>Copy following configuration files to <i>/etc/shibboleth/</i> directory:
<br>
 <ul>
  <li><a href="../shibboleth/shibboleth2.xml">shibboleth2.xml</a> (main shibboleth configuration file customized for CERN SSO).
  <li><a href="../shibboleth/ADFS-metadata.xml">ADFS-metadata.xml</a> (ADFS configuration customized for CERN SSO)
  <li><a href="../shibboleth/attribute-map.xml">attribute-map.xml</a> (ADFS attribute mapping)
  <li><a href="../shibboleth/wsignout.gif">wsignout.gif</a>
 </ul>

<br>
<li>Edit <i>/etc/shibboleth/shibboleth2.xml</i><br>
<ul>
 <li>set up the listener host (default setting of localhost should be used in most cases):
<pre>&lt;TCPListener address="127.0.0.1" port="1600" acl="127.0.0.1"/&gt;</pre>
 <li>replace <b>ALL 5</b> occurences of <b>somehost.cern.ch</b>, by your system hostname:
 <ul>
  <li>&lt;Site id=&quot;1&quot; name=&quot;<b>somehost.cern.ch</b>&quot;/&gt;
  <li>&lt;Host name=&quot;<b>somehost.cern.ch</b>&quot;/&gt;
  <li>&lt;ApplicationDefaults id=&quot;default&quot; policyId=&quot;default&quot; entityID=&quot;https://<b>somehost.cern.ch</b>/Shibboleth.sso/ADFS&quot; homeURL=&quot;https://<b>somehost.cern.ch</b>&quot; ....
  <li>&lt;saml:Audience&gt;<b>https://somehost.cern.ch</b>/Shibboleth.sso/ADFS&lt;/saml:Audience&gt;
</ul>
<br>
<li>Review <i>/etc/httpd/conf.d/shib.conf</i> shibboleth apache configuration.
<br>
<br>
<li>Configure per-directory (in <i>.htaccess</i> file) or global (in <i>/etc/httpd/conf.d/shib.conf</i>) authentication rules:
<pre>
&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;
SSLRequireSSL   &#35; The modules only work using HTTPS
AuthType shibboleth
ShibRequireSession On
ShibRequireAll On
ShibExportAssertion Off

&#35;&#35;&#35; ShibUseHeaders On
&#35;&#35;&#35; Uncomment above line if you want shibboleth to
&#35;&#35;&#35; use also old-style request headers
&#35;&#35;&#35; may be required for use with Tomcat, or to
&#35;&#35;&#35; allow easy migration of older applications.
&#35;&#35;&#35; It is strongly recommended not to use above
&#35;&#35;&#35; option in order to improve security.
&lt;RequireAll&gt;
    Require valid-user
    Require ADFS_GROUP "Some Users Group" "Some Other Users Group"
&lt;/RequireAll&gt;
&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;&#35;
</pre>
<br>
<li>Script configuration:
<br>
Please note that contrary to the previous "NICE password"
authentication on central WEB/AFS services, the <tt>REMOTE_USER</tt>
field now holds the user mail address (and not the
login name). To get back at the login name, you have to use
<tt>HTTP_ADFS_LOGIN</tt> instead.</li>
<br>
<li>ADFS Application configuration:
<br>
Once your Apache Web application is configured, you simply need to have your application added to the allowed application
list in CERN Single Sign On. <br>
To do so, <a href="http://www.cern.ch/winservices/SSO/registerapplication.aspx">simply go to this form</a> and specify these 3 items:
<ul>
   <li> Your <b>Application Name</b>, please provide a telling name for your application (it must be unique).
   <li> Your application <b>URL</b>, as declared in saml:Audience property above.
   <li> Your <b>name</b> and <b>email</b> for further contact.
</ul>
<br>
<li> Once you get a confirmation that your application has been configured for CERN SSO, (re)start services on your system as root:
<pre>
&#35; /sbin/service shibd restart
&#35; /sbin/service httpd restart
</pre>

</ul>

<h3>Support</h3>
For problems related to packaging of shibboleth / log4shib, contact: <a href="mailto: linux.support@cern.ch">linux.support@cern.ch</a>
<br>
For information and help about shibboleth configuration for CERN Single Sign On, see:
<a href="https://espace.cern.ch/authentication/default.aspx">CERN Authentication web pages</a>
<br>

<div align="right"><a href="mailto:Jaroslaw.Polok@cern.ch">Jaroslaw.Polok@cern.ch</a></div>

