<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on SVN Servers:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envIT_PES" href="../envIT_PES">IT-PES</a></td><td class="statusunknown">unknown</td><td> Helge Meinhard</td><td class="statusfail">fail</td><td></td><td></td><td>2011-11-15</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: SVN Servers (environment) </h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><b>SVN Servers</b></td><td class="statusunknown">unknown</td><td> Nils Hoimyr</td><td class="status(none)">(none)</td><td></td><td>should work with slc6, but not yest tested</td><td>2011-10-21</td>
</tr>
</table>
<hr size="2" /><h2>Entities that SVN Servers depends on:</h2>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>