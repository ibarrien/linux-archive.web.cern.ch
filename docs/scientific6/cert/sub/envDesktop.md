<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on Desktop:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envCertification" href="../envCertification">Certification</a></td><td class="statusunknown">unknown</td><td> Jan van Eldik</td><td class="statusfail">fail</td><td></td><td></td><td>2012-01-24</td>
</tr>
<tr class ="blocking">
<td><a name="envPLUS_BATCH" href="../envPLUS_BATCH">PLUS/BATCH</a></td><td class="statusunknown">unknown</td><td> GavinMcCance</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-15</td>
</tr>
<tr class ="">
<td><a name="envIT_OIS_ODS" href="../envIT_OIS_ODS">IT-OIS-ODS</a></td><td class="statusunknown">unknown</td><td> Andreas Wagner</td><td class="statusok">ok</td><td></td><td>Linux support</td><td>2011-11-14</td>
</tr>
<tr class ="blocking">
<td><a name="envATLAS_online" href="../envATLAS_online">ATLAS-online</a></td><td class="statusunknown">unknown</td><td> Bruce Barnett</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-14</td>
</tr>
<tr class ="">
<td><a name="envEngineer_Desktop" href="../envEngineer_Desktop">Engineer Desktop</a></td><td class="statusunknown">unknown</td><td> Nils Hoimyr</td><td class="statusfail">fail</td><td></td><td></td><td>2011-11-14</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: Desktop (environment) </h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><b>Desktop</b></td><td class="statusok">ok</td><td> Jarek Polok</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-14</td>
</tr>
</table>
<hr size="2" /><h2>Entities that Desktop depends on:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envDevelopment_Support" href="../envDevelopment_Support">Development Support</a></td><td class="statusok">ok</td><td> Jarek Polok</td><td class="statusworksforme">worksforme</td><td></td><td></td><td>2011-11-14</td>
</tr>
<tr class ="">
<td><a name="envBase" href="../envBase">Base</a></td><td class="statusok">ok</td><td> Jarek Polok</td><td class="statusok">ok</td><td></td><td></td><td>2011-10-21</td>
</tr>
</table><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgprinting" href="../pkgprinting">printing</a></td><td></td><td class="statusok">ok</td><td> Jarek Polok</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgmail_clients" href="../pkgmail_clients">mail clients</a></td><td></td><td class="statusok">ok</td><td> Jan van Eldik</td><td></td><td class="statuswont">wont</td><td>(alpine, mozilla, thunderbird, evolution) vs CERN IMAP server.</td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgtcsh" href="../pkgtcsh">tcsh</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgssh" href="../pkgssh">ssh</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgopenoffice" href="../pkgopenoffice">openoffice</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgEDH_web_client" href="../pkgEDH_web_client">EDH web client</a></td><td></td><td class="statusunknown">unknown</td><td> Derek Mathieson</td><td></td><td class="statuswont">wont</td><td></td><td>2011-05-06</td><td>connect to http://edh.cern.ch</td><td></td>
</tr>
<tr>
<td><a name="pkgxscreensaver" href="../pkgxscreensaver">xscreensaver</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkggdm" href="../pkggdm">gdm</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgHEPiX_shells_scripts" href="../pkgHEPiX_shells_scripts">HEPiX shells scripts</a></td><td></td><td class="statustest">test</td><td> Jan van Eldik</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-10-19</td><td></td><td>RPM/AFS</td>
</tr>
<tr>
<td><a name="pkgkernel" href="../pkgkernel">kernel</a></td><td></td><td class="statusok">ok</td><td> Jarek Polok</td><td></td><td class="statusok">ok</td><td></td><td>2011-10-19</td><td></td><td>RPM</td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>