<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on BE/CO-development:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envCertification" href="../envCertification">Certification</a></td><td class="statusunknown">unknown</td><td> Jan van Eldik</td><td class="statusfail">fail</td><td></td><td></td><td>2012-01-24</td>
</tr>
<tr class ="blocking">
<td><a name="envBE_CO_operations" href="../envBE_CO_operations">BE/CO-operations</a></td><td class="statusok">ok</td><td> Alastair Bland</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2012-01-24</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: BE/CO-development (environment) </h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><b>BE/CO-development</b></td><td class="statusok">ok</td><td> Nicolas de Metz-Noblat</td><td class="statustest">test</td><td>blocking</td><td></td><td>2011-10-21</td>
</tr>
</table>
<hr size="2" /><h2>Entities that BE/CO-development depends on:</h2><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgmail_clients" href="../pkgmail_clients">mail clients</a></td><td></td><td class="statusok">ok</td><td> Jan van Eldik</td><td></td><td class="statuswont">wont</td><td>(alpine, mozilla, thunderbird, evolution) vs CERN IMAP server.</td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgMathematica8" href="../pkgMathematica8">Mathematica</a></td><td>8</td><td class="statusworksforme">worksforme</td><td> Philippe Defert</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-20</td><td></td><td>/afs/cern.ch/project/parc/</td>
</tr>
<tr>
<td><a name="pkgOpenMotif" href="../pkgOpenMotif">OpenMotif</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkggdb" href="../pkggdb">gdb</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkggcc" href="../pkggcc">gcc</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-10-19</td><td>http://gcc.gnu.org/bugzilla/show_bug.cgi?id=48597 to be fixed in 6.2</td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgemacs" href="../pkgemacs">emacs</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgORACLE_Instant_Client11_0_2_0_3" href="../pkgORACLE_Instant_Client11_0_2_0_3">ORACLE Instant Client</a></td><td>11.0.2.0.3</td><td class="statusworksforme">worksforme</td><td> Nilo Segura Chinchilla</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-10-21</td><td></td><td>RPM, "onlycern" repository</td>
</tr>
<tr>
<td><a name="pkgHEPiX_shells_scripts" href="../pkgHEPiX_shells_scripts">HEPiX shells scripts</a></td><td></td><td class="statustest">test</td><td> Jan van Eldik</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-10-19</td><td></td><td>RPM/AFS</td>
</tr>
<tr>
<td><a name="pkgCVS_client" href="../pkgCVS_client">CVS client</a></td><td></td><td class="statustest">test</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td>GSSAPI with load-balanced isscvs needs to work</td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgkernel_source" href="../pkgkernel_source">kernel-source</a></td><td></td><td class="statusok">ok</td><td> Jarek Polok</td><td></td><td class="statusok">ok</td><td>means: ability to compile modules against CERN kernel, and create customized kernel versions</td><td>2011-10-19</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgOpenAFS1_4_14" href="../pkgOpenAFS1_4_14">OpenAFS</a></td><td>1.4.14</td><td class="statusok">ok</td><td> Rainer Tobbicke</td><td></td><td class="statusok">ok</td><td>problem with afs_admin (no permission)</td><td>2011-10-21</td><td>/afs/cern.ch/project/linux/dev/benchmarks/config/stress/afs-fs-stress.sh</td><td>RPM</td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>