<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on ORACLE Instant Client:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envCOMPASS" href="../envCOMPASS">COMPASS</a></td><td class="statusunknown">unknown</td><td> Sergei Gerassimov</td><td class="statustest">test</td><td></td><td></td><td>2011-10-19</td>
</tr>
<tr class ="">
<td><a name="envHARP" href="../envHARP">HARP</a></td><td class="statusunknown">unknown</td><td> Simone Giani</td><td class="statustest">test</td><td></td><td></td><td>2011-05-06</td>
</tr>
<tr class ="blocking">
<td><a name="envBE_CO_development" href="../envBE_CO_development">BE/CO-development</a></td><td class="statusunknown">unknown</td><td> Nicolas de Metz-Noblat</td><td class="statustest">test</td><td>blocking</td><td></td><td>2011-10-19</td>
</tr>
<tr class ="">
<td><a name="envDevelopment_Support" href="../envDevelopment_Support">Development Support</a></td><td class="statusunknown">unknown</td><td> Jarek Polok</td><td class="statustest">test</td><td></td><td></td><td>2011-10-19</td>
</tr>
<tr class ="">
<td><a name="envIT_PES" href="../envIT_PES">IT-PES</a></td><td class="statusunknown">unknown</td><td> Helge Meinhard</td><td class="statusfail">fail</td><td></td><td></td><td>2011-10-19</td>
</tr>
<tr class ="blocking">
<td><a name="envLHCb_Online" href="../envLHCb_Online">LHCb Online</a></td><td class="statusunknown">unknown</td><td> Rainer Schwemmer</td><td class="statustest">test</td><td>blocking</td><td></td><td>2011-10-19</td>
</tr>
</table><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgCASTOR_client_sw2_1_8_0" href="../pkgCASTOR_client_sw2_1_8_0">CASTOR client sw</a></td><td>2.1.8-0</td><td class="statusunknown">unknown</td><td> Sebastien Ponce</td><td></td><td class="statustest">test</td><td></td><td>2011-10-19</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgperl_DBD_Oracle" href="../pkgperl_DBD_Oracle">perl-DBD-Oracle</a></td><td></td><td class="statusok">ok</td><td> Jan van Eldik</td><td></td><td class="statustest">test</td><td></td><td>2011-10-19</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgPVSS3_8" href="../pkgPVSS3_8">PVSS</a></td><td>3.8</td><td class="statusunknown">unknown</td><td> Piotr Golonka</td><td></td><td class="statustest">test</td><td></td><td>2011-10-19</td><td></td><td></td>
</tr>
</table>
<hr size="2" /><h2>Current entity: ORACLE Instant Client (package) </h2><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><b>ORACLE Instant Client</b></td><td>11.0.2.0.2</td><td class="statustest">test</td><td> Nilo Segura Chinchilla</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM, "onlycern" repository</td>
</tr>
</table>
<hr size="2" /><h2>Entities that ORACLE Instant Client depends on:</h2>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>