<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on Non-LHC experiments:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envCertification" href="../envCertification">Certification</a></td><td class="statusunknown">unknown</td><td> Jan van Eldik</td><td class="statusfail">fail</td><td></td><td></td><td>2012-01-24</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: Non-LHC experiments (environment) </h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><b>Non-LHC experiments</b></td><td class="statusunknown">unknown</td><td> Sergei Gerassimov</td><td class="statusfail">fail</td><td>blocking</td><td></td><td>2011-11-15</td>
</tr>
</table>
<hr size="2" /><h2>Entities that Non-LHC experiments depends on:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envDELPHI" href="../envDELPHI">DELPHI</a></td><td class="statusunknown">unknown</td><td> Fabio Cossutti</td><td class="status(none)">(none)</td><td></td><td></td><td>2010-11-11</td>
</tr>
<tr class ="">
<td><a name="envNA48" href="../envNA48">NA48</a></td><td class="statusunknown">unknown</td><td> Andrew Maier</td><td class="statustest">test</td><td></td><td></td><td>2011-11-14</td>
</tr>
<tr class ="">
<td><a name="envCOMPASS" href="../envCOMPASS">COMPASS</a></td><td class="statusunknown">unknown</td><td> claude marchand</td><td class="statustest">test</td><td></td><td></td><td>2011-11-14</td>
</tr>
<tr class ="">
<td><a name="envOPAL" href="../envOPAL">OPAL</a></td><td class="statusfail">fail</td><td> Matthias Schroder</td><td class="statusworksforme">worksforme</td><td></td><td>Waiting for CERNLIB. Had to drop dependency on GPHIGS :(</td><td>2011-11-15</td>
</tr>
<tr class ="">
<td><a name="envHARP" href="../envHARP">HARP</a></td><td class="statusunknown">unknown</td><td> Simone Giani</td><td class="statusworksforme">worksforme</td><td></td><td></td><td>2011-10-21</td>
</tr>
<tr class ="">
<td><a name="envDevelopment_Support" href="../envDevelopment_Support">Development Support</a></td><td class="statusok">ok</td><td> Jarek Polok</td><td class="statusworksforme">worksforme</td><td></td><td></td><td>2011-11-14</td>
</tr>
<tr class ="">
<td><a name="envDIRAC" href="../envDIRAC">DIRAC</a></td><td class="statusunknown">unknown</td><td> Leonid Afanasev</td><td class="status(none)">(none)</td><td></td><td></td><td>2010-11-11</td>
</tr>
<tr class ="">
<td><a name="envOPERA" href="../envOPERA">OPERA</a></td><td class="statusunknown">unknown</td><td> chaussad</td><td class="status(none)">(none)</td><td></td><td></td><td>2010-11-11</td>
</tr>
<tr class ="">
<td><a name="envALEPH" href="../envALEPH">ALEPH</a></td><td class="statusunknown">unknown</td><td> Jacques Boucrot</td><td class="status(none)">(none)</td><td></td><td>(ALEPH has frozen the archiving system on SLC3)</td><td>2010-11-11</td>
</tr>
<tr class ="">
<td><a name="envNA49" href="../envNA49">NA49</a></td><td class="statusunknown">unknown</td><td> Andres Sandoval</td><td class="status(none)">(none)</td><td></td><td></td><td>2010-11-11</td>
</tr>
<tr class ="">
<td><a name="envNA60" href="../envNA60">NA60</a></td><td class="statusunknown">unknown</td><td> Andre David</td><td class="statustest">test</td><td></td><td></td><td>2011-11-14</td>
</tr>
<tr class ="blocking">
<td><a name="envPLUS_BATCH" href="../envPLUS_BATCH">PLUS/BATCH</a></td><td class="statusunknown">unknown</td><td> GavinMcCance</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-15</td>
</tr>
</table><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgCERNLIB2006_3" href="../pkgCERNLIB2006_3">CERNLIB</a></td><td>2006-3</td><td class="statusunknown">unknown</td><td> Benedikt Hegner</td><td></td><td class="statusworksforme">worksforme</td><td></td><td>2011-11-14</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgROOT" href="../pkgROOT">ROOT</a></td><td></td><td class="statusunknown">unknown</td><td> Fons Rademakers</td><td></td><td class="statusok">ok</td><td></td><td>2011-10-19</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgDATE" href="../pkgDATE">DATE</a></td><td></td><td class="statusunknown">unknown</td><td> Fons Rademakers</td><td></td><td class="status(none)">(none)</td><td></td><td>2010-11-11</td><td></td><td></td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>