<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on IT-FIO-FS:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envIT_FIO" href="../envIT_FIO">IT-FIO</a></td><td class="statusunknown">unknown</td><td> Jan van Eldik</td><td class="statuswont">wont</td><td></td><td></td><td>2009-12-17</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: IT-FIO-FS (environment) </h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><b>IT-FIO-FS</b></td><td class="statusunknown">unknown</td><td> Olof Barring</td><td class="statuswont">wont</td><td></td><td>FIO Fabric services</td><td>2009-12-17</td>
</tr>
</table>
<hr size="2" /><h2>Entities that IT-FIO-FS depends on:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><a name="envPLUS_BATCH" href="../envPLUS_BATCH">PLUS/BATCH</a></td><td class="statusunknown">unknown</td><td> Ulrich Schwickerath</td><td class="statuswont">wont</td><td>blocking</td><td>https://twiki.cern.ch/twiki/bin/view/FIOgroup/BatchServices#SLC5; work will not start before end of August 08</td><td>2009-11-03</td>
</tr>
<tr class ="">
<td><a name="envCASTOR_service" href="../envCASTOR_service">CASTOR service</a></td><td class="statusunknown">unknown</td><td> Olof Barring</td><td class="statusunknown">unknown</td><td></td><td>diskservers ok and in prod., code for headnodes not yet certified</td><td>2009-12-17</td>
</tr>
<tr class ="">
<td><a name="envDisk_Servers" href="../envDisk_Servers">Disk Servers</a></td><td class="statusunknown">unknown</td><td> Olof Barring</td><td class="status(none)">(none)</td><td></td><td></td><td>2009-09-23</td>
</tr>
</table><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgZUUL" href="../pkgZUUL">ZUUL</a></td><td></td><td class="statusunknown">unknown</td><td> Jan Iven</td><td>project-elfms@cern.ch</td><td class="status(none)">(none)</td><td>user management for FIO clusters, replaces REGIS. Some intermittent lookup failures being debugged.</td><td>2008-07-31</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgPrepareInstall" href="../pkgPrepareInstall">PrepareInstall</a></td><td></td><td class="statusunknown">unknown</td><td> Veronique Lefebure</td><td></td><td class="statusunknown">unknown</td><td></td><td>2009-09-25</td><td></td><td>RPM</td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>