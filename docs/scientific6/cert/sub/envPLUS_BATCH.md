<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on PLUS/BATCH:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envCertification" href="../envCertification">Certification</a></td><td class="statusunknown">unknown</td><td> Jan van Eldik</td><td class="statusfail">fail</td><td></td><td></td><td>2012-01-24</td>
</tr>
<tr class ="blocking">
<td><a name="envNon_LHC_experiments" href="../envNon_LHC_experiments">Non-LHC experiments</a></td><td class="statusunknown">unknown</td><td> Sergei Gerassimov</td><td class="statusfail">fail</td><td>blocking</td><td></td><td>2011-11-15</td>
</tr>
<tr class ="">
<td><a name="envIT_PES" href="../envIT_PES">IT-PES</a></td><td class="statusunknown">unknown</td><td> Helge Meinhard</td><td class="statusfail">fail</td><td></td><td></td><td>2011-11-15</td>
</tr>
<tr class ="blocking">
<td><a name="envCMS_offline" href="../envCMS_offline">CMS-offline</a></td><td class="statusunknown">unknown</td><td> Peter Elmer</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-15</td>
</tr>
<tr class ="blocking">
<td><a name="envATLAS_offline" href="../envATLAS_offline">ATLAS-offline</a></td><td class="statustest">test</td><td> Emil Obreshkov</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2012-01-24</td>
</tr>
<tr class ="blocking">
<td><a name="envALICE_offline" href="../envALICE_offline">ALICE-offline</a></td><td class="statusunknown">unknown</td><td> Fons Rademakers</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-15</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: PLUS/BATCH (environment) </h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><b>PLUS/BATCH</b></td><td class="statusunknown">unknown</td><td> GavinMcCance</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-15</td>
</tr>
</table>
<hr size="2" /><h2>Entities that PLUS/BATCH depends on:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envDevelopment_Support" href="../envDevelopment_Support">Development Support</a></td><td class="statusok">ok</td><td> Jarek Polok</td><td class="statusworksforme">worksforme</td><td></td><td></td><td>2011-11-14</td>
</tr>
<tr class ="">
<td><a name="envCASTOR_service" href="../envCASTOR_service">CASTOR service</a></td><td class="statusunknown">unknown</td><td> Massimo Lamanna</td><td class="statusworksforme">worksforme</td><td></td><td></td><td>2011-11-14</td>
</tr>
<tr class ="blocking">
<td><a name="envDesktop" href="../envDesktop">Desktop</a></td><td class="statusok">ok</td><td> Jarek Polok</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-14</td>
</tr>
</table><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgperl_Proc_ProcessTable" href="../pkgperl_Proc_ProcessTable">perl-Proc-ProcessTable</a></td><td></td><td class="statusok">ok</td><td> Jan van Eldik</td><td></td><td class="status(none)">(none)</td><td>EPEL</td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgperl" href="../pkgperl">perl</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgprinting" href="../pkgprinting">printing</a></td><td></td><td class="statusok">ok</td><td> Jarek Polok</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgphonebook" href="../pkgphonebook">phonebook</a></td><td></td><td class="statusok">ok</td><td> Jarek Polok</td><td></td><td class="status(none)">(none)</td><td>replaces phone command</td><td>2011-05-06</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgtkdiff" href="../pkgtkdiff">tkdiff</a></td><td></td><td class="statusok">ok</td><td> Matthias Schroder</td><td></td><td class="status(none)">(none)</td><td>comes with tkcvs from EPEL</td><td>2011-05-06</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgssh" href="../pkgssh">ssh</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgxscreensaver" href="../pkgxscreensaver">xscreensaver</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgHEPiX_shells_scripts" href="../pkgHEPiX_shells_scripts">HEPiX shells scripts</a></td><td></td><td class="statustest">test</td><td> Jan van Eldik</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-10-19</td><td></td><td>RPM/AFS</td>
</tr>
<tr>
<td><a name="pkgREMEDY_web_client" href="../pkgREMEDY_web_client">REMEDY web client</a></td><td></td><td class="statusunknown">unknown</td><td> Veronique Lefebure</td><td></td><td class="statuswont">wont</td><td></td><td>2011-10-19</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgHummingbird_eXceed" href="../pkgHummingbird_eXceed">Hummingbird eXceed</a></td><td></td><td class="statuswont">wont</td><td> Sebastien Dellabella</td><td></td><td class="status(none)">(none)</td><td>To be replaced by X-Win32</td><td>2011-11-15</td><td>(connect to Linux machines, both through XDMCP and directly. Check "pine" and terminal colors)</td><td>(n.a.)</td>
</tr>
<tr>
<td><a name="pkgmail_clients" href="../pkgmail_clients">mail clients</a></td><td></td><td class="statusok">ok</td><td> Jan van Eldik</td><td></td><td class="statuswont">wont</td><td>(alpine, mozilla, thunderbird, evolution) vs CERN IMAP server.</td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgacrobat" href="../pkgacrobat">acrobat</a></td><td></td><td class="statuswont">wont</td><td> Jarek Polok</td><td></td><td class="status(none)">(none)</td><td>Evince works well on SLC6</td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgghostview_gv" href="../pkgghostview_gv">ghostview/gv</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgCERN_TeX_styles" href="../pkgCERN_TeX_styles">CERN TeX styles</a></td><td></td><td class="statustest">test</td><td> Jan Iven</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgTeX_LaTeX" href="../pkgTeX_LaTeX">TeX/LaTeX</a></td><td></td><td class="statustest">test</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgacrontab" href="../pkgacrontab">acrontab</a></td><td></td><td class="statusunknown">unknown</td><td> Rainer Tobbicke</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-01-07</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgarc" href="../pkgarc">arc</a></td><td></td><td class="statusunknown">unknown</td><td> Rainer Tobbicke</td><td></td><td class="status(none)">(none)</td><td></td><td>2010-11-11</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgELFms" href="../pkgELFms">ELFms</a></td><td></td><td class="statusunknown">unknown</td><td> Veronique Lefebure</td><td>project-elfms@cern.ch</td><td class="statusunknown">unknown</td><td></td><td>2011-05-27</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgemacs" href="../pkgemacs">emacs</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgpico" href="../pkgpico">pico</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td>EPEL (comes with alpine)</td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgLSF" href="../pkgLSF">LSF</a></td><td></td><td class="statusworksforme">worksforme</td><td> Ulrich Schwickerath</td><td></td><td class="statusok">ok</td><td></td><td>2011-10-21</td><td></td><td>RPM (CC-addons)</td>
</tr>
<tr>
<td><a name="pkgperl_BSD_Resource" href="../pkgperl_BSD_Resource">perl-BSD-Resource</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgperl_Text_CSV_XS" href="../pkgperl_Text_CSV_XS">perl-Text-CSV_XS</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td>EPEL</td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgperl_SQL_Statement" href="../pkgperl_SQL_Statement">perl-SQL-Statement</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td>EPEL</td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgperl_DBD_Oracle" href="../pkgperl_DBD_Oracle">perl-DBD-Oracle</a></td><td></td><td class="statusok">ok</td><td> Jan van Eldik</td><td></td><td class="statusworksforme">worksforme</td><td></td><td>2011-10-21</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgperl_Tk" href="../pkgperl_Tk">perl-Tk</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td>EPEL</td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgperl_DBD_CSV" href="../pkgperl_DBD_CSV">perl-DBD-CSV</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td>EPEL</td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>