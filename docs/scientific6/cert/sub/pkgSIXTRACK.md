<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on SIXTRACK:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envIT_PES" href="../envIT_PES">IT-PES</a></td><td class="statusunknown">unknown</td><td> Helge Meinhard</td><td class="statusfail">fail</td><td></td><td></td><td>2011-11-15</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: SIXTRACK (package) </h2><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><b>SIXTRACK</b></td><td></td><td class="statusunknown">unknown</td><td> Frank Schmidt</td><td></td><td class="statuswont">wont</td><td></td><td>2011-11-14</td><td></td><td>http://cern.ch/frs/Source/</td>
</tr>
</table>
<hr size="2" /><h2>Entities that SIXTRACK depends on:</h2><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgCASTOR_client_sw2_1_11" href="../pkgCASTOR_client_sw2_1_11">CASTOR client sw</a></td><td>2.1.11</td><td class="statusworksforme">worksforme</td><td> Sebastien Ponce</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-11-14</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgfsplit5_5_3" href="../pkgfsplit5_5_3">fsplit</a></td><td>5.5-3</td><td class="statusworksforme">worksforme</td><td> Jan van Eldik</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-04-12</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkghtml2ps" href="../pkghtml2ps">html2ps</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgf2c" href="../pkgf2c">f2c</a></td><td></td><td class="statuswont">wont</td><td> Jan van Eldik</td><td></td><td class="status(none)">(none)</td><td>(available from atrpms-stable repository???)</td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>