<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on ATLAS-online:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envCertification" href="../envCertification">Certification</a></td><td class="statusunknown">unknown</td><td> Jan van Eldik</td><td class="statusfail">fail</td><td></td><td></td><td>2012-01-24</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: ATLAS-online (environment) </h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><b>ATLAS-online</b></td><td class="statusunknown">unknown</td><td> Bruce Barnett</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-14</td>
</tr>
</table>
<hr size="2" /><h2>Entities that ATLAS-online depends on:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><a name="envDesktop" href="../envDesktop">Desktop</a></td><td class="statusok">ok</td><td> Jarek Polok</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-14</td>
</tr>
<tr class ="blocking">
<td><a name="envLCG_application_Area" href="../envLCG_application_Area">LCG application Area</a></td><td class="statusunknown">unknown</td><td> Benedikt Hegner</td><td class="statustest">test</td><td>blocking</td><td></td><td>2011-11-14</td>
</tr>
</table><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgperl" href="../pkgperl">perl</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgJava__JDK_" href="../pkgJava__JDK_">Java (JDK)</a></td><td></td><td class="statusok">ok</td><td> Jarek Polok</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-10-19</td><td></td><td>RPM, in "onlycern" repository</td>
</tr>
<tr>
<td><a name="pkgOpenMotif" href="../pkgOpenMotif">OpenMotif</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkggcc" href="../pkggcc">gcc</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-10-19</td><td>http://gcc.gnu.org/bugzilla/show_bug.cgi?id=48597 to be fixed in 6.2</td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgssh" href="../pkgssh">ssh</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgGAUDI" href="../pkgGAUDI">GAUDI</a></td><td></td><td class="statusunknown">unknown</td><td> Marco Cattaneo</td><td></td><td class="statustest">test</td><td></td><td>2011-11-14</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgexpect" href="../pkgexpect">expect</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgqt" href="../pkgqt">qt</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgpython" href="../pkgpython">python</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgmysql" href="../pkgmysql">mysql</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgCMT" href="../pkgCMT">CMT</a></td><td></td><td class="statustest">test</td><td> Grigori Rybkin</td><td></td><td class="statusok">ok</td><td></td><td>2011-11-14</td><td></td><td>/afs/cern.ch/sw/contrib/CMT</td>
</tr>
</table><h4>Configurations</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="last poll">last poll</th><th col="timestamp">timestamp</th><th col="comment">comment</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="cfge1000_driver" href="../cfge1000_driver">e1000 driver</a></td><td class="statuswont">wont</td><td> Jarek Polok</td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td>in kernel now</td><td></td>
</tr>
<tr>
<td><a name="cfgkernel_scheduling" href="../cfgkernel_scheduling">kernel-scheduling</a></td><td class="statusok">ok</td><td> Matthias Schroder</td><td class="status(none)">(none)</td><td></td><td>2011-10-19</td><td></td><td>(included in kernel)</td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>