<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on IT-OIS-ODS:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envIT_OIS" href="../envIT_OIS">IT-OIS</a></td><td class="statusunknown">unknown</td><td> Tim Bell</td><td class="statuswont">wont</td><td></td><td></td><td>2011-11-18</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: IT-OIS-ODS (environment) </h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><b>IT-OIS-ODS</b></td><td class="statusunknown">unknown</td><td> Andreas Wagner</td><td class="statusok">ok</td><td></td><td>Linux support</td><td>2011-11-14</td>
</tr>
</table>
<hr size="2" /><h2>Entities that IT-OIS-ODS depends on:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><a name="envDesktop" href="../envDesktop">Desktop</a></td><td class="statusok">ok</td><td> Jarek Polok</td><td class="statuswont">wont</td><td>blocking</td><td></td><td>2011-11-14</td>
</tr>
<tr class ="">
<td><a name="envCVI" href="../envCVI">CVI</a></td><td class="statusok">ok</td><td> Jan van Eldik</td><td class="statusok">ok</td><td></td><td>CERN Virtual Infrastructure</td><td>2011-10-19</td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>