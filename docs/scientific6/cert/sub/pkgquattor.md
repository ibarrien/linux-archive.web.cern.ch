<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on quattor:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><a name="envLHCb_Online" href="../envLHCb_Online">LHCb Online</a></td><td class="statusunknown">unknown</td><td> Rainer Schwemmer</td><td class="statusworksforme">worksforme</td><td>blocking</td><td></td><td>2011-12-02</td>
</tr>
<tr class ="">
<td><a name="envWeb_servers" href="../envWeb_servers">Web servers</a></td><td class="statusunknown">unknown</td><td> Andreas Wagner</td><td class="statusok">ok</td><td></td><td></td><td>2011-05-06</td>
</tr>
<tr class ="blocking">
<td><a name="envCMS_online" href="../envCMS_online">CMS-online</a></td><td class="statusunknown">unknown</td><td> Marc Dobson</td><td class="statustest">test</td><td>blocking</td><td></td><td>2011-12-02</td>
</tr>
<tr class ="">
<td><a name="envCERN_Document_Service" href="../envCERN_Document_Service">CERN Document Service</a></td><td class="statusunknown">unknown</td><td> Tibor Simko</td><td class="statusok">ok</td><td></td><td></td><td>2011-10-21</td>
</tr>
</table><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgELFms" href="../pkgELFms">ELFms</a></td><td></td><td class="statusunknown">unknown</td><td> Veronique Lefebure</td><td>project-elfms@cern.ch</td><td class="statusunknown">unknown</td><td></td><td>2011-05-27</td><td></td><td></td>
</tr>
</table>
<hr size="2" /><h2>Current entity: quattor (package) </h2><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><b>quattor</b></td><td></td><td class="statusunknown">unknown</td><td> Veronique Lefebure</td><td></td><td class="statusunknown">unknown</td><td></td><td>2010-11-11</td><td></td><td></td>
</tr>
</table>
<hr size="2" /><h2>Entities that quattor depends on:</h2><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgcdb_server" href="../pkgcdb_server">cdb server</a></td><td></td><td class="statusunknown">unknown</td><td> Veronique Lefebure</td><td></td><td class="status(none)">(none)</td><td></td><td>2010-11-11</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgswrep_server" href="../pkgswrep_server">swrep server</a></td><td></td><td class="statusunknown">unknown</td><td> Veronique Lefebure</td><td></td><td class="status(none)">(none)</td><td></td><td>2010-11-11</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgquattor_client_software" href="../pkgquattor_client_software">quattor client software</a></td><td></td><td class="statusunknown">unknown</td><td> Veronique Lefebure</td><td></td><td class="status(none)">(none)</td><td></td><td>2010-11-11</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgNCM_components" href="../pkgNCM_components">NCM components</a></td><td></td><td class="statusunknown">unknown</td><td> Veronique Lefebure</td><td></td><td class="status(none)">(none)</td><td></td><td>2010-11-11</td><td></td><td></td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>