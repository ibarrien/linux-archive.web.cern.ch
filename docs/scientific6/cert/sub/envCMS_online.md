<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on CMS-online:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envCertification" href="../envCertification">Certification</a></td><td class="statusunknown">unknown</td><td> Jan van Eldik</td><td class="statusfail">fail</td><td></td><td></td><td>2012-01-24</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: CMS-online (environment) </h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><b>CMS-online</b></td><td class="statusunknown">unknown</td><td> Marc Dobson</td><td class="statustest">test</td><td>blocking</td><td></td><td>2011-12-02</td>
</tr>
</table>
<hr size="2" /><h2>Entities that CMS-online depends on:</h2><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgprinting" href="../pkgprinting">printing</a></td><td></td><td class="statusok">ok</td><td> Jarek Polok</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgperl" href="../pkgperl">perl</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgcdb_server" href="../pkgcdb_server">cdb server</a></td><td></td><td class="statusunknown">unknown</td><td> Veronique Lefebure</td><td></td><td class="status(none)">(none)</td><td></td><td>2010-11-11</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgquattor_client_software" href="../pkgquattor_client_software">quattor client software</a></td><td></td><td class="statusunknown">unknown</td><td> Veronique Lefebure</td><td></td><td class="status(none)">(none)</td><td></td><td>2010-11-11</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgrdesktop" href="../pkgrdesktop">rdesktop</a></td><td></td><td class="statustest">test</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkggcc" href="../pkggcc">gcc</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-10-19</td><td>http://gcc.gnu.org/bugzilla/show_bug.cgi?id=48597 to be fixed in 6.2</td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgssh" href="../pkgssh">ssh</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgquattor" href="../pkgquattor">quattor</a></td><td></td><td class="statusunknown">unknown</td><td> Veronique Lefebure</td><td></td><td class="statusunknown">unknown</td><td></td><td>2010-11-11</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgCASTOR_client_sw2_1_11" href="../pkgCASTOR_client_sw2_1_11">CASTOR client sw</a></td><td>2.1.11</td><td class="statusworksforme">worksforme</td><td> Sebastien Ponce</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-11-14</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgORACLE_Instant_Client11_0_2_0_3" href="../pkgORACLE_Instant_Client11_0_2_0_3">ORACLE Instant Client</a></td><td>11.0.2.0.3</td><td class="statusworksforme">worksforme</td><td> Nilo Segura Chinchilla</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-10-21</td><td></td><td>RPM, "onlycern" repository</td>
</tr>
<tr>
<td><a name="pkgipmitool" href="../pkgipmitool">ipmitool</a></td><td></td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-05-06</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgPVSS3_8SP2" href="../pkgPVSS3_8SP2">PVSS</a></td><td>3.8SP2</td><td class="statusok">ok</td><td> Piotr Golonka</td><td></td><td class="statusworksforme">worksforme</td><td>Only supported on 64bit systems, needs 32 bit libs</td><td>2011-12-02</td><td></td><td>http://cern.ch/enice/PVSS+Service+Download+3.8SP2 </td>
</tr>
<tr>
<td><a name="pkgkernel_source" href="../pkgkernel_source">kernel-source</a></td><td></td><td class="statusok">ok</td><td> Jarek Polok</td><td></td><td class="statusok">ok</td><td>means: ability to compile modules against CERN kernel, and create customized kernel versions</td><td>2011-10-19</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgsubversion1_6" href="../pkgsubversion1_6">subversion</a></td><td>1.6</td><td class="statusok">ok</td><td>vendor</td><td></td><td class="status(none)">(none)</td><td></td><td>2011-10-21</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgphonebook" href="../pkgphonebook">phonebook</a></td><td></td><td class="statusok">ok</td><td> Jarek Polok</td><td></td><td class="status(none)">(none)</td><td>replaces phone command</td><td>2011-05-06</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgTSM_client" href="../pkgTSM_client">TSM client</a></td><td></td><td class="statusunknown">unknown</td><td> Alex Iribarren</td><td></td><td class="status(none)">(none)</td><td>works for drupal servers</td><td>2011-10-21</td><td></td><td>RPM, /afs/cern.ch/project/tsm/installations/linux/current/</td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>
