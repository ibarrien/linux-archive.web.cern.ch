<!--#include virtual="/linux/layout/header" --><h3>CERN Linux certification status</h3>
<br><br><hr size="2" /><h2>Entities that depend on LHCb Offline:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envCertification" href="../envCertification">Certification</a></td><td class="statusunknown">unknown</td><td> Jan van Eldik</td><td class="statusfail">fail</td><td></td><td></td><td>2012-01-24</td>
</tr>
</table>
<hr size="2" /><h2>Current entity: LHCb Offline (environment) </h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="blocking">
<td><b>LHCb Offline</b></td><td class="statusunknown">unknown</td><td> Joel Closier</td><td class="statustest">test</td><td>blocking</td><td></td><td>2011-11-14</td>
</tr>
</table>
<hr size="2" /><h2>Entities that LHCb Offline depends on:</h2><h4>Environments</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="status">status</th><th col="responsible">responsible</th><th col="dependency status">dependency status</th><th col="blocking">blocking</th><th col="comment">comment</th><th col="timestamp">timestamp</th></tr> <!-- end headers --><tr class ="">
<td><a name="envLHCb_Visualisation" href="../envLHCb_Visualisation">LHCb Visualisation</a></td><td class="statusunknown">unknown</td><td> Marco Cattaneo</td><td class="status(none)">(none)</td><td></td><td></td><td>2010-11-11</td>
</tr>
<tr class ="">
<td><a name="envLHCb_Production_Tools" href="../envLHCb_Production_Tools">LHCb Production Tools</a></td><td class="statusunknown">unknown</td><td> Joel Closier</td><td class="status(none)">(none)</td><td></td><td></td><td>2010-11-11</td>
</tr>
<tr class ="">
<td><a name="envLHCb_Software_Development" href="../envLHCb_Software_Development">LHCb Software Development</a></td><td class="statusunknown">unknown</td><td> Marco Cattaneo</td><td class="statusok">ok</td><td></td><td></td><td>2011-05-06</td>
</tr>
</table><h4>Packages</h4>
<table width="100%" border="1">
<tr> <!-- start headers --><th col="name">name</th><th col="version">version</th><th col="status">status</th><th col="responsible">responsible</th><th col="support">support</th><th col="dependency status">dependency status</th><th col="comment">comment</th><th col="timestamp">timestamp</th><th col="test">test</th><th col="distrib">distrib</th></tr> <!-- end headers --><tr>
<td><a name="pkgGAUDI" href="../pkgGAUDI">GAUDI</a></td><td></td><td class="statusunknown">unknown</td><td> Marco Cattaneo</td><td></td><td class="statustest">test</td><td></td><td>2011-11-14</td><td></td><td></td>
</tr>
<tr>
<td><a name="pkgCERNLIB2006_3" href="../pkgCERNLIB2006_3">CERNLIB</a></td><td>2006-3</td><td class="statusunknown">unknown</td><td> Benedikt Hegner</td><td></td><td class="statusworksforme">worksforme</td><td></td><td>2011-11-14</td><td></td><td>RPM</td>
</tr>
<tr>
<td><a name="pkgGEANT4" href="../pkgGEANT4">GEANT4</a></td><td></td><td class="statusunknown">unknown</td><td> Gabriele Cosmo</td><td></td><td class="statusok">ok</td><td>http://lcgapp.cern.ch/project/spi/extsoft/packages.php?pkg=geant4
</td><td>2011-10-19</td><td></td><td></td>
</tr>
</table>
<hr size="2" /><br><br><br><br><h3>Explanation</h3><table border="1"><tr><td class="statusok">OK</td><td><a name="statusok"></a>tested and worked ok, no serious problems found</td></tr><tr><td class="statustest">TEST</td><td><a name="statustest"></a>Test in progress</td></tr><tr><td class="statusfail">FAIL</td><td><a name="statusfail"></a>Tested and failed</td></tr><tr><td class="statuswont">WONT</td><td><a name="statuswont"></a>Will not be tested (e.g. not part of the certification or will be running a custom version of Linux)</td></tr><tr><td class="statusworksforme">WORKSFORME</td><td><a name="statusworksforme"></a>Tested (but not neccessarily by maintainer or exhaustive), seems to work.Also used for commercial applications without formal support for this platform, but which seem to work.</td></tr><tr><td class="statusunknown">UNKNOWN</td><td><a name="statusunknown"></a>not tested or no feedback from maintainer, no other reports of success or failure</td></tr><tr><td class="statusmissing">MISSING</td><td><a name="statusmissing"></a>required (perhaps only in previous versions) but not provided</td></tr></table>