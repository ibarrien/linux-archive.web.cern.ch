<!--#include virtual="/linux/layout/header" -->
# SLC6: Scientific Linux CERN 6

<h2>What will be Scientific Linux CERN 6 (SLC6) ?</h2>
<p>Scientific Linux CERN 6 will be a Linux distribution build within the
framework of <a href="http://www.scientificlinux.org">Scientific Linux</a>
which in turn is rebuilt from the freely
available <a href="https://www.redhat.com/software/rhel/">Red Hat
Enterprise Linux 6 (Server)</a>
product sources under terms and conditions of the Red Hat EULA.
Scientific Linux CERN is built to integrate into the CERN computing
environment but it is not a site-specific product:
all CERN site customizations are optional and
can be deactivated for external users. </p>

<h2>Scientific Linux CERN 6.0 (SLC6.0) TEST1</h2>
A <em>TEST</em> version of future SLC6 is available
since 29.11.2010 for installs via the <a href="/linux/install/#pxe">PXE network boot</a>
at CERN from the 'Expert Operating System Install Menu' / 'SLC 6 PREVIEW/TEST Install Menu'
<p>
Please note that this is a <em>pre-release</em> version
<em>NOT</em> suitable for production purposes and possibly affected by 
numerous problems.
<p>
Since the SLC6 version is not supported yet please note that
we (linux.support@cern.ch) do not guarantee timely updates
(including security ones) nor CERN helpdesk offers support
for it. If you would like to participate in discussions 
about future SLC6 please join <a href="http://simba3.web.cern.ch/simba3/SelfSubscription.aspx?groupName=linux-certification">linux-certification@cern.ch</a> mailing list.

<h2>Known problems</h2>

<h5>installation</h5>
- no 'recommended setup' for now.
- Kerberos keytab(s) are not generated automatically on install
  (see below)

<h5>cern-config-keytab</h5>
- sometimes there are problems generating keytab files,
if cern-config-keytab -f hangs forever , please remove 
/etc/krb5.keytab* files and retry.


<h5>kerberized SSH</h5>
- make sure that /etc/hosts does not include
  real DNS hostname for localhost (127.0.0.1 or ::1)
- make sure that keytab(s) are generated OK (see above)
 

<h5> more to come ...</h5>
...

<h2>Certification status</h2>

Formal certification of SLC6 will start soon, we will keep 
you updated on these web pages.


