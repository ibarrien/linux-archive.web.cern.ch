<!--#include virtual="/linux/layout/header" -->
# Non-PAE SLC6X / i386 variant
<h2>Scientific Linux CERN 6 Non-PAE / i386 variant <em>TEST</em></h2>
<p>
As of 2012 CERN Experiments are using large numbers of VME-based single board systems featuring Pentium III / Pentium M processors
not implementing PAE (Page Address Extensions) technology. Standard SLC 6X does not support such systems, therefore 
we have prepared a special variant which can be installed and used on Non-PAE 32 bit systems.
</p>
<p>
This version (i386nonpae) features same set of packages as standard SLC6X i386 version, except
for the kernel and sl-release packages. Kernel package has been rebuild without PAE support and
sl-release package contains set of additional repositories for future non-PAE kernel updates.
</p>
<p>
Please note: This is a <em>TEST</em> variant of SLC6X/i386 version, not to be used in production.

<h2>Installation</h2>
Standard Scientific Linux CERN 6 <a href="/linux/scientific6/docs/install">installation instructions</a> do apply, except for installation images location and installation path.
<ul>
<li>Boot CD media can be found at: <a href="http://linuxsoft.cern.ch/cern/slc63/i386nonpae/images/boot.iso">http://linuxsoft.cern.ch/cern/slc63/i386nonpae/images/boot.iso</a>
<li>Installation path is: <a href="http://linuxsoft.cern.ch/cern/slc63/i386nonpae/">http://linuxsoft.cern.ch/cern/slc63/i386nonpae/</a>
</ul>
The SLC6X i386nonpae version can also be installed from <a href="/linux/install/">Network Installation System</a> ("Expert Operating System Install Menu" -> "Scientific Linux CERN Install Menu" -> "Install SLC 6.3 NONPAE").

<h2>Updates</h2>

Reqular SLC6 updates are applicable to this version, except kernel/sl-release updates. i386nonpae systems
are preconfigured to automatically fetch and apply all updates from standard SLC6/i386 repositories, excluding
kernel/sl-release packages. kernel and sl-release packages are provided from additional i386nonpae specific
repositories. 
<p>
<em>Note:</em>: Non-PAE kernel updates will be published only on request from CERN Experiments (probably starting in 2013 only). 

<h2>Support</h2>

Only limited support (distribution packaging/ installation) is provided to collaborating CERN Experiment groups.

<h2>Version</h2>
Current i386nonpae SLC6X release is 6.3 (17.08.2012).

<div align="right"><a href="mailto:Jaroslaw.Polok@cern.ch">Jaroslaw.Polok@cern.ch</a></div>

