<!--#include virtual="/linux/layout/header" -->
# SLC6: Scientific Linux CERN 6

<h2>What is Scientific Linux CERN 6 (SLC6) ?</h2>
<p>Scientific Linux CERN 6 is be a Linux distribution build within the
framework of <a href="http://www.scientificlinux.org">Scientific Linux</a>
which in turn is rebuilt from the freely
available <a href="https://www.redhat.com/software/rhel/">Red Hat
Enterprise Linux 6 (Server)</a>
product sources under terms and conditions of the Red Hat EULA.
Scientific Linux CERN is built to integrate into the CERN computing
environment but it is not a site-specific product:
all CERN site customizations are optional and
can be deactivated for external users. </p>

<h2>Scientific Linux CERN 6.1 (SLC6.1) BETA</h2>
A <em>BETA</em> version of SLC6 is available
since 09.06.2011 for installs via the <a href="/linux/install/#pxe">PXE network boot</a>
at CERN from the main Install Menu'
<p>
If you would like to participate in discussions 
about future SLC6 please join <a href="http://simba3.web.cern.ch/simba3/SelfSubscription.aspx?groupName=linux-certification">linux-certification@cern.ch</a> mailing list.

<h2>Known problems</h2>

<h5>installation</h5>
- 'recommended setup' installation option is not definitive.
<!-- - Kerberos keytab(s) are not generated automatically on install
  (see below)
-->

<!--<h5>cern-config-keytab</h5>
- sometimes there are problems generating keytab files,
if cern-config-keytab -f hangs forever , please remove 
/etc/krb5.keytab* files and retry.
-->
<h5>kerberized SSH</h5>
- make sure that /etc/hosts does not include
  real DNS hostname for localhost (127.0.0.1 or ::1)
- make sure that keytab(s) are generated OK (see above)
 

<h5> more to come ...</h5>
...

<h2>Certification status</h2>

Formal certification of SLC6 started on 31st of January,
please see <a href="cert/">certification pages</a>.


