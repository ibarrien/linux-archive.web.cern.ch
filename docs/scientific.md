# What is Scientific Linux (SL) ?

From [Scientific Linux web site](http://www.scientificlinux.org/):

> SL is a Linux release put together by Fermilab, CERN, and various other labs and universities around the world. Its primary purpose is to reduce duplicated effort of the labs, and to have a common install base for the various experimenters.

> The base SL distribution is basically [ Red Hat ] Enterprise Linux, recompiled from source.

> Our main goal for the base distribution is to have everything compatible with Enterprise, with only a few minor additions or changes. An example of of items that were added are Pine, and OpenAFS.

> Our secondary goal is to allow easy customization for a site, without disturbing the Scientific Linux base. The various labs are able to add their own modifications to their own site areas. By the magic of scripts, and the anaconda installer, each site is to be able to create their own distributions with minimal effort. Or, if a users wishes, they can simply install the base SL release.

## And Scientific Linux CERN (SLC) ?

SLC is an SL variant that is built on top of the genuine SL and it is tailored to integrate within the CERN computing environment.

SLC is fully compatible with SL therefore with the [ Red Hat ] Enterprise Linux: all software used or built on one of the versions should function properly on any other version.

## Which one should I use ?

If your system is installed at CERN you should definitely use CERN provided version: the Scientific Linux CERN, that is the only version we provide support for installations made on CERN site.

If your system is located outside CERN lab, you may still use SLC there: CERN customizations and site integration we provide for CERN users shall be inobtrusive for others and are not activated by default if the system is installed outside CERN network: and, of course, your system will still receive all the security, enhancement and bugfix errata.

## .. in numbers

As of March 2009 around **36000** systems run Scientific Linux (SL), around **14000** systems run Scientific Linux CERN (SLC).

Above includes only systems updating regularily from either SL or SLC main distribution servers.