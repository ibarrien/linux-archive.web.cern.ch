# Linux web

This repo follows <https://how-to.docs.cern.ch>.

## Customisations

* `/theme` directory is used as the [Material Design](https://github.com/squidfunk/mkdocs-material) `custom_dir` for [overriding the theme](https://squidfunk.github.io/mkdocs-material/customization/).

* We use https://gitlab.cern.ch/linuxsupport/websites/updates-autogeneration-plugin/ for autogenerating, ordering and adding to our navigation all distribution updates.

## CI/CD

Instead of relying on webhooks as suggested by <https://how-to.docs.cern.ch>, we run builds on CI so we can notice whether they fail or not.

This requires setting up a service account on each namespace:

```
oc login openshift.cern.ch
oc project linux-archive
oc create sa openshift-operations
oc policy add-role-to-user admin system:serviceaccount:linux-archive:openshift-operations
oc sa get-token openshift-operations
# Note the token on https://gitlab.cern.ch/groups/linuxsupport/-/settings/ci_cd -> LINUXARCHIVE_OPENSHIFT. REMEMBER TO SELECT MASK VARIABLE!!!
```

## Known issues and limitations

* Due to the size of our documentation we need to edit the corresponding `BuildConfig` object with specific limits so it does not use the default ones:

```
# Edit the buildconfig object
oc edit buildconfig linux-archive
```

```
# Add the following section:
spec:
  (...)
  resources:
    limits:
      memory: 1Gi
    requests:
      memory: 512Mi
```

